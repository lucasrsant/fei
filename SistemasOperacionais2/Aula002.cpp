#include <windows.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <tchar.h>
#include <map>

using namespace std;
map<string, int> filesizeMap;

void findFile(string path, string mask, bool verbose) {

    HANDLE hFind;
	WIN32_FIND_DATA data;
	string pathAndMask = path + mask;
	hFind = FindFirstFile(pathAndMask.c_str(), &data);
	
	if (hFind != INVALID_HANDLE_VALUE) {
	  	do {
			if(_tcscmp(".", data.cFileName) != 0 && _tcscmp("..", data.cFileName) != 0) {
				basic_string<TCHAR> filenameStr = data.cFileName;
				filesizeMap[filenameStr] = data.nFileSizeLow;
				
				if(verbose)
					cout << path << "\\" << filenameStr << endl;
					
		    	findFile(path + data.cFileName + "\\", mask, verbose);
			}
	  	}
		while (FindNextFile(hFind, &data));

	  	FindClose(hFind);
	}
}

int main(int argc, char** argv) {
	cout << "Loading data..." << endl;
	findFile("C:\\Temp\\", "*", true);
	cout << "Type a filename:" << endl;
	string filename;
	cin >> filename;

	map<string, int>::iterator i = filesizeMap.find(filename);
	if(i != filesizeMap.end())
	    cout << "The file " << i->first << " has " << i->second << " bytes" << endl;
	else
	    cout << "File not found" << endl;

	system("pause");
}
