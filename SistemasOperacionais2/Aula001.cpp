#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <errno.h>

void printUsage() {
	printf("\nThe correct usage of exemplo.exe is:\n");
	printf("\texemplo.exe -a [FIRST_NUMBER] -b [SECOND_NUMBER]\n\n");
	printf("Both first and second numbers must be integers of 32 bits\n");
}

void exitWithError(int error) {
	printUsage();
	exit(error);
}

int sum(int a, int b) {
	return a + b;
}

int main(int argc, char *argv[])
{
	int c;
	printf("Existem %d argumentos.\n", argc);
	
	int a = 0;
	int b = 0;
	
	int aSet = 0;
	int bSet = 0;

 	char* end;
	for (c = 0; c < argc; c++) {
		if(strcmp(argv[c], "-a") == 0 && (c < argc - 1)) {
			a = strtol(argv[c + 1], &end, 10);

			if(errno)
				exitWithError(30);

			aSet = 1;
		}

		if(strcmp(argv[c], "-b") == 0 && (c + 1 < argc)) {
   			b = strtol(argv[c + 1], &end, 10);
   			if(errno)
			    exitWithError(30);

			bSet = 1;
		}
	}
	
	if(aSet && bSet)
	    printf("%d + %d = %d\n", a, b, sum(a, b));
	else
	    exitWithError(31);
	   
	//vou retornar 25 para o sistema operacional
	return(25);
}
