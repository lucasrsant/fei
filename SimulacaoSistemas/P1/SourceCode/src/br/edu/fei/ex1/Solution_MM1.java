package br.edu.fei.ex1;

import br.edu.fei.utils.MM1;
import br.edu.fei.utils.Utils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import java.util.ArrayList;
import java.util.List;

public class Solution_MM1 {


    public static void calculateMM1(double a) {
        List<Computer> result = new ArrayList<>();

        result.add(calculateC1_MM1(a / 60 / 60));
        result.add(calculateC2_MM1(a / 60 / 60));
        result.add(calculateC3_MM1(a / 60 / 60));
        result.add(calculateC4_MM1(a / 60 / 60));
        result.add(calculateCPU_MM1(a / 60 / 60));

        Utils.printResult(result, a);
    }

    private static Computer calculateC1_MM1(double a) {
        Computer computer = new Computer();
        computer.name = "C1";

        double K = 1.15f;
        double r = 0.774264; //manually calculated
        double u = MM1.getUInFunctionOfRAndA(a, r);
        double Ts = MM1.getTs(u);

        double Pn4 = MM1.getPn(r, 4);
        double Tw = MM1.getTwInFunctionOfAAndU(a, u, K);
        double Tr = MM1.getTrInFunctionOfTsAndR(Ts, r);
        double Lw = MM1.getLwInFunctionOfR(r);
        double Ls = MM1.getLsInFunctionOfR(r);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", MM1.getP0(r)));

        return computer;
    }

    private static Computer calculateC2_MM1(double a) {
        Computer computer = new Computer();
        computer.name = "C2";

        double K = 1.45f;
        double Lw = 8;
        double r = 0.898979; //manually calculated

        double u = MM1.getUInFunctionOfRAndA(a, r);
        double Ts = MM1.getTs(u);
        double Pn4 = MM1.getPn(r, 4);
        double Tw = MM1.getTwInFunctionOfAAndU(a, u, K);
        double Tr = MM1.getTrInFunctionOfTsAndR(Ts, r);
        double Ls = MM1.getLsInFunctionOfR(r);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", MM1.getP0(r)));

        return computer;
    }

    private static Computer calculateC3_MM1(double a) {

        Computer computer = new Computer();
        computer.name = "C3";

        double Tw = 9.25;
        double u = 0.14817; //manually calculated
        double Ts = MM1.getTs(u);
        double r = MM1.getRInFunctionOfTs(a, Ts);
        double Tr = MM1.getTrInFunctionOfTsAndR(Ts, r);
        double Lw = MM1.getLwInFunctionOfR(r);
        double Ls = MM1.getLsInFunctionOfR(r);
        double Pn4 = MM1.getPn(r, 4);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", MM1.getP0(r)));

        return computer;
    }

    private static Computer calculateC4_MM1(double a) {
        Computer computer = new Computer();
        computer.name = "C4";

        double Ts = MM1.getTs(Exercice1.randomNumbersC4, 10);

        double K = MM1.getK(new StandardDeviation().evaluate(Utils.toDoubleArray(Exercice1.randomNumbersC4)), Ts);

        double r = MM1.getRInFunctionOfTs(a, Ts);
        double u = MM1.getU(Ts);

        double Pn4 = MM1.getPn(r, 4);
        double Tw = MM1.getTwInFunctionOfAAndU(a, u, K);
        double Tr = MM1.getTrInFunctionOfTsAndR(Ts, r);
        double Ls = MM1.getLsInFunctionOfR(r);
        double Lw = MM1.getLwInFunctionOfR(r);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", MM1.getP0(r)));

        return computer;
    }

    private static Computer calculateCPU_MM1(double a) {
        Computer computer = new Computer();
        computer.name = "CPU";

        double Ts = MM1.getTs(Exercice1.randomNumbersCPU,8);
        double u = MM1.getU(Ts);
        double r = MM1.getRInFunctionOfU(a, u);
        double K = MM1.getK(new StandardDeviation().evaluate(Utils.toDoubleArray(Exercice1.randomNumbersCPU)), Ts);

        double Pn4 = MM1.getPn(r, 4);
        double Tw = MM1.getTwInFunctionOfAAndU(a, u, K);
        double Tr = MM1.getTrInFunctionOfTsAndR(Ts, r);
        double Ls = MM1.getLsInFunctionOfR(r);
        double Lw = MM1.getLwInFunctionOfR(r);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", MM1.getP0(r)));

        return computer;
    }
}
