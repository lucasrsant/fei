package br.edu.fei.ex1;

import br.edu.fei.utils.RandomGenerator;

import java.util.List;

public class Exercice1 {

    /*package*/ static List<Double> randomNumbersC4 = RandomGenerator.randomLCG(10);
    /*package*/ static List<Double> randomNumbersCPU = RandomGenerator.randomLCG(20);

    public static void solve() {
        for(double a = 50; a <= 1000; a+=50) {
            Solution_MM1.calculateMM1(a);
        }

        for(double a = 50; a <= 1000; a+=50) {
            Solution_MMC.calculateMMC(a);
        }
    }
}
