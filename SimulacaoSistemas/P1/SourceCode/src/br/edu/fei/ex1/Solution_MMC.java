package br.edu.fei.ex1;

import br.edu.fei.utils.MMC;
import br.edu.fei.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Solution_MMC {

    private static final int C = 5;

    public static void calculateMMC(double a) {
        List<Computer> result = new ArrayList<>();

        result.add(calculateC1_MMC(a));
        result.add(calculateC2_MMC(a));
        result.add(calculateC3_MMC(a));
        result.add(calculateC4_MMC(a));
        result.add(calculateCPU_MMC(a));

        Utils.printResult(result, a);
    }

    private static Computer calculateC1_MMC(double a) {
        Computer computer = new Computer();
        computer.name = "C1";

        double r = 0.774264; //manually calculated

        double u = MMC.getU(a, r);
        double ro = MMC.getR(a, u);

        double p0 = MMC.getP0(C, a, u, ro);
        double delta = MMC.getDelta(C, ro, p0);

        double Ts = MMC.getTs(u);

        double Pn4 = MMC.getPn(4, C, a, u, p0);
        double Tw = MMC.getTw(C, delta, ro, u);
        double Tr = MMC.getTr(C, delta, ro, u);
        double Lw = MMC.getLw(ro, delta);
        double Ls = MMC.getLs(C, delta, ro);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", p0))  ;

        return computer;
    }

    private static Computer calculateC2_MMC(double a) {
        Computer computer = new Computer();
        computer.name = "C2";

        double Lw = 8;
        double r = 0.898979; //manually calculated
        double u = MMC.getU(a, r);

        double ro = MMC.getRo(a, C, u);
        double p0 = MMC.getP0(C, a, u, ro);
        double delta = MMC.getDelta(C, ro, p0);

        double Ts = MMC.getTs(u);
        double Pn4 = MMC.getPn(4, C, a, u, p0);
        double Tw = MMC.getTw(C, delta, ro, u);
        double Tr = MMC.getTr(C, delta, ro, u);
        double Ls = MMC.getLs(C, delta, ro);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", p0));

        return computer;
    }

    private static Computer calculateC3_MMC(double a) {

        Computer computer = new Computer();
        computer.name = "C3";

        double Tw = 9.25;
        double u = 0.14817; //manually calculated
        double Ts = MMC.getTs(u);
        double ro = MMC.getRo(a, C, u);
        double p0 = MMC.getP0(C, a, u, ro);
        double delta = MMC.getDelta(C, ro, p0);

        double Tr = MMC.getTr(C, delta, ro, u);
        double Lw = MMC.getLw(ro, delta);
        double Ls = MMC.getLs(C, delta, ro);
        double Pn4 = MMC.getPn(4, C, a, u, p0);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", p0));

        return computer;
    }

    private static Computer calculateC4_MMC(double a) {
        Computer computer = new Computer();
        computer.name = "C4";

        double Ts = MMC.getTs(Exercice1.randomNumbersC4, 10);

        double r = MMC.getR(a, Ts);
        double u = MMC.getU(a, r);

        double ro = MMC.getRo(a, C, u);
        double p0 = MMC.getP0(C, a, u, ro);

        double delta = MMC.getDelta(C, ro, p0);

        double Pn4 = MMC.getPn(4, C, a, u, p0);
        double Tw = MMC.getTw(C, delta, ro, u);
        double Tr = MMC.getTr(C, delta, ro, u);
        double Ls = MMC.getLs(C, delta, ro);
        double Lw = MMC.getLw(ro, delta);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", p0));

        return computer;
    }

    private static Computer calculateCPU_MMC(double a) {
        Computer computer = new Computer();
        computer.name = "CPU";

        double Ts = MMC.getTs(Exercice1.randomNumbersCPU, 8);
        double u = MMC.getU(Ts);

        double ro = MMC.getRo(a, C, u);
        double p0 = MMC.getP0(C, a, u, ro);

        double delta = MMC.getDelta(C, ro, p0);

        double Pn4 = MMC.getPn(4, C, a, u, p0);
        double Tw = MMC.getTw(C, delta, ro, u);
        double Tr = MMC.getTr(C, delta, ro, u);
        double Ls = MMC.getLs(C, delta, ro);
        double Lw = MMC.getLw(ro, delta);

        computer.parameters = new ArrayList<>();
        computer.parameters.add(new Parameter("Tr", Tr));
        computer.parameters.add(new Parameter("Ts", Ts));
        computer.parameters.add(new Parameter("Tw", Tw));
        computer.parameters.add(new Parameter("Lw", Lw));
        computer.parameters.add(new Parameter("Ls", Ls));
        computer.parameters.add(new Parameter("P(n = 4)", Pn4));
        computer.parameters.add(new Parameter("P0", p0));

        return computer;
    }
}
