package br.edu.fei.ex1;

public class Parameter {
    public String name;
    public double value;

    public Parameter(String name, double value) {
        this.name = name;
        this.value = value;
    }
}
