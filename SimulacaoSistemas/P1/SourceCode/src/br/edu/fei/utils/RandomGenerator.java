package br.edu.fei.utils;

import java.util.ArrayList;
import java.util.List;

public class RandomGenerator {
    private static final int A = 19;
    private static final int C = 5;
    private static final int MODULUS = 100;
    private static final int SEED = 22115017;

    public static List<Double> randomLCG(int quantity) {
        List<Double> randomNumbers = new ArrayList<>();
        randomNumbers.add((double) SEED);

        for (int i = 1; i <= quantity; i++) {
            double Xn = (A * randomNumbers.get(i - 1) + C) % MODULUS;
            randomNumbers.add(Xn / MODULUS);
        }

        randomNumbers.remove(0); //remove seed
        return randomNumbers;
    }
}
