package br.edu.fei.utils;

import java.util.ArrayList;
import java.util.List;

public class MMC {
    public static double getRo(double a, double c, double u) {
        return a / (c * u);
    }

    public static double getR(double a, double u) {
        return a / u;
    }

    public static double getU(double a, double r) {
        return a / r;
    }

    public static double getTs(double u) {
        return 1 / u;
    }

    public static double getU(double ts) {
        return 1 / ts;
    }

    public static double getTs(List<Double> randomNumbers, double theta) {
        List<Double> samples = new ArrayList<>();

        for(Double randomNumber : randomNumbers) {
            samples.add(- theta * Math.log(randomNumber));
        }

        return samples.stream().mapToDouble(value -> value).average().getAsDouble();
    }

    public static double getP0(int c, double a, double u, double ro) {
        double firstPart = 0;

        for(int n = 0; n < c; n++) {
            firstPart += (1/factorial(n)) * Math.pow(a/u, n);
        }

        double secondPart = (1/factorial(c))*Math.pow(a/u, c)*(1/(1 - ro));

        return Math.pow(firstPart + secondPart, -1);
    }

    public static double getPn(int n, int c, double a, double u, double p0) {
        if (n < c) {
            return (1 / factorial(n)) * Math.pow(a/u, n) * p0;
        }
        else {
            return (1 / (factorial(c) * Math.pow(c, n - c)) ) * Math.pow(a / u, n) * p0;
        }
    }

    public static double getDelta(int c, double ro, double p0) {
        return ( Math.pow(c * ro, c) / (factorial(c) * (1 - ro))) * p0;
    }

    public static double getLw(double ro, double delta) {
        return (ro * delta) / (1 - ro);
    }

    public static double getTw(int c, double delta, double ro, double u) {
        return delta / (c * u * (1 - ro));
    }

    public static double getLs(int c, double delta, double ro) {
        return (c * ro) + ((delta * ro)/(1 - ro));
    }

    public static double getTr(int c, double delta, double ro, double u) {
        return (1 / u) * (1 + (delta / (c * (1 - ro))));
    }

    private static double factorial(int n) {
        return (n == 1 || n == 0) ? 1 : n * factorial(n - 1);
    }
}
