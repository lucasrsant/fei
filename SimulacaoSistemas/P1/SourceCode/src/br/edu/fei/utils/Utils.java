package br.edu.fei.utils;

import br.edu.fei.ex1.Computer;
import br.edu.fei.ex1.Parameter;

import java.util.List;

public class Utils {
    public static double[] toDoubleArray(List<Double> list) {
        double[] target = new double[list.size()];
        for (int i = 0; i < target.length; i++) {
            // or:
            target[i] = list.get(i);                // java 1.5+ style (outboxing)
        }

        return target;
    }

    public static void printResult(List<Computer> result, double a) {
        for(Computer computer : result) {
            System.out.println(computer.name + " (A = " + a + ")");

            for(Parameter parameter : computer.parameters) {
                System.out.printf("%s: %.10f", parameter.name, parameter.value);
                System.out.println();
            }

            System.out.println();
        }
    }
}
