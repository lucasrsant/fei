package br.edu.fei.utils;

import java.util.ArrayList;
import java.util.List;

public class MM1 {
    public static double getRInFunctionOfU(double a, double u) {
        return a / u;
    }

    public static double getRInFunctionOfTs(double a, double ts) {
        return a * ts;
    }

    public static double getTs(double u) {
        return 1 / u;
    }

    public static double getTs(List<Double> randomNumbers, double theta) {
        List<Double> samples = new ArrayList<>();

        for(Double randomNumber : randomNumbers) {
            samples.add(-theta * Math.log(randomNumber));
        }

        return samples.stream().mapToDouble(value -> value).average().getAsDouble();
    }

    public static double getU(double ts) {
        return 1 / ts;
    }

    public static double getUInFunctionOfRAndA(double a, double r) {
        return a / r;
    }

    public static double getPn(double r, int n) {
        return (1 - r) * Math.pow(r, n);
    }

    public static double getP0(double r) {
        return 1 - r;
    }

    public static double getLsInFunctionOfAAndU(double a, double u) {
        return a / (u - a);
    }

    public static double getLsInFunctionOfR(double r) {
        return r / (1 - r);
    }

    public static double getLwInFunctionOfAAndU(double a, double u) {
        return Math.pow(a, 2)/(u * (u - a));
    }

    public static double getLwInFunctionOfR(double r) {
        return Math.pow(r, 2)/(1 - r);
    }

    public static double getTwInFunctionOfAAndU(double a, double u, double K) {
        return (a * K) / (u * (u - a));
    }

    public static double getTwInFunctionOfRAndTs(double r, double ts, double K) {
        return (r * ts * K) / (1 - r);
    }

    public static double getTrInFunctionOfTsAndR(double ts, double r) {
        return ts / (1 - r);
    }

    public static double getTrInFunctionOfTsAndTw(double ts, double tw) {
        return ts + tw;
    }

    public static double getK(double sd, double ts) {
        return 1 + Math.pow((sd/ts), 2);
    }

    public static double getProbabilityP(double percentage, double r) {
        return (Math.log10(1 - percentage / 100)/Math.log10(r)) - 1;
    }
}
