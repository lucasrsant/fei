SQL>  create table moodle (nome char(20), idade number(3), sexo char(1));

Tabela criada.

SQL> desc moodle;
 Nome                                      Nulo?    Tipo
 ----------------------------------------- -------- ----------------------------
 NOME                                               CHAR(20)
 IDADE                                              NUMBER(3)
 SEXO                                               CHAR(1)

SQL> alter table moodle modify nome char(40);

Tabela alterada.

SQL> desc moodle;
 Nome                                      Nulo?    Tipo
 ----------------------------------------- -------- ----------------------------
 NOME                                               CHAR(40)
 IDADE                                              NUMBER(3)
 SEXO                                               CHAR(1)

SQL> grant select on moodle to bd1_221150402;

Concessão bem-sucedida.

SQL> revoke select on moodle from bd1_221150402;

Revogação bem-sucedida.

SQL> insert into moodle (nome, idade, sexo) values ('Lucas', 23, 'M');

1 linha criada.

SQL> select * from moodle where nome = 'Lucas';

NOME                                          IDADE S                           
---------------------------------------- ---------- -                           
Lucas                                            23 M                           

SQL> update moodle set nome = 'Lucas Santos' where nome = 'Lucas';

1 linha atualizada.

SQL> select * from moodle;

NOME                                          IDADE S                           
---------------------------------------- ---------- -                           
Lucas Santos                                     23 M                           

SQL> delete moodle where nome = 'Lucas Santos';

1 linha deletada.

SQL> select * from moodle;

não há linhas selecionadas

SQL> drop table moodle;

Tabela eliminada.

SQL> desc moodle;
ERROR:
ORA-04043: o objeto moodle não existe 


SQL> spool off;
