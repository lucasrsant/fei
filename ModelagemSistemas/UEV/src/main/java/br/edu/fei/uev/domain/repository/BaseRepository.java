package br.edu.fei.uev.domain.repository;

import br.edu.fei.uev.domain.entity.BaseEntity;

import java.util.List;

public interface BaseRepository<T extends BaseEntity> {
    T find(int id);
    List<T> findAll();
    void delete(int id);
    void update(T entity);
    void save(T entity);
}
