package br.edu.fei.uev.app.di;


public class DependencyNotInitializedException extends RuntimeException {
    DependencyNotInitializedException(String className) {
        super("Dependency of type " + className + " has not been initialized.");
    }
}
