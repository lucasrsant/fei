package br.edu.fei.uev.reactive;

public interface Observable<T> {
    Subscription subscribe(Subscriber<T> subscriber);
}