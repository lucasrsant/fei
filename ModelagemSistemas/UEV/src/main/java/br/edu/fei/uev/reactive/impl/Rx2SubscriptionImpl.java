package br.edu.fei.uev.reactive.impl;

import br.edu.fei.uev.reactive.Subscription;

public class Rx2SubscriptionImpl implements Subscription {
    private final io.reactivex.disposables.Disposable rxDisposable;

    public Rx2SubscriptionImpl(io.reactivex.disposables.Disposable rxDisposable) {
        this.rxDisposable = rxDisposable;
    }
}
