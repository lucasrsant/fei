package br.edu.fei.uev.library.repository;

public class HibernateConnectionProvider {
    private static HibernateConnectionProvider instance;

    private HibernateConnectionProvider() {

    }

    public static HibernateConnectionProvider getProviderInstance() {
        if(instance == null)
            instance = new HibernateConnectionProvider();

        return instance;
    }
}
