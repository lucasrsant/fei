package br.edu.fei.uev.app;

import br.edu.fei.uev.app.jobs.InitializerJobQueue;

public class Application {
    public static void main(String[] args) {
        InitializerJobQueue.run();
    }
}