package br.edu.fei.uev.reactive;

import br.edu.fei.uev.reactive.impl.Rx2ObservableImpl;
import br.edu.fei.uev.reactive.impl.Rx2SubscriptionImpl;

public class ReactiveStreamConverter {
    public static <T> Observable<T> fromRx2Observable(io.reactivex.Observable<T> rxObservable) {
        return new Rx2ObservableImpl<T>(rxObservable);
    }

    public static Subscription fromRx2Disposable(io.reactivex.disposables.Disposable disposable) {
        return new Rx2SubscriptionImpl(disposable);
    }
}
