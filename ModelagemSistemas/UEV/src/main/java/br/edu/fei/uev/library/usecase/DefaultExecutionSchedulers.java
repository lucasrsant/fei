package br.edu.fei.uev.library.usecase;

import br.edu.fei.uev.domain.usecase.ExecutionSchedulers;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class DefaultExecutionSchedulers implements ExecutionSchedulers {
    public Scheduler getObserveScheduler() {
        return Schedulers.trampoline();
    }

    public Scheduler getSubscribeScheduler() {
        return Schedulers.computation();
    }
}
