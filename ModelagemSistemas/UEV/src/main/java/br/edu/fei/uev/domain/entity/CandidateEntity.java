package br.edu.fei.uev.domain.entity;

public class CandidateEntity extends BaseEntity {

    private CandidateEntity() { }

    public static class Builder {
        private CandidateEntity entity;

        private Builder() {
            entity = new CandidateEntity();
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public CandidateEntity build() {
            return entity;
        }

        public Builder withId(long id) {
            entity.setId(id);
            return this;
        }
    }
}