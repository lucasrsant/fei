package br.edu.fei.uev.domain.entity;

public class BaseEntity {
    private long id;

    public long getId() {
        return id;
    }

    protected void setId(long id) {
        this.id = id;
    }
}
