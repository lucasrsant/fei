package br.edu.fei.uev.domain.usecase;

import io.reactivex.Scheduler;

public interface ExecutionSchedulers {
    Scheduler getObserveScheduler();
    Scheduler getSubscribeScheduler();
}
