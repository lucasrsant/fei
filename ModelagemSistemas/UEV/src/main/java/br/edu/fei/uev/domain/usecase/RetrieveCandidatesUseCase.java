package br.edu.fei.uev.domain.usecase;

import br.edu.fei.uev.domain.model.Candidate;
import br.edu.fei.uev.reactive.Observable;

public interface RetrieveCandidatesUseCase {
    Observable<Candidate> execute();
}