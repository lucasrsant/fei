package br.edu.fei.uev.app.jobs;

public interface AsyncJobCallback {
    void onCompleted(String serviceName);
    void onError(String serviceName, Throwable throwable);
}
