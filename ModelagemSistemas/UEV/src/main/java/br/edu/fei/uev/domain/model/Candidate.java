package br.edu.fei.uev.domain.model;

public class Candidate {

    private long id;

    private Candidate() { }

    public static class Builder {

        private Candidate candidate;

        private Builder() {
            candidate = new Candidate();
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder withId(long id) {
            candidate.id = id;
            return this;
        }

        public Candidate build() {
            return candidate;
        }
    }
}
