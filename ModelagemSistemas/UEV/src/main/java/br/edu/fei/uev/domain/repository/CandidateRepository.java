package br.edu.fei.uev.domain.repository;

import br.edu.fei.uev.domain.entity.CandidateEntity;

public interface CandidateRepository extends BaseRepository<CandidateEntity> {

}