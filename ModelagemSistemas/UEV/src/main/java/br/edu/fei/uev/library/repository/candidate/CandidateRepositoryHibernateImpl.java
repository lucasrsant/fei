package br.edu.fei.uev.library.repository.candidate;

import br.edu.fei.uev.domain.entity.CandidateEntity;
import br.edu.fei.uev.domain.repository.CandidateRepository;
import br.edu.fei.uev.library.repository.HibernateConnectionProvider;

import java.util.List;

public class CandidateRepositoryHibernateImpl implements CandidateRepository {

    private HibernateConnectionProvider connectionProvider;

    public CandidateRepositoryHibernateImpl(HibernateConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public CandidateEntity find(int id) {
        return null;
    }

    public List<CandidateEntity> findAll() {
        return null;
    }

    public void delete(int id) {

    }

    public void update(CandidateEntity entity) {

    }

    public void save(CandidateEntity entity) {

    }
}