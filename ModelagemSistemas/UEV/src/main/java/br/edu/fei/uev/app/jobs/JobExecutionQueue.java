package br.edu.fei.uev.app.jobs;

import java.util.ArrayList;

public class JobExecutionQueue implements AsyncJobCallback {

    private ArrayList<AsyncJob> services = new ArrayList<AsyncJob>();

    public void addService(AsyncJob asyncService) {
        services.add(asyncService);
    }

    public void run() {
        for(AsyncJob service : services)
            service.execute(this);
    }

    public void onCompleted(String serviceName) {
        System.out.println(serviceName + " has been completed");
    }

    public void onError(String serviceName, Throwable throwable) {
        System.out.println("Unable to complete " + serviceName);
        throwable.printStackTrace();
    }
}
