package br.edu.fei.uev.domain.mappers;

import br.edu.fei.uev.domain.dto.CandidateDto;
import br.edu.fei.uev.domain.entity.CandidateEntity;
import br.edu.fei.uev.domain.model.Candidate;

public class CandidateMapper {
    public static CandidateEntity mapDtoToEntity(CandidateDto candidateDto) {
        return CandidateEntity.Builder.newBuilder()
                .withId(candidateDto.id)
                .build();
    }

    public static Candidate mapEntityToModel(CandidateEntity candidateEntity) {
        return Candidate.Builder.newBuilder()
                .withId(candidateEntity.getId())
                .build();
    }
}
