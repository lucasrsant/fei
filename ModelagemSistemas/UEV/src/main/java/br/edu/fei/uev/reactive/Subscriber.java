package br.edu.fei.uev.reactive;

public interface Subscriber<T> {
    void onCompleted();
    void onError(Throwable e);
    void onNext(T t);
}
