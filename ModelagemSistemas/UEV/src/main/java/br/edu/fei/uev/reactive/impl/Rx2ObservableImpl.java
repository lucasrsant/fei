package br.edu.fei.uev.reactive.impl;

import br.edu.fei.uev.reactive.Observable;
import br.edu.fei.uev.reactive.ReactiveStreamConverter;
import br.edu.fei.uev.reactive.Subscriber;
import br.edu.fei.uev.reactive.Subscription;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class Rx2ObservableImpl<T> implements Observable<T> {

    private final io.reactivex.Observable<T> rxObservable;

    public Rx2ObservableImpl(io.reactivex.Observable<T> rxObservable) {
        this.rxObservable = rxObservable;
    }

    public Subscription subscribe(final Subscriber<T> subscriber) {
        Disposable disposable = rxObservable.subscribe(new Consumer<T>() {
            public void accept(T t) throws Exception {
                subscriber.onNext(t);
            }
        }, new Consumer<Throwable>() {
            public void accept(Throwable throwable) throws Exception {
                subscriber.onError(throwable);
            }
        }, new Action() {
            public void run() throws Exception {
                subscriber.onCompleted();
            }
        });

        return ReactiveStreamConverter.fromRx2Disposable(disposable);
    }
}
