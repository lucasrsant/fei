package br.edu.fei.uev.domain.service;

import br.edu.fei.uev.domain.dto.CandidateDto;
import retrofit2.http.GET;

public interface CandidateService {
    String RETRIEVE_CANDIDATES = "RetrieveCandidates";

    @GET(RETRIEVE_CANDIDATES)
    io.reactivex.Observable<CandidateDto> retrieveCandidates();
}