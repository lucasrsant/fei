package br.edu.fei.uev.app.jobs;

public interface AsyncJob {
    void execute(AsyncJobCallback serviceCallback);
}