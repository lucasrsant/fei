package br.edu.fei.uev.app.jobs;

import br.edu.fei.uev.app.di.Injector;
import br.edu.fei.uev.app.jobs.impl.RetrieveCandidatesJob;

public class InitializerJobQueue {
    public static void run() {
        JobExecutionQueue jobExecutionQueue = new JobExecutionQueue();
        jobExecutionQueue.addService(new RetrieveCandidatesJob(Injector.providesRetrieveCandidatesUseCase()));
        jobExecutionQueue.run();
    }
}
