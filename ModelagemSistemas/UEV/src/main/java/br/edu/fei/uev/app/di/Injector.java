package br.edu.fei.uev.app.di;

import br.edu.fei.uev.domain.repository.CandidateRepository;
import br.edu.fei.uev.domain.service.CandidateService;
import br.edu.fei.uev.domain.usecase.ExecutionSchedulers;
import br.edu.fei.uev.domain.usecase.RetrieveCandidatesUseCase;
import br.edu.fei.uev.library.repository.HibernateConnectionProvider;
import br.edu.fei.uev.library.repository.candidate.CandidateRepositoryHibernateImpl;
import br.edu.fei.uev.library.usecase.DefaultExecutionSchedulers;
import br.edu.fei.uev.library.usecase.RetrieveCandidatesUseCaseImpl;
import retrofit2.Retrofit;

import java.util.HashMap;

public class Injector {

    private static HashMap<Class<?>, Object> dependencies = new HashMap<Class<?>, Object>();

    public static <T> T getDependency(Class<T> clazz) {
        if(dependencies.containsKey(clazz))
            return (T)dependencies.get(clazz);

        throw new DependencyNotInitializedException(clazz.getName());
    }

    public static RetrieveCandidatesUseCase providesRetrieveCandidatesUseCase() {
        return new RetrieveCandidatesUseCaseImpl(getDependency(ExecutionSchedulers.class),
                providesCandidateService(),
                providesCandidateRepository());
    }

    private static CandidateService providesCandidateService() {
        return getDependency(Retrofit.class).create(CandidateService.class);
    }

    private static CandidateRepository providesCandidateRepository() {
        return new CandidateRepositoryHibernateImpl(HibernateConnectionProvider.getProviderInstance());
    }

    private static void initializeSingletonDependencies() {
        dependencies.put(ExecutionSchedulers.class, new DefaultExecutionSchedulers());
        dependencies.put(Retrofit.class, providesRetrofit());
    }

    private static Retrofit providesRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://localhost")
                .build();
    }
}