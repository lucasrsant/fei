package br.edu.fei.uev.app.jobs.impl;

import br.edu.fei.uev.app.jobs.AsyncJob;
import br.edu.fei.uev.app.jobs.AsyncJobCallback;
import br.edu.fei.uev.domain.model.Candidate;
import br.edu.fei.uev.domain.usecase.RetrieveCandidatesUseCase;
import br.edu.fei.uev.reactive.Subscriber;

public class RetrieveCandidatesJob implements AsyncJob {

    private final static String JOB_NAME = "RetrieveCandidatesJob";
    private final RetrieveCandidatesUseCase retrieveCandidatesUseCase;

    public RetrieveCandidatesJob(RetrieveCandidatesUseCase retrieveCandidatesUseCase) {
        this.retrieveCandidatesUseCase = retrieveCandidatesUseCase;
    }

    public void execute(final AsyncJobCallback serviceCallback) {
        retrieveCandidatesUseCase.execute().subscribe(new Subscriber<Candidate>() {
            public void onCompleted() {
                serviceCallback.onCompleted(JOB_NAME);
            }

            public void onError(Throwable e) {
                serviceCallback.onError(JOB_NAME, e);
            }

            public void onNext(Candidate candidate) {

            }
        });
    }
}
