package br.edu.fei.uev.library.usecase;

import br.edu.fei.uev.domain.dto.CandidateDto;
import br.edu.fei.uev.domain.entity.CandidateEntity;
import br.edu.fei.uev.domain.mappers.CandidateMapper;
import br.edu.fei.uev.domain.model.Candidate;
import br.edu.fei.uev.domain.repository.CandidateRepository;
import br.edu.fei.uev.domain.service.CandidateService;
import br.edu.fei.uev.domain.usecase.ExecutionSchedulers;
import br.edu.fei.uev.domain.usecase.RetrieveCandidatesUseCase;
import br.edu.fei.uev.reactive.Observable;
import br.edu.fei.uev.reactive.ReactiveStreamConverter;

import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class RetrieveCandidatesUseCaseImpl implements RetrieveCandidatesUseCase {

    private final CandidateService candidateService;
    private final CandidateRepository candidateRepository;
    private final ExecutionSchedulers schedulers;

    public RetrieveCandidatesUseCaseImpl(ExecutionSchedulers schedulers, CandidateService candidateService, CandidateRepository candidateRepository) {
        this.candidateService = candidateService;
        this.candidateRepository = candidateRepository;
        this.schedulers = schedulers;
    }

    public Observable<Candidate> execute() {
        return ReactiveStreamConverter.fromRx2Observable(candidateService.retrieveCandidates()
            .map(new Function<CandidateDto, CandidateEntity>() {
                public CandidateEntity apply(CandidateDto candidateDto) throws Exception {
                    return CandidateMapper.mapDtoToEntity(candidateDto);
                }
            })
            .flatMap(new Function<CandidateEntity, ObservableSource<Candidate>>() {
                public ObservableSource<Candidate> apply(CandidateEntity candidateEntity) throws Exception {
                    candidateRepository.save(candidateEntity);
                    return io.reactivex.Observable.just(CandidateMapper.mapEntityToModel(candidateEntity));
                }
            })
            .subscribeOn(schedulers.getSubscribeScheduler())
            .observeOn(schedulers.getObserveScheduler()));
    }
}