1) /home/lucassantos. Os diretórios dos usuários ficam localizados na pasta /home
2a) pwd --> /home/lucassantos
2b) 
	mkdir resumos
	mkdir resumos/aulas
	mkdir resumos/aulas/A
	mkdir resumos/aulas/B
	mkdir resumos/provas
	mkdir resumos/provas/P1
	mkdir resumos/provas/P2

2c) tree (ver arquivo em anexo image001.jpg)
2d) 
	vim aula1.txt :wq
	vim aula2.txt :wq
	vim provas1.txt :wq
	vim p1-1.txt :wq

2e) 
	find / -name "aula1.txt"
	find / -name "aula2.txt"
	find / -name "provas1.txt"
	find / -name "p1-1.txt"

2f)
	which ping --> /bin/ping (usado para localizar o binário de um programa)
	whereis ping --> /bin/ping /usr/share/man/man8/ping.8.gz (usado para achar o binário, o manual page e o source quando disponível)

2g)
	rmdir resumos/aulas/A
	rm -rf resumos/aulas

2h) 
	mkdir resumos/aula1
	mkdir resumos/aula1/aula01
	mkdir resumos/aula1/aula02

2i)
	mv resumos/provas resumos/aula1/aula01 (ver imagens em anexo image002.png e image003.png)

2j) 
	mkdir resumos/Exerc
	cp -r resumos/aula1/aula01 resumos/Exerc

2k) du --> mostra o tamanho e o caminho relativo de todos os arquivos abaixo do diretorio atual recursivamente
2l) touch file.txt --> cria um arquivo vazio chamado file.txt
2m)
	time [program] --> mostra o tempo que [program] levou para ser executado
	/proc --> pseudo-arquivos com informações sobre o sistema
	df --> mostra informações sobre os discos montados
	who --> mostra todos os usuários logados no sistema e que horas eles logaram


