//Implementation of class Person
#include "Person.h"

using namespace std;

Person::Person()
{

}
		
Person::Person(string name, string cpf)
{
	_name = name;
	_cpf = cpf;
}

void Person::setName(string name)
{
	_name = name;
}

void Person::setCPF(string cpf)
{
	_cpf = cpf;
}

string Person::getName()
{
	return _name;
}

string Person::getCPF()
{
	return _cpf;
}

void Person::print()
{
	cout << "Name: " << getName() << endl << "CPF: " << getCPF() << endl;
}