#ifndef __GRADE_MENU__
#define __GRADE_MENU__

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "Subject.h"
#include "SubjectDB.h"
#include "Defines.h"

using std::cin;
using std::cout;
using std::endl;

class SubjectMenu
{
	private:
		char readOption();

	public:
		void printMenu();	
};

#endif