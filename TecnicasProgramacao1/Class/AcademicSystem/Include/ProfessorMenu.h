#ifndef __PROFESSOR_MENU__
#define __PROFESSOR_MENU__

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "Professor.h"
#include "ProfessorDB.h"
#include "Defines.h"

using std::cin;
using std::cout;
using std::endl;

class ProfessorMenu
{
	private:
		char readOption();

	public:
		void printMenu();	
};

#endif