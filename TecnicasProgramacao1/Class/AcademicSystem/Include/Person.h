#ifndef __PERSON__
#define __PERSON__

#include <string>

using std::string;

class Person
{
	private:
		string _name;
		string _cpf;
	public:
		Person();
		Person(string, string);

		void setName(string);
		void setCPF(string);

		string getName();
		string getCPF();
};

#endif