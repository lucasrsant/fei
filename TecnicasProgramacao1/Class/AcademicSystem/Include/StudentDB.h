#ifndef __STUDENT_DB__
#define __STUDENT_DB__

#include <string>
#include <cstring>
#include <cstdlib>
#include "BaseDB.h"
#include "Student.h"
#include "Defines.h"

using std::string;

class StudentDB : public BaseDB
{
	static StudentDB *__instance;

	private:
		StudentDB();
		Student** _database;
		int _quantity;
		int binarySearch(int, int, string);

	public:
		static StudentDB* getInstance();

		int addStudent(Student*);
		void removeStudent(string);
		int getQuantity();
		Student* findStudent(string);
		//void listStudents(Student**);
		Student** listStudents();

		void printAllRecords();

};

#endif