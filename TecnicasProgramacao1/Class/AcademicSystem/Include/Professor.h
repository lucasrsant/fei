#ifndef __PROFESSOR__
#define __PROFESSOR__

#include "Person.h"
#include "Defines.h"
#include "Subject.h"
#include <string>
#include <iostream>
#include <cstdlib>

using std::string;
using std::cout;
using std::endl;

class Subject;

class Professor : public Person
{
	private:
		string _position;
		int _totalSubjectsTeaching;
		Subject** _subjectsTeaching;
		int getSubjectIndex(string);
		int subjectBinarySearch(int, int, string);
	public:
		//Constructors
		Professor(string, string, string);
		Professor();

		//Misc
		void print();
		void printSimple();
		void addSubject(Subject*);
		void removeSubject(Subject*);

		Subject** listSubjectsTeaching();

		//Getters and Setters
		void setPosition(string);
		string getPosition();

		int getTotalSubjectsTeaching();
};

#endif