#ifndef __MAINMENU_HEADER__
#define __MAINMENU_HEADER__

#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <limits>

#include "StudentMenu.h"
#include "ProfessorMenu.h"
#include "SubjectMenu.h"
#include "EnrollMenu.h"
#include "ProfessorEnrollMenu.h"

#include "BaseDB.h"
#include "StudentDB.h"
#include "ProfessorDB.h"

using std::cout;
using std::cin;
using std::endl;
using std::setw;

class MainMenu
{
	public:
		char printMenu();
		void printStudentMenu();
		void printProfessorMenu();
		void printSubjectMenu();
		void printEnrollMenu();
		void printProfessorEnrollMenu();
};

#endif