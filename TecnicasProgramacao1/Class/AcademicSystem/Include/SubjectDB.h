#ifndef __SUBJECT_DB__
#define __SUBJECT_DB__

#include "Defines.h"
#include "Subject.h"
#include <string>

using std::string;
class SubjectDB
{
	static SubjectDB* __instance;

	private:
		SubjectDB();
		Subject** _database;
		int _quantity;
		int binarySearch(int, int, string);

	public:
		static SubjectDB* getInstance();
		int addSubject(Subject*);
		void removeSubject(string);
		Subject* findSubject(string);

		Subject** listSubjects();
		int getQuantity();
};

#endif