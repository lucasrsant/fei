#ifndef __PROFESSOR_DB__
#define __PROFESSOR_DB__

#include <string>
#include <cstring>
#include <cstdlib>
#include "BaseDB.h"
#include "Professor.h"
#include "Defines.h"

using std::string;

class ProfessorDB : public BaseDB
{
	static ProfessorDB *__instance;

	private:
		ProfessorDB();
		Professor** _database;
		int _quantity;
		int binarySearch(int, int, string);

	public:
		static ProfessorDB* getInstance();

		int addProfessor(Professor*);
		void removeProfessor(string);
		int getQuantity();

		Professor* findProfessor(string);		
		Professor** listProfessors();

		void printAllRecords();
};

#endif