#ifndef __STUDENT_GRADES__
#define __STUDENT_GRADES__

#include <string>
#include "Subject.h"

class Subject;

class StudentGrade
{
	private:
		Subject* _subject;
		float _p1;
		float _p2;
		float _factor;
		float _avg;

		void setSubject(Subject* subject);
		void setP1(float p1);
		void setP2(float p2);
		void setFactor(float factor);
		void calculateAverage();

	public:
		StudentGrade(Subject*, float, float, float);
		Subject* getSubject();

		float getP1();
		float getP2();
		float getFactor();
		float getAverage();

};

#endif