#ifndef __STUDENT__
#define __STUDENT__

#include "Person.h"
#include "Defines.h"
#include "Subject.h"
#include "StudentGrade.h"
#include <string>
#include <iostream>
#include <cstdlib>

using std::string;
using std::cout;
using std::endl;

class Subject;
class StudentGrade;

class Student : public Person
{
	private:
		string _course;
		int _totalSubjectsEnrolled;
		int _totalStudentGrades;
		Subject** _subjectsEnrolled;
		StudentGrade** _studentGrades;
		int getSubjectIndex(string);
		int subjectBinarySearch(int, int, string);

		int getGradeIndex(string subjectCode);
		int gradeBinarySearch(int, int, string);
	public:
		//Constructors
		Student(string, string, string);
		Student();

		//Misc
		void print();
		void printSimple();
		void addSubject(Subject*);
		void removeSubject(Subject*);
		void addGrade(StudentGrade*);
	
		int getTotalSubjectsEnrolled();
		int getTotalGrades();
		bool isEnrolledToSubject(string);

		void removeGrade(string);

		Subject** listSubjectsEnrolled();
		StudentGrade** listGrades();

		//Getters and Setters
		void setCourse(string);
		string getCourse();
};

#endif