#ifndef __BASE_DB__
#define __BASE_DB__

class BaseDB
{
	public:
		virtual void printAllRecords() = 0;
};

#endif