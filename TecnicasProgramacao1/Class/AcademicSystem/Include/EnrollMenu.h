#ifndef __ENROLL_MENU__
#define __ENROLL_MENU__

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "Student.h"
#include "StudentDB.h"
#include "Subject.h"
#include "SubjectDB.h"
#include "Defines.h"

using std::cin;
using std::cout;
using std::endl;

class EnrollMenu
{
	private:
		char readOption();

	public:
		void printMenu();	
};

#endif