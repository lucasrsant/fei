#ifndef __STUDENT_MENU__
#define __STUDENT_MENU__

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "Student.h"
#include "StudentDB.h"
#include "StudentGrade.h"
#include "SubjectDB.h"
#include "Defines.h"

using std::cin;
using std::cout;
using std::endl;

class StudentMenu
{
	private:
		char readOption();

	public:
		void printMenu();	
};

#endif