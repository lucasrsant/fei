#ifndef __SUBJECT__
#define __SUBJECT__

#include <string>
#include "Defines.h"
#include "Student.h"
#include "Professor.h"

using std::string;

class Student;
class Professor;

class Subject
{
	public:
		Subject(string, string);

		void setCode(string);
		void setDescription(string);

		string getCode();
		string getDescription();

		void addStudent(Student*);
		void removeStudent(Student*);

		void addProfessor(Professor*);
		void removeProfessor(Professor*);

		void print();

		Student** listStudentsEnrolled();
		Professor** listProfessorsTeaching();
		int getTotalStudentsEnrolled();
		int getTotalProfessorsTeaching();

	private:
		string _code;
		string _description;
		Student** _studentsEnrolled;
		Professor** _professorsTeaching;
		int _totalStudentsEnrolled;
		int _totalProfessorsTeaching;
		int getStudentIndex(string);
		int getProfessorIndex(string);
		int studentBinarySearch(int, int, string);
		int professorBinarySearch(int, int, string);
};

#endif