#ifndef __PROFESSOR_ENROLL_MENU__
#define __PROFESSOR_ENROLL_MENU__

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "Professor.h"
#include "ProfessorDB.h"
#include "Subject.h"
#include "SubjectDB.h"
#include "Defines.h"

using std::cin;
using std::cout;
using std::endl;

class ProfessorEnrollMenu
{
	private:
		char readOption();

	public:
		void printMenu();	
};

#endif