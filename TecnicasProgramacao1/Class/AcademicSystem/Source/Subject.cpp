#include "Subject.h"

Subject::Subject(string code, string description)
{
	_totalStudentsEnrolled = 0;
	_studentsEnrolled = 0;

	_totalProfessorsTeaching = 0;
	_professorsTeaching = 0;
	setCode(code);
	setDescription(description);
}

void Subject::setDescription(string description)
{
	_description = description;
}

void Subject::setCode(string code)
{
	_code = code;
}

string Subject::getCode()
{
	return _code;
}

string Subject::getDescription()
{
	return _description;
}

void Subject::print()
{
	cout << "\tCode: " << getCode() << endl;
	cout << "\tDescription: " << getDescription() << endl;
}

Student** Subject::listStudentsEnrolled()
{
	return _studentsEnrolled;
}

int Subject::getTotalStudentsEnrolled()
{
	return _totalStudentsEnrolled;
}

Professor** Subject::listProfessorsTeaching()
{
	return _professorsTeaching;
}

int Subject::getTotalProfessorsTeaching()
{
	return _totalProfessorsTeaching;
}

int Subject::studentBinarySearch(int start, int end, string key)
{
	if(end == start)
		return -1;

	int pivot = (end + start) / 2;
	if(_studentsEnrolled[pivot]->getCPF() == key)
		return pivot;

	if(_studentsEnrolled[pivot]->getCPF().compare(key) > 0)
		return studentBinarySearch(start, pivot, key);
	else
		return studentBinarySearch(pivot + 1, end, key);
}

int Subject::professorBinarySearch(int start, int end, string key)
{
	if(end == start)
		return -1;

	int pivot = (end + start) / 2;
	if(_professorsTeaching[pivot]->getCPF() == key)
		return pivot;

	if(_professorsTeaching[pivot]->getCPF().compare(key) > 0)
		return professorBinarySearch(start, pivot, key);
	else
		return professorBinarySearch(pivot + 1, end, key);
}

int Subject::getStudentIndex(string cpf)
{
	return studentBinarySearch(0, _totalStudentsEnrolled, cpf);
}

int Subject::getProfessorIndex(string cpf)
{
	return professorBinarySearch(0, _totalProfessorsTeaching, cpf);
}

void Subject::removeStudent(Student* s)
{
	if(_studentsEnrolled)
	{
		int studentIndex = getStudentIndex(s->getCPF());

		if(studentIndex >= 0)
		{
			for(int i = studentIndex; i < _totalStudentsEnrolled; i++)
			{
				_studentsEnrolled[i] = _studentsEnrolled[i + 1];
			}

			_totalStudentsEnrolled--;
		}
	}
}

void Subject::removeProfessor(Professor* p)
{
	if(_professorsTeaching)
	{
		int professorIndex = getProfessorIndex(p->getCPF());

		if(professorIndex >= 0)
		{
			for(int i = professorIndex; i < _totalProfessorsTeaching; i++)
			{
				_professorsTeaching[i] = _professorsTeaching[i + 1];
			}

			_totalProfessorsTeaching--;
		}
	}
}

void Subject::addStudent(Student* s)
{
	if(_studentsEnrolled)
	{
		int studentIndex = getStudentIndex(s->getCPF());

		if(studentIndex == -1)
		{
			if(_totalStudentsEnrolled >= MAX)
				return; //The students are full

			int i;
			for(i = 0; i < _totalStudentsEnrolled && s->getCPF().compare(_studentsEnrolled[i]->getCPF()) > 0; i++)
			{
				//do nothing
			}

			int j;
			for(j = _totalStudentsEnrolled; j > i; j--)
				_studentsEnrolled[j] = _studentsEnrolled[j - 1];

			_studentsEnrolled[i] = s;
			_totalStudentsEnrolled++;
		}
	}
	else
	{
		_studentsEnrolled = (Student**)calloc(sizeof(Student*) * MAX, 1);
		_studentsEnrolled[0] = s;
		_totalStudentsEnrolled++;
	}
}

void Subject::addProfessor(Professor* p)
{
	if(_professorsTeaching)
	{
		int professorIndex = getProfessorIndex(p->getCPF());

		if(professorIndex == -1)
		{
			if(_totalProfessorsTeaching >= MAX)
				return; //The professors are full

			int i;
			for(i = 0; i < _totalProfessorsTeaching && p->getCPF().compare(_professorsTeaching[i]->getCPF()) > 0; i++)
			{
				//do nothing
			}

			int j;
			for(j = _totalProfessorsTeaching; j > i; j--)
				_professorsTeaching[j] = _professorsTeaching[j - 1];

			_professorsTeaching[i] = p;
			_totalProfessorsTeaching++;
		}
	}
	else
	{
		_professorsTeaching = (Professor**)calloc(sizeof(Professor*) * MAX, 1);
		_professorsTeaching[0] = p;
		_totalProfessorsTeaching++;
	}
}