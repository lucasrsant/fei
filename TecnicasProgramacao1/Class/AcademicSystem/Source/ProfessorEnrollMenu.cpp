#include "ProfessorEnrollMenu.h"

char ProfessorEnrollMenu::readOption()
{
	cout << "Choose a professor enroll option:" << endl;
	cout << "\t1 - Add a professor to a subject" << endl;
	cout << "\t2 - Remove a professor from subject" << endl;
	cout << "\t3 - Return to main menu" << endl;

	char opt = cin.get();
	cin.sync();
	return opt;
}

void addProfessorToSubject()
{
	string professorCPF;
	string subjectCode;

	cout << "Input the professor CPF:" << endl;
	getline(cin, professorCPF);

	cout << "Input the subject code:" << endl;
	getline(cin, subjectCode);

	ProfessorDB* professorDB = ProfessorDB::getInstance();
	SubjectDB* subjectDB = SubjectDB::getInstance();

	Professor* professor = professorDB->findProfessor(professorCPF);
	Subject* subject = subjectDB->findSubject(subjectCode);

	if(!professor)
	{
		cout << "Professor not foud" << endl;
		return;
	}

	if(!subject)
	{
		cout << "Subject not found" << endl;
		return;
	}

	professor->addSubject(subject);
	subject->addProfessor(professor);

	cout << "The professor " << professor->getName() << " now teaches the subject " << subject->getDescription() << "." << endl;
}

void removeProfessorFromSubject()
{
	string professorCPF;
	string subjectCode;

	cout << "Input the professor CPF:" << endl;
	getline(cin, professorCPF);

	cout << "Input the subject code:" << endl;
	getline(cin, subjectCode);

	ProfessorDB* professorDB = ProfessorDB::getInstance();
	SubjectDB* subjectDB = SubjectDB::getInstance();

	Professor* professor = professorDB->findProfessor(professorCPF);
	Subject* subject = subjectDB->findSubject(subjectCode);

	if(!professor)
	{
		cout << "Professor not foud" << endl;
		return;
	}

	if(!subject)
	{
		cout << "Subject not found" << endl;
		return;
	}

	professor->removeSubject(subject);
	subject->removeProfessor(professor);

	cout << "The professor " << professor->getName() << " does not teach the subject " << subject->getDescription() << " anymore." << endl;
}

void ProfessorEnrollMenu::printMenu()
{
	char opt;
	do
	{
		opt = readOption();

		switch(opt)
		{
			case '1':
				addProfessorToSubject();
				break;
			case '2':
				removeProfessorFromSubject();
				break;
			case '3':
				break;
			break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != '3');
}