#include "SubjectDB.h"

SubjectDB *SubjectDB::__instance = 0;

SubjectDB::SubjectDB()
{
	_quantity = 0;
	_database = (Subject**)calloc(sizeof(Subject*) * MAX, 1);
}

SubjectDB* SubjectDB::getInstance()
{
	if(!__instance)
		__instance = new SubjectDB;

	return __instance;
}

int SubjectDB::getQuantity()
{
	return _quantity;
}

int SubjectDB::binarySearch(int start, int end, string code)
{
	if(end == start)
		return -1;//Not found

	int pivot = (end + start) / 2;
	if(_database[pivot]->getCode() == code)
		return pivot;//Found!

	if(_database[pivot]->getCode().compare(code) > 0)
		return binarySearch(start, pivot, code);
	else
		return binarySearch(pivot + 1, end, code);
}

Subject* SubjectDB::findSubject(string code)
{
	int subjectIndex = binarySearch(0, _quantity, code);

	if(subjectIndex > -1)
		return _database[subjectIndex];

	return NULL;
}


//0 for success
//-1 for error
int SubjectDB::addSubject(Subject* subject)
{
	if(_quantity >= MAX)
		return -1; //The database is full

	int i;
	for(i = 0; i < _quantity && subject->getCode().compare(_database[i]->getCode()) > 0; i++)
	{
		//do nothing
	}

	int j;
	for(j = _quantity; j > i; j--)
		_database[j] = _database[j - 1];

	_database[i] = subject;
	_quantity++;

	return 0;
}

Subject** SubjectDB::listSubjects()
{
	return _database;
}

void SubjectDB::removeSubject(string code)
{
	int subjectIndex = binarySearch(0, _quantity, code);

	if(subjectIndex > -1)
	{
		for(int i = subjectIndex; i < _quantity; i++)
			_database[i] = _database[i + 1];

		_quantity--;
	}
}