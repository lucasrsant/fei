#include "StudentDB.h"

StudentDB *StudentDB::__instance = 0;

StudentDB::StudentDB()
{
	_quantity = 0;
	_database = (Student**)calloc(sizeof(Student*) * MAX, 1);
}

StudentDB* StudentDB::getInstance()
{
	if(!__instance)
		__instance = new StudentDB;

	return __instance;
}

int StudentDB::binarySearch(int start, int end, string cpf)
{
	if(end == start)
		return -1; //Not found

	int pivot = (end + start) / 2;
	if(_database[pivot]->getCPF() == cpf)
		return pivot; //Found!

	if(_database[pivot]->getCPF().compare(cpf) > 0)
		return binarySearch(start, pivot, cpf);
	else
		return binarySearch(pivot + 1, end, cpf);
}

Student* StudentDB::findStudent(string cpf)
{
	int studentIndex = binarySearch(0, _quantity, cpf);

	if(studentIndex > -1)
		return _database[studentIndex];

	return NULL;
}

//0 for success
//-1 for full database
//-2 for cpf already registered
int StudentDB::addStudent(Student* s)
{
	if(_quantity >= MAX)
		return -1; //The database is full

	if(findStudent(s->getCPF()) != NULL)
		return -2; //CPF already registered

	int i;
	for(i = 0; i < _quantity && s->getCPF().compare(_database[i]->getCPF()) > 0; i++)
	{
		//do nothing
	}

	int j;
	for(j = _quantity; j > i; j--)
		_database[j] = _database[j - 1];

	_database[i] = s;
	_quantity++;

	return 0;
}

void StudentDB::removeStudent(string cpf)
{
	int studentIndex = binarySearch(0, _quantity, cpf);

	if(studentIndex > -1)
	{
		for(int i = studentIndex; i < _quantity; i++)
			_database[i] = _database[i + 1];

		_quantity--;
	}
}

int StudentDB::getQuantity()
{
	return _quantity;
}

Student** StudentDB::listStudents()
{
	return _database;
}

void StudentDB::printAllRecords()
{
	for(int i = 0; i < _quantity; i++)
	{
		Student* s = _database[i];
		s->print();
	}
}