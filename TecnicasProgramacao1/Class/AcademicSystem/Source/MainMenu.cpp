#include "MainMenu.h"

char MainMenu::printMenu()
{
	cout << "Choose an option: " << endl;
	cout << "\t1 - Student menu" << endl;
	cout << "\t2 - Professor menu" << endl;
	cout << "\t3 - Subject menu" << endl;
	cout << "\t4 - Enroll menu" << endl;
	cout << "\t5 - Professor enroll menu" << endl;
	cout << "\t6 - Print all professors and students" << endl;
	cout << "\t7 - Quit" << endl;

	char opt = cin.get();
	cin.sync();
	return opt;
}

void MainMenu::printStudentMenu()
{
	StudentMenu studentMenu;
	studentMenu.printMenu();
}

void MainMenu::printProfessorMenu()
{
	ProfessorMenu professorMenu;
	professorMenu.printMenu();
}

void MainMenu::printSubjectMenu()
{
	SubjectMenu subjectMenu;
	subjectMenu.printMenu();
}

void MainMenu::printEnrollMenu()
{
	EnrollMenu enrollMenu;
	enrollMenu.printMenu();
}

void MainMenu::printProfessorEnrollMenu()
{
	ProfessorEnrollMenu professorEnrollMenu;
	professorEnrollMenu.printMenu();
}

int main(int argc, char** argv)
{
	MainMenu menu;
	char opt;
	do
	{
		opt = menu.printMenu();

		switch(opt)
		{
			case '1':
				menu.printStudentMenu();
				break;
			case '2':
				menu.printProfessorMenu();
				break;
			case '3':
				menu.printSubjectMenu();
				break;
			case '4':
				menu.printEnrollMenu();
				break;
			case '5':
				menu.printProfessorEnrollMenu();
				break;
			case '6':
			{
				BaseDB* professorDB = ProfessorDB::getInstance();
				BaseDB* studentDB = StudentDB::getInstance();

				cout << "Professors:" << endl;
				professorDB->printAllRecords();

				cout << endl << endl;
				cout << "Students:" <<endl;

				studentDB->printAllRecords();

				break;
			}
			case '7':
				break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != '7');

	return 0;
}