#include "Professor.h"

Professor::Professor() : Person()
{
	_totalSubjectsTeaching = 0;
	_subjectsTeaching = 0;
}

Professor::Professor(string name, string cpf, string position) : Person(name, cpf)
{
	_totalSubjectsTeaching = 0;
	_subjectsTeaching = 0;
	setPosition(position);
}

void Professor::setPosition(string position)
{
	_position = position;
}

string Professor::getPosition()
{
	return _position;
}

int Professor::subjectBinarySearch(int start, int end, string key)
{
	if(end == start)
		return -1;

	int pivot = (end + start) / 2;
	if(_subjectsTeaching[pivot]->getCode() == key)
		return pivot;

	if(_subjectsTeaching[pivot]->getCode().compare(key) > 0)
		return subjectBinarySearch(start, pivot, key);
	else
		return subjectBinarySearch(pivot + 1, end, key);
}

int Professor::getSubjectIndex(string subjectCode)
{
	return subjectBinarySearch(0, _totalSubjectsTeaching, subjectCode);
}

void Professor::removeSubject(Subject* subject)
{
	if(_subjectsTeaching)
	{
		int subjectIndex = getSubjectIndex(subject->getCode());

		if(subjectIndex >= 0)
		{
			for(int i = subjectIndex; i < _totalSubjectsTeaching; i++)
			{
				_subjectsTeaching[i] = _subjectsTeaching[i + 1];
			}

			_totalSubjectsTeaching--;
		}
	}
}

void Professor::addSubject(Subject* g)
{
	if(_subjectsTeaching)
	{
		int subjectIndex = getSubjectIndex(g->getCode());

		if(subjectIndex == -1)
		{
			if(_totalSubjectsTeaching >= MAX)
				return; //The subjects are full

			int i;
			for(i = 0; i < _totalSubjectsTeaching && g->getCode().compare(_subjectsTeaching[i]->getCode()) > 0; i++)
			{
				//do nothing
			}

			int j;
			for(j = _totalSubjectsTeaching; j > i; j--)
				_subjectsTeaching[j] = _subjectsTeaching[j - 1];

			_subjectsTeaching[i] = g;
			_totalSubjectsTeaching++;
		}
	}
	else
	{
		_subjectsTeaching = (Subject**)calloc(sizeof(Subject*) * MAX, 1);
		_subjectsTeaching[0] = g;
		_totalSubjectsTeaching++;
	}
}

void Professor::print()
{
	cout << "\tName: " << getName() << endl;
	cout << "\tCPF: " << getCPF() << endl;
	cout << "\tPosition: " << getPosition() << endl;
}

void Professor::printSimple()
{
	cout << "\tName: " << getName() << endl;
}

Subject** Professor::listSubjectsTeaching()
{
	return _subjectsTeaching;
}

int Professor::getTotalSubjectsTeaching()
{
	return _totalSubjectsTeaching;
}