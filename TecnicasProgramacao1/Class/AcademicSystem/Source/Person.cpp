#include "Person.h"

Person::Person(){ }

Person::Person(string name, string cpf)
{
	setName(name);
	setCPF(cpf);
}

void Person::setName(string name)
{
	_name = name;
}

void Person::setCPF(string cpf)
{
	_cpf = cpf;
}

string Person::getName()
{
	return _name;
}

string Person::getCPF()
{
	return _cpf;
}