#include "StudentMenu.h"

char StudentMenu::readOption()
{
	cout << "Choose a student option:" << endl;
	cout << "\t1 - Insert" << endl;
	cout << "\t2 - Delete" << endl;
	cout << "\t3 - Search" << endl;
	cout << "\t4 - List" << endl;
	cout << "\t5 - Add grade" << endl;
	cout << "\t6 - Remove grade" << endl; 
	cout << "\t7 - Show school records" << endl;
	cout << "\t8 - Return to main menu" << endl;

	char opt = cin.get();
	cin.sync();
	return opt;
}

void insertStudent()
{
	string name;
	string cpf;
	string course;

	cout << "Insert the student name:" << endl;
	getline(cin, name);

	cout << "Insert the student CPF:" << endl;
	getline(cin, cpf);

	cout << "Insert the student course:" << endl;
	getline(cin, course);

	Student* s = new Student(name, cpf, course);
	StudentDB* db = StudentDB::getInstance();
	int result = db->addStudent(s);

	switch(result)
	{
		case 0:
			cout << "The student was added successfully" << endl;
			break;
		case -1:
			cout << "The database is full" << endl;
			break;
		case -2:
			cout << "This CPF is already registered" <<endl;
			break;
		default:
			break;
	}
}

void printStudentSubjects(Student* s)
{
	int subjectsEnrolled = s->getTotalSubjectsEnrolled();

	if(subjectsEnrolled > 0)
	{
		Subject** subjectsList = s->listSubjectsEnrolled();

		for(int j = 0; j < subjectsEnrolled; j++)
		{
			Subject* subject = subjectsList[j];
			subject->print();
		}
	}
	else
	{
		cout << "\t***This student are not enrolled to any subject***" << endl;
	}
}

void listStudents()
{
	StudentDB* db = StudentDB::getInstance();
	int totalStudents = db->getQuantity();

	//Student** studentsList = (Student**)calloc(sizeof(Student*) * totalStudents, 1);
	Student** studentsList = db->listStudents();

	cout << endl;

	for(int i = 0; i < totalStudents; i++)
	{
		if(studentsList[i])
		{
			Student* s = studentsList[i];

			cout << "Student " << i + 1 << endl;
			s->print();
			printStudentSubjects(s);
		}
	}

	cout << endl;
}

void searchStudent()
{
	string cpf;
	cout << "Input the CPF: " << endl;
	getline(cin, cpf);

	StudentDB* db = StudentDB::getInstance();
	Student* s = db->findStudent(cpf);

	if(!s)
	{
		cout << "No results found" << endl;
	}
	else
	{
		s->print();
		printStudentSubjects(s);
	}
}

void deleteStudent()
{
	string cpf;
	cout << "Input the CPF: " << endl;
	getline(cin, cpf);

	StudentDB* studentDB = StudentDB::getInstance();
	Student* s = studentDB->findStudent(cpf);

	Subject** subjectsList = s->listSubjectsEnrolled();
	int totalSubjectsEnrolled = s->getTotalSubjectsEnrolled();

	for(int i = 0; i < totalSubjectsEnrolled; i++)
	{
		Subject* subject = subjectsList[i];
		subject->removeStudent(s);		
	}

	studentDB->removeStudent(s->getCPF());

	cout << "The student has been deleted" << endl;
}

void addGrades()
{
	string cpf;
	string subjectCode;

	cout << "Input the student CPF: " << endl;
	getline(cin, cpf);

	cout << "Input the subject code: " << endl;
	getline(cin, subjectCode);

	StudentDB* studentDB = StudentDB::getInstance();
	Student* student = studentDB->findStudent(cpf);

	if(!student)
	{
		cout << "Student not found" << endl;
		return;
	}

	if(student->isEnrolledToSubject(subjectCode))
	{
		SubjectDB* subjectDB = SubjectDB::getInstance();
		Subject* subject = subjectDB->findSubject(subjectCode);
		if(!subject)
		{
			cout << "Subject not found" << endl;
			return;
		}

		float p1, p2, factor;
		cout << "Input P1 grade: ";
		cin >> p1;
		cin.ignore();
		cout << endl << "Input P2 grade: ";
		cin >> p2;
		cin.ignore();
		cout << endl << "Input factor: ";
		cin >> factor;
		cin.ignore();

		StudentGrade* grade = new StudentGrade(subject, p1, p2, factor);
		student->addGrade(grade); 

		cout << "The grades for subject " << subject->getDescription() << " was added successfully" << endl;
	}
	else
	{
		cout << "This student are not enrolled to this subject" << endl;
	}
}

void removeGrades()
{
	string cpf;
	string subjectCode;

	cout << "Input the student CPF: " << endl;
	getline(cin, cpf);

	cout << "Input the subject code: " << endl;
	getline(cin, subjectCode);

	StudentDB* studentDB = StudentDB::getInstance();
	Student* student = studentDB->findStudent(cpf);

	if(!student)
	{
		cout << "Student not found" << endl;
		return;
	}

	student->removeGrade(subjectCode);
	cout << "Grade has been removed successfully" << endl;
}

void showSchoolRecords()
{
	string cpf;
	cout << "Input the student CPF: " << endl;
	getline(cin, cpf);

	StudentDB* db = StudentDB::getInstance();
	Student* student = db->findStudent(cpf);

	StudentGrade** grades = student->listGrades();
	int totalGrades = student->getTotalGrades();

	if(totalGrades > 0)
	{
		cout << "\t*** SCHOOL RECORDS ***" << endl;
		cout << "\t" << student->getName() << " - " << student->getCourse() << endl << endl;
		cout << "\tSubject" << endl;
	}

	for(int i = 0; i < totalGrades; i++)
	{
		StudentGrade* g = grades[i];
		cout << "\t\t" << g->getSubject()->getDescription() << endl;
		cout << "\t\t\tP1: " << g->getP1() << endl;
		cout <<" \t\t\tP2: " << g->getP2() << endl;
		cout << "\t\t\tFactor: " << g->getFactor() << endl;
		cout << "\t\t\tAverage: " << g->getAverage() << endl << endl;
	}
}

void StudentMenu::printMenu()
{
	char opt;
	do
	{
		opt = readOption();

		switch(opt)
		{
			case '1':
				insertStudent();
				break;
			case '2':
				deleteStudent();
				break;
			case '3':
				searchStudent();
				break;
			break;
			case '4':
				listStudents();
				break;
			case '5':
				addGrades();
				break;
			case '6':
				removeGrades();
				break;
			case '7':
				showSchoolRecords();
				break;
			case '8':
				break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != '8');
}