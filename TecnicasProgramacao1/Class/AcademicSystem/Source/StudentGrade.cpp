#include "StudentGrade.h"

StudentGrade::StudentGrade(Subject* subject, float p1, float p2, float factor)
{
	setSubject(subject);
	setP1(p1);
	setP2(p2);
	setFactor(factor);

	calculateAverage();
}

void StudentGrade::setSubject(Subject* subject)
{
	_subject = subject;
}

void StudentGrade::setP1(float p1)
{
	_p1 = p1;
}

void StudentGrade::setP2(float p2)
{
	_p2 = p2;
}

void StudentGrade::setFactor(float factor)
{
	_factor = factor;
}

void StudentGrade::calculateAverage()
{
	_avg = (_p1 * 0.4f) + (_p2 * 0.6f);
	_avg *= _factor;
}

Subject* StudentGrade::getSubject()
{
	return _subject;
}

float StudentGrade::getP1()
{
	return _p1;
}

float StudentGrade::getP2()
{
	return _p2;
}

float StudentGrade::getFactor()
{
	return _factor;
}

float StudentGrade::getAverage()
{
	return _avg;;
}