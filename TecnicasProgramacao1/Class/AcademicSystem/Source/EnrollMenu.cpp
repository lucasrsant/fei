#include "EnrollMenu.h"

char EnrollMenu::readOption()
{
	cout << "Choose a enroll option:" << endl;
	cout << "\t1 - Enroll a student" << endl;
	cout << "\t2 - Unenroll a student" << endl;
	cout << "\t3 - Return to main menu" << endl;

	char opt = cin.get();
	cin.sync();
	return opt;
}

void enrollStudent()
{
	string studentCPF;
	string subjectCode;

	cout << "Input the student CPF:" << endl;
	getline(cin, studentCPF);

	cout << "Input the subject code:" << endl;
	getline(cin, subjectCode);

	StudentDB* studentDB = StudentDB::getInstance();
	SubjectDB* subjectDB = SubjectDB::getInstance();

	Student* student = studentDB->findStudent(studentCPF);
	Subject* subject = subjectDB->findSubject(subjectCode);

	if(!student)
	{
		cout << "Student not foud" << endl;
		return;
	}

	if(!subject)
	{
		cout << "Subject not found" << endl;
		return;
	}

	student->addSubject(subject);
	subject->addStudent(student);

	cout << "The student " << student->getName() << " was enrolled to the subject " << subject->getDescription() << " successfully" << endl;
}

void unenrollStudent()
{
	string studentCPF;
	string subjectCode;

	cout << "Input the student CPF:" << endl;
	getline(cin, studentCPF);

	cout << "Input the subject code:" << endl;
	getline(cin, subjectCode);

	StudentDB* studentDB = StudentDB::getInstance();
	SubjectDB* subjectDB = SubjectDB::getInstance();

	Student* student = studentDB->findStudent(studentCPF);
	Subject* subject = subjectDB->findSubject(subjectCode);

	if(!student)
	{
		cout << "Student not foud" << endl;
		return;
	}

	if(!subject)
	{
		cout << "Subject not found" << endl;
		return;
	}

	student->removeSubject(subject);
	subject->removeStudent(student);

	cout << "The student " << student->getName() << " was unenrolled from the subject " << subject->getDescription() << " successfully" << endl;
}

void EnrollMenu::printMenu()
{
	char opt;
	do
	{
		opt = readOption();

		switch(opt)
		{
			case '1':
				enrollStudent();
				break;
			case '2':
				unenrollStudent();
				break;
			case '3':
				break;
			break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != '3');
}