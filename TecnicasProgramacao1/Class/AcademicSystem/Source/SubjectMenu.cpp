#include "SubjectMenu.h"

char SubjectMenu::readOption()
{
	cout << "Choose a subject option:" << endl;
	cout << "\t1 - Insert" << endl;
	cout << "\t2 - Delete" << endl;
	cout << "\t3 - Search" << endl;
	cout << "\t4 - List" << endl;
	cout << "\t5 - Return to main menu" << endl;

	char opt = cin.get();
	cin.sync();
	return opt;
}

void insertSubject()
{
	string code;
	string description;

	cout << "Insert the subject code:" << endl;
	getline(cin, code);

	cout << "Insert the subject description:" << endl;
	getline(cin, description);

	Subject* subject = new Subject(code, description);
	SubjectDB* db = SubjectDB::getInstance();
	db->addSubject(subject);
}

void printSubjectStudents(Subject* subject)
{
	int studentsEnrolled = subject->getTotalStudentsEnrolled();

	cout << "Students enrolled:" << endl;

	if(studentsEnrolled > 0)
	{
		Student** studentsList = subject->listStudentsEnrolled();

		for(int j = 0; j < studentsEnrolled; j++)
		{
			Student* student = studentsList[j];
			student->printSimple();
		}
	}
	else
	{
		cout << "\t***There are no students enrolled to this subject***" << endl;
	}

	cout << endl;
}


void printSubjectProfessors(Subject* subject)
{
	int professorsTeaching = subject->getTotalProfessorsTeaching();

	cout << "Professors teaching:" << endl;

	if(professorsTeaching > 0)
	{
		Professor** professorsList = subject->listProfessorsTeaching();

		for(int j = 0; j < professorsTeaching; j++)
		{
			Professor* professor = professorsList[j];
			professor->printSimple();
		}
	}
	else
	{
		cout << "\t***There are no students enrolled to this subject***" << endl;
	}

	cout << endl;
}


void listSubject()
{
	SubjectDB* db = SubjectDB::getInstance();
	int totalSubjects = db->getQuantity();

	Subject** subjectsList = db->listSubjects();

	cout << endl;

	for(int i = 0; i < totalSubjects; i++)
	{
		if(subjectsList[i])
		{
			Subject* subject = subjectsList[i];

			cout << "Subject " << i + 1 << endl;
			subject->print();
			printSubjectStudents(subject);
			printSubjectProfessors(subject);
		}
	}

	cout << endl;
}

void searchSubject()
{
	string code;
	cout << "Input the Subject Code: " << endl;
	getline(cin, code);

	SubjectDB* db = SubjectDB::getInstance();
	Subject* subject = db->findSubject(code);

	if(!subject)
	{
		cout << "No results found" << endl;
	}
	else
	{
		subject->print();
		cout << "Students enrolled:" << endl;
		printSubjectStudents(subject);

		cout << endl << "Professors teaching:" << endl;
		printSubjectProfessors(subject);
	}
} 

void deleteSubject()
{
	string code;
	cout << "Input the subject code: " << endl;
	getline(cin, code);

	SubjectDB* subjectDB = SubjectDB::getInstance();
	Subject* subject = subjectDB->findSubject(code);

	Professor** professorsList = subject->listProfessorsTeaching();
	int totalProfessorsTeaching = subject->getTotalProfessorsTeaching();

	Student** studentsList = subject->listStudentsEnrolled();
	int totalStudentsEnrolled = subject->getTotalStudentsEnrolled();

	for(int i = 0; i < totalStudentsEnrolled; i++)
	{
		Student* student = studentsList[i];
		student->removeSubject(subject);
	}

	for(int i = 0; i < totalProfessorsTeaching; i++)
	{
		Professor* professor = professorsList[i];
		professor->removeSubject(subject);
	}

	subjectDB->removeSubject(subject->getCode());

	cout << "The subject has been deleted" << endl;
}

void SubjectMenu::printMenu()
{
	char opt;
	do
	{
		opt = readOption();

		switch(opt)
		{
			case '1':
				insertSubject();
				break;
			case '2':
				deleteSubject();
				break;
			case '3':
				searchSubject();
				break;
			case '4':
				listSubject();
				break;
			case '5':
			break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != '5');
}