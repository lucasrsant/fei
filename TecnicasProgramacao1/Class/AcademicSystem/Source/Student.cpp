#include "Student.h"

Student::Student() : Person()
{
	_totalSubjectsEnrolled = 0;
	_subjectsEnrolled = 0;

	_totalStudentGrades = 0;
	_studentGrades = 0;
}

Student::Student(string name, string cpf, string course) : Person(name, cpf)
{
	_totalSubjectsEnrolled = 0;
	_subjectsEnrolled = 0;

	_totalStudentGrades = 0;
	_studentGrades = 0;
	setCourse(course);
}

void Student::setCourse(string course)
{
	_course = course;
}

string Student::getCourse()
{
	return _course;
}

int Student::subjectBinarySearch(int start, int end, string key)
{
	if(end == start)
		return -1;

	int pivot = (end + start) / 2;
	if(_subjectsEnrolled[pivot]->getCode() == key)
		return pivot;

	if(_subjectsEnrolled[pivot]->getCode().compare(key) > 0)
		return subjectBinarySearch(start, pivot, key);
	else
		return subjectBinarySearch(pivot + 1, end, key);
}

int Student::gradeBinarySearch(int start, int end, string key)
{
	if(end == start)
		return -1;

	int pivot = (end + start) / 2;
	if(_studentGrades[pivot]->getSubject()->getCode() == key)
		return pivot;

	if(_studentGrades[pivot]->getSubject()->getCode().compare(key) > 0)
		return gradeBinarySearch(start, pivot, key);
	else
		return gradeBinarySearch(pivot + 1, end, key);
}

int Student::getSubjectIndex(string subjectCode)
{
	return subjectBinarySearch(0, _totalSubjectsEnrolled, subjectCode);
}

int Student::getGradeIndex(string subjectCode)
{
	return gradeBinarySearch(0, _totalStudentGrades, subjectCode);
}

void Student::removeSubject(Subject* g)
{
	if(_subjectsEnrolled)
	{
		int subjectIndex = getSubjectIndex(g->getCode());

		if(subjectIndex >= 0)
		{
			for(int i = subjectIndex; i < _totalSubjectsEnrolled; i++)
			{
				_subjectsEnrolled[i] = _subjectsEnrolled[i + 1];
			}

			_totalSubjectsEnrolled--;
		}
	}
}

void Student::addSubject(Subject* subject)
{
	if(_subjectsEnrolled)
	{
		int subjectIndex = getSubjectIndex(subject->getCode());

		if(subjectIndex == -1)
		{
			if(_totalSubjectsEnrolled >= MAX)
				return; //The subjects are full

			int i;
			for(i = 0; i < _totalSubjectsEnrolled && subject->getCode().compare(_subjectsEnrolled[i]->getCode()) > 0; i++)
			{
				//do nothing
			}

			int j;
			for(j = _totalSubjectsEnrolled; j > i; j--)
				_subjectsEnrolled[j] = _subjectsEnrolled[j - 1];

			_subjectsEnrolled[i] = subject;
			_totalSubjectsEnrolled++;
		}
	}
	else
	{
		_subjectsEnrolled = (Subject**)calloc(sizeof(Subject*) * MAX, 1);
		_subjectsEnrolled[0] = subject;
		_totalSubjectsEnrolled++;
	}
}

void Student::addGrade(StudentGrade* grade)
{
	if(_studentGrades)
	{
		if(!grade || !grade->getSubject())
			return;

		int gradeIndex = getGradeIndex(grade->getSubject()->getCode());

		if(gradeIndex == -1)
		{
			if(_totalStudentGrades >= MAX)
				return; //The grades are full

			int i;
			for(i = 0; i < _totalStudentGrades && grade->getSubject()->getCode().compare(_studentGrades[i]->getSubject()->getCode()) > 0; i++)
			{
				//do nothing
			}

			int j;
			for(j = _totalStudentGrades; j > i; j--)
				_studentGrades[j] = _studentGrades[j - 1];

			_studentGrades[i] = grade;
			_totalStudentGrades++;
		}
	}
	else
	{
		_studentGrades = (StudentGrade**)calloc(sizeof(StudentGrade*) * MAX, 1);
		_studentGrades[0] = grade;
		_totalStudentGrades++;
	}
}

void Student::print()
{
	cout << "\tName: " << getName() << endl;
	cout << "\tCPF: " << getCPF() << endl;
	cout << "\tCourse: " << getCourse() << endl;
}

void Student::printSimple()
{
	cout << "\tName: " << getName() << endl;
}

Subject** Student::listSubjectsEnrolled()
{
	return _subjectsEnrolled;
}

int Student::getTotalSubjectsEnrolled()
{
	return _totalSubjectsEnrolled;
}

bool Student::isEnrolledToSubject(string subjectCode)
{
	return getSubjectIndex(subjectCode) > -1;
}

StudentGrade** Student::listGrades()
{
	return _studentGrades;
}

int Student::getTotalGrades()
{
	return _totalStudentGrades;
}

void Student::removeGrade(string subjectCode)
{
	int gradeIndex = getGradeIndex(subjectCode);

	delete _studentGrades[gradeIndex];
	for(int i = gradeIndex; i < _totalStudentGrades; i++)
	{
		_studentGrades[i] = _studentGrades[i + 1];
	}

	_totalStudentGrades--;
}