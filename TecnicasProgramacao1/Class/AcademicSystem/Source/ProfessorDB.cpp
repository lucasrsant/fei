#include "ProfessorDB.h"

ProfessorDB *ProfessorDB::__instance = 0;

ProfessorDB::ProfessorDB()
{
	_quantity = 0;
	_database = (Professor**)calloc(sizeof(Professor*) * MAX, 1);
}

ProfessorDB* ProfessorDB::getInstance()
{
	if(!__instance)
		__instance = new ProfessorDB;

	return __instance;
}

int ProfessorDB::binarySearch(int start, int end, string cpf)
{
	if(end == start)
		return -1; //Not found

	int pivot = (end + start) / 2;
	if(_database[pivot]->getCPF() == cpf)
		return pivot; //Found!

	if(_database[pivot]->getCPF().compare(cpf) > 0)
		return binarySearch(start, pivot, cpf);
	else
		return binarySearch(pivot + 1, end, cpf);
}

Professor* ProfessorDB::findProfessor(string cpf)
{
	int professorIndex = binarySearch(0, _quantity, cpf);

	if(professorIndex > -1)
		return _database[professorIndex];

	return NULL;
}

//0 for success
//-1 for full database
//-2 for cpf already registered
int ProfessorDB::addProfessor(Professor* p)
{
	if(_quantity >= MAX)
		return -1; //The database is full

	if(findProfessor(p->getCPF()) != NULL)
		return -2; //CPF already registered

	int i;
	for(i = 0; i < _quantity && p->getCPF().compare(_database[i]->getCPF()) > 0; i++)
	{
		//do nothing
	}

	int j;
	for(j = _quantity; j > i; j--)
		_database[j] = _database[j - 1];

	_database[i] = p;
	_quantity++;

	return 0;
}

void ProfessorDB::removeProfessor(string cpf)
{
	int professorIndex = binarySearch(0, _quantity, cpf);

	if(professorIndex > -1)
	{
		for(int i = professorIndex; i < _quantity; i++)
			_database[i] = _database[i + 1];

		_quantity--;
	}
}

int ProfessorDB::getQuantity()
{
	return _quantity;
}

Professor** ProfessorDB::listProfessors()
{
	return _database;
}

void ProfessorDB::printAllRecords()
{
	for(int i = 0; i < _quantity; i++)
	{
		Professor* p = _database[i];
		p->print();
	}
}