#include "ProfessorMenu.h"

char ProfessorMenu::readOption()
{
	cout << "Choose a professor option:" << endl;
	cout << "\t1 - Insert" << endl;
	cout << "\t2 - Delete" << endl;
	cout << "\t3 - Search" << endl;
	cout << "\t4 - List" << endl;
	cout << "\t5 - Return to main menu" << endl;

	char opt = cin.get();
	cin.sync();
	return opt;
}

void insertProfessor()
{
	string name;
	string cpf;
	string position;

	cout << "Insert the professor name:" << endl;
	getline(cin, name);

	cout << "Insert the professor CPF:" << endl;
	getline(cin, cpf);

	cout << "Insert the professor position:" << endl;
	getline(cin, position);

	Professor* p = new Professor(name, cpf, position);
	ProfessorDB* db = ProfessorDB::getInstance();
	int result = db->addProfessor(p);

	switch(result)
	{
		case 0:
			cout << "The professor was added successfully" << endl;
			break;
		case -1:
			cout << "The database is full" << endl;
			break;
		case -2:
			cout << "This CPF is already registered" <<endl;
			break;
		default:
			break;
	}
}

void printProfessorSubjects(Professor* p)
{
	int subjectsTeaching = p->getTotalSubjectsTeaching();

	if(subjectsTeaching > 0)
	{
		Subject** subjectsList = p->listSubjectsTeaching();

		for(int j = 0; j < subjectsTeaching; j++)
		{
			Subject* subject = subjectsList[j];
			subject->print();
		}
	}
	else
	{
		cout << "\t***This professor are not teaching any subject***" << endl;
	}
}

void listProfessors()
{
	ProfessorDB* db = ProfessorDB::getInstance();
	int totalProfessors = db->getQuantity();

	Professor** professorsList = db->listProfessors();

	cout << endl;

	for(int i = 0; i < totalProfessors; i++)
	{
		if(professorsList[i])
		{
			Professor* p = professorsList[i];

			cout << "Professor " << i + 1 << endl;
			p->print();
			printProfessorSubjects(p);
		}
	}

	cout << endl;
}

void searchProfessor()
{
	string cpf;
	cout << "Input the CPF: " << endl;
	getline(cin, cpf);

	ProfessorDB* db = ProfessorDB::getInstance();
	Professor* p = db->findProfessor(cpf);

	if(!p)
	{
		cout << "No results found" << endl;
	}
	else
	{
		p->print();
		printProfessorSubjects(p);
	}
}

void deleteProfessor()
{
	string cpf;
	cout << "Input the CPF: " << endl;
	getline(cin, cpf);

	ProfessorDB* professorDB = ProfessorDB::getInstance();
	Professor* p = professorDB->findProfessor(cpf);

	Subject** subjectsList = p->listSubjectsTeaching();
	int totalSubjectsTeaching = p->getTotalSubjectsTeaching();

	for(int i = 0; i < totalSubjectsTeaching; i++)
	{
		Subject* g = subjectsList[i];
		g->removeProfessor(p);
	}

	professorDB->removeProfessor(p->getCPF());

	cout << "The professor has been deleted" << endl;
}

void ProfessorMenu::printMenu()
{
	char opt;
	do
	{
		opt = readOption();

		switch(opt)
		{
			case '1':
				insertProfessor();
				break;
			case '2':
				deleteProfessor();
				break;
			case '3':
				searchProfessor();
				break;
			break;
			case '4':
				listProfessors();
				break;
			case '5':
			break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != '5');
}