#include <iostream>
#include <cstring>

using namespace std;

class Person
{
	private:
		string _name;
		string _cpf;
	public:
		Person()
		{

		}
		
		Person(string name, string cpf)
		{
			_name = name;
			_cpf = cpf;
		}

		void setName(string name)
		{
			_name = name;
		}

		void setCPF(string cpf)
		{
			_cpf = cpf;
		}

		string getName()
		{
			return _name;
		}

		string getCPF()
		{
			return _cpf;
		}

		void print()
		{
			cout << "Name: " << getName() << endl << "CPF: " << getCPF() << endl;
		}
};

int main(void)
{
	Person p1;
	Person p2;

	string p1Name;
	string p2Name;
	string p1CPF;
	string p2CPF;

	cout << "Insert a name for person 1: " << endl;
	getline(cin, p1Name);

	cout << "Insert a CPF for person 1: " << endl;
	getline(cin, p1CPF);

	cout << "Insert a name for person 2: " << endl;
	getline(cin, p2Name);

	cout << "Insert a CPF for person 2: " << endl;
	getline(cin, p2CPF);

	p1.setName(p1Name);
	p1.setCPF(p1CPF);

	p2 = Person(p2Name, p2CPF);

	p1.print();
	cout << "Name: " << p2.getName() << endl << "CPF: " << p2.getCPF() << endl;
}