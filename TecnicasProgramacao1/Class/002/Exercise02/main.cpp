#include <iostream>

using namespace std;

class Square
{
	private:
		int _size;
	public:
		Square()
		{

		}
		
		Square(int size)
		{
			_size = size;
		}

		void setSize(int size)
		{
			_size = size;
		}

		int getSize()
		{
			return _size;
		}

		int getArea()
		{
			return _size * _size;
		}

		int getPerimeter()
		{
			return _size * 4;
		}

		void print()
		{
			cout << "Size: " << getSize() << endl;
			cout << "Area: " << getArea() << endl;
			cout << "Perimeter: " << getPerimeter() << endl;
		}
};

int main(void)
{
	float s1, s2;
	cout << "Input the size of the first square: " << endl;
	cin >> s1;

	cout << "Input the size of the second square: " << endl;
	cin >> s2;

	Square q1;
	Square q2;

	q1.setSize(s1);
	q2 = Square(s2);

	q1.print();

	cout << "Size: " << q2.getSize() << endl;
	cout << "Area: " << q2.getArea() << endl;
	cout << "Perimeter: " << q2.getPerimeter() << endl;

	return 0;
}