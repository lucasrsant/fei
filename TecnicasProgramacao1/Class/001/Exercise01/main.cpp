#include <iostream>
#include <string>

using namespace std;

struct Person
{
	int cpf;
	string name;
};

void print(Person p)
{
	cout << "CPF: " << p.cpf;
	cout << " Name: " << p.name << endl;
}

int main(int argc, char** argv)
{
	Person p1, p2;

	p1.cpf = 10;
	p1.name = "Paulo";

	p2.cpf = 20;
	p2.name = "Maria";

	print(p1);
	print(p2);

	return 0; 
}
