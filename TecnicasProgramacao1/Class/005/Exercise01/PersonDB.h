//Implementation of class PersonDB

#ifndef __PERSONDB__
#define __PERSONDB__

#include "Person.h"
#include <string>

using namespace std;

#define DATABASE_SIZE 10

class PersonDB
{
	private:
		Person *_listP[DATABASE_SIZE];
		int _totalRecords;
		int exists(string);
		int binarySearch(string, int, int);
	public:
		PersonDB();

		int removePerson(string);
		int insert(string, string);
		int getTotalRecords();
		int search(string);

		void printRecord(int);
		void list();
		void clear();
};

#endif