#include "Person.h"
#include "PersonDB.h"

PersonDB personDB;

void insertRecord()
{
	if(personDB.getTotalRecords() >= DATABASE_SIZE)
	{
		cout << "Database is full" << endl;
		return;
	}
	
	cin.ignore();
	
	cout << "Input a name: " << endl;
	string name;
	getline(cin, name);

	cout << "Input a CPF: " << endl;
	string cpf;
	getline(cin, cpf);

	personDB.insert(cpf, name);
}

void printDatabase()
{
	if(personDB.getTotalRecords() == 0)
	{
		cout << "The database is empty" << endl;
		return;
	}

	int totalRecords = personDB.getTotalRecords();
	for(int i = 0; i < totalRecords; i++)
		personDB.printRecord(i);
}

void findRecord()
{
	cin.ignore();

	cout << "Type a CPF to find:" << endl;
	string cpf;
	getline(cin, cpf);

	int index = personDB.search(cpf);

	if(index < 0)
		cout << "No register found" << endl;
	else
		personDB.printRecord(index);
}

void deleteRecord()
{
	cin.ignore();
	cout << "Type a CPF to remove:" << endl;
	string cpf;
	getline(cin, cpf);

	if(personDB.removePerson(cpf) == 0)
		cout << "The CPF has been removed successfully" << endl;
	else
		cout << "The CPF has not been found" << endl;
}

void clearDatabase()
{
	personDB.clear();
}

int readOptions()
{
	cout << "Choose an option:" << endl;
	cout << "\t1 - Insert a record" << endl;
	cout << "\t2 - Find a record" << endl;
	cout << "\t3 - Print all records" << endl;
	cout << "\t4 - Delete a record" << endl;
	cout << "\t5 - Clear database" << endl;
	cout << "\t6 - Exit program" << endl;

	int opt;
	cin >> opt;
	return opt;
}

int main(void)
{
	int opt;
	do
	{
		opt = readOptions();

		switch(opt)
		{
			case 1:
				insertRecord();
				break;
			case 2:
				findRecord();
				break;
			case 3:
				printDatabase();
				break;
			case 4:
				deleteRecord();
				break;
			case 5:
				clearDatabase();
				break;
			case 6:
				break;
			default:
				cout << "Invalid option" << endl;
		}
	}
	while(opt != 6);

	return 0;
}