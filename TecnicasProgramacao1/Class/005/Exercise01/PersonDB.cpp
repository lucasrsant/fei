//Implementation of class PersonDB

#include "PersonDB.h"

PersonDB::PersonDB()
{
	_totalRecords = 0;
}

int PersonDB::getTotalRecords()
{
	return _totalRecords;
}

int PersonDB::binarySearch(string cpf, int begin, int end)
{
	int pivot = (end + begin) / 2;
	if(cpf.compare(_listP[pivot]->getCPF()) == 0)
		return pivot;

	if(end == begin)
		return -1;
	else if(cpf.compare(_listP[pivot]->getCPF()) > 0)
		return binarySearch(cpf, pivot + 1, end);
	else
		return binarySearch(cpf, 0, pivot - 1);
}

int PersonDB::search(string cpf)
{
	if(_totalRecords == 0)
		return -1;

	return binarySearch(cpf, 0, _totalRecords - 1);
}

int PersonDB::exists(string cpf)
{
	return search(cpf) > -1;
}

//Returns -1 in case of error
//Returns 0 in chase of success
int PersonDB::insert(string cpf, string name)
{
	if(_totalRecords >= DATABASE_SIZE)
		return -1;

	if(exists(cpf))
		return -2;

	int i;
	for(i = 0; i < _totalRecords && cpf.compare(_listP[i]->getCPF()) > 0; i++)
	{
		//do nothing
	}

	int j;
	for(j = _totalRecords; j > i; j--)
		_listP[j] = _listP[j - 1];

	Person *p = new Person();
	p->setName(name);
	p->setCPF(cpf);
	_listP[i] = p;
	_totalRecords++;

	return 0;
}

//Returns 0 in case of success
//Returns -1 if the CPF wasn't found
int PersonDB::removePerson(string cpf)
{
	int index = search(cpf);
	if(index > -1)
	{
		delete _listP[index];
		//Reorder all records
		for(int j = index; j < _totalRecords - 1; j++)
			_listP[j] = _listP[j + 1];

		_listP[_totalRecords - 1] = 0x0;

		_totalRecords--;

		return 0;
	}

	return 1;
}

void PersonDB::list()
{
	for(int i = 0; i < DATABASE_SIZE; i++)
		cout << "Register #" << i << " - Name: " << _listP[i]->getName() << " CPF: " << _listP[i]->getCPF() << endl;
}

void PersonDB::printRecord(int index)
{
	cout << "Register #" << index << " - Name: " << _listP[index]->getName() << " CPF: " << _listP[index]->getCPF() << endl;
}

void PersonDB::clear()
{
	_totalRecords = 0;
}