//Definition of class Square
#include <iostream>

class Square
{
	private:
		int _size;
	public:
		Square();
		Square(int);

		void setSize(int);

		int getSize();
		int getArea();
		int getPerimeter();

		void print();
};