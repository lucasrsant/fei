//Implementation of class Square

#include "Square.h"

using namespace std;

Square::Square()
{

}
		
Square::Square(int size)
{
	_size = size;
}

void Square::setSize(int size)
{
	_size = size;
}

int Square::getSize()
{
	return _size;
}

int Square::getArea()
{
	return _size * _size;
}

int Square::getPerimeter()
{
	return _size * 4;
}

void Square::print()
{
	cout << "Size: " << getSize() << endl;
	cout << "Area: " << getArea() << endl;
	cout << "Perimeter: " << getPerimeter() << endl;
}