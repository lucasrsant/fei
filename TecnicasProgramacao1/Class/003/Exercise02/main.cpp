#include "Square.h"

using namespace std;

int main(void)
{
	float s1, s2;
	cout << "Input the size of the first square: " << endl;
	cin >> s1;

	cout << "Input the size of the second square: " << endl;
	cin >> s2;

	Square q1;
	Square q2;

	q1.setSize(s1);
	q2 = Square(s2);

	q1.print();

	cout << "Size: " << q2.getSize() << endl;
	cout << "Area: " << q2.getArea() << endl;
	cout << "Perimeter: " << q2.getPerimeter() << endl;

	return 0;
}