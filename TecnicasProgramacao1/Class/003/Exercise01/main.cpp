#include "Person.h"
#include <iostream>

using namespace std;

int main(void)
{
	Person p1;
	Person p2;

	string p1Name;
	string p2Name;
	string p1CPF;
	string p2CPF;

	cout << "Insert a name for person 1: " << endl;
	getline(cin, p1Name);

	cout << "Insert a CPF for person 1: " << endl;
	getline(cin, p1CPF);

	cout << "Insert a name for person 2: " << endl;
	getline(cin, p2Name);

	cout << "Insert a CPF for person 2: " << endl;
	getline(cin, p2CPF);

	p1.setName(p1Name);
	p1.setCPF(p1CPF);

	p2 = Person(p2Name, p2CPF);

	p1.print();
	cout << "Name: " << p2.getName() << endl << "CPF: " << p2.getCPF() << endl;
}