//Definition of class Person

#include <string>
#include <iostream>
using namespace std;

class Person
{
	private:
		string _name;
		string _cpf;
	public:
		Person();
		Person(string, string);

		void setName(string);	
		void setCPF(string);

		string getName();
		string getCPF();

		void print();
};