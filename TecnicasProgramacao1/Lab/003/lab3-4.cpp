#include "DateTime.h"
#include <iostream>

using std::cout;
using std::endl;

int main(void)
{
	DateTime dt;

	dt.printUniversal();
	cout << endl;

	for(int i = 0; i < 60 * 60 * 24 * 7 - 15000; i++)	
		dt.tick();

	dt.printUniversal();

	return 0;
}