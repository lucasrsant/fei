//Implementation of class Rectangle
#include "Rectangle.h"
#include <iostream>

using std::cout;
using std::endl;

Rectangle::Rectangle(float width, float height)
{
	setWidth(width);
	setHeight(height);
}

void Rectangle::setWidth(float width)
{
	_width = width > 0.0 && width <= 20.0 ? width : 1.0;
}

void Rectangle::setHeight(float height)
{
	_height = height > 0.0 && _height <= 20.0 ? height : 1;
}

float Rectangle::getPerimeter()
{
	return (_width * 2) + (_height * 2);
}

float Rectangle::getArea()
{
	return _width * _height;
}

void Rectangle::print()
{
	cout << "Width: " << _width << endl;
	cout << "Height: " << _height << endl;
	cout << "Perimeter: " << getPerimeter() << endl;
	cout << "Area: " << getArea() << endl;
}