//Definition of class Time
#include <iostream>
using std::cout;

#include <iomanip>
using std::setfill;
using std::setw;

#include "Time.h" //Includes the class defition Time from Time.h

Time::Time(int hour, int minute, int second)
{
	setTime(hour, minute, second);
}

void Time::setTime(int hour, int minute, int second)
{
	setHour(hour);
	setMinute(minute);
	setSecond(second);
}

void Time::setHour(int h)
{
	hour = (h >= 0 && h < 24) ? h : 0; //Validate the hours
}

void Time::setMinute(int m)
{
	minute = (m >= 0 && m < 60) ? m : 0;//Validate the minutes
}

void Time::setSecond(int s)
{
	second = (s >= 0 && s < 60) ? s : 0;//Validate the seconds
}

int Time::getHour()
{
	return hour;
}

int Time::getMinute()
{
	return minute;
}

int Time::getSecond()
{
	return second;
}

void Time::printUniversal()
{
	cout << setfill('0') << setw(2) << getHour() << ":"
	<< setw(2) << getMinute() << ":" << setw(2) << getSecond();
}

void Time::printStandard()
{
	cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
		<< ":" << setfill('0') << setw(2) << getMinute()
		<< ":" << setw(2) << getSecond() << (hour < 12 ? "AM" : "PM");
}

void Time::tick()
{
	if(second == 59)
	{
		second = 0;

		if(minute == 59)
		{
			minute = 0;

			if(hour == 23)
				hour = 0;
			else
				hour++;
		}
		else
			minute++;
	}
	else
		second++;
}