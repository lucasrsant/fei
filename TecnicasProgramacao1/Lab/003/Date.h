//Declaration of class Date
//Functions are implemented on Date.cpp

//Include guard
#ifndef DATE_H
#define DATE_H

class Date
{
	public:
		Date(int = 1, int = 1, int = 2000); //Default constructor
		void print();
		void nextDay(); //Increment one day to the current date
	private:
		void nextMonth();
		void nextYear();
		int month;
		int day;
		int year;
};

#endif