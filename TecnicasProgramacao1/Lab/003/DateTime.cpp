//Implementation of class DateTime
#include "DateTime.h"
#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::setfill;
using std::setw;

DateTime::DateTime(int s, int m, int h, int d, int M, int y) //second, minute, hour, day, month, year
{
	month = M >= 1 && M <= 12 ? M : 1;
	year = y >= 0 && y <= 9999 ? y : 2000;

	if(M == 2) //february
	{
		//check if is leap year
		if(year % 4 == 0)
			d = d >= 1 && d <= 29 ? d : 1;
		else
			d = d >= 1 && d <= 28 ? d : 1;
	}
	else if(M == 1 || M == 3 || M == 5 || M == 7 || M == 8 || M == 10 || M == 12) //Those months whose have 31 days
		day = d >= 1 && d <= 31 ? d : 1;
	else //otherwise, consider 30 days
		day = d >= 1 && d <= 30 ? d : 1;

	hour = (h >= 0 && h < 24) ? h : 0; //Validate the hours
	minute = (m >= 0 && m < 60) ? m : 0;//Validate the minutes
	second = (s >= 0 && s < 60) ? s : 0;//Validate the seconds
}

int DateTime::getDay()
{
	return day;
}

int DateTime::getYear()
{
	return year;
}

string DateTime::getMonth()
{
	switch(month)
	{
		case 1:
			return "Jan";
		case 2:
			return "Fev";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec";
	}

	return "";
}

void DateTime::nextDay()
{
	if(month == 2)
	{
		if(year % 4 == 0)
		{
			if(day == 29)
			{
				day = 1;
				nextMonth();
			}
			else
				day++;
		}
		else
		{
			if(day == 28)
			{
				day = 1;
				nextMonth();
			}
			else
				day++;
		}
	}
	else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
	{
		if(day == 31)
		{
			day = 1;
			nextMonth();
		}
		else
			day++;
	}
	else
	{
		if(day == 30)
		{
			day = 1;
			nextMonth();
		}
		else
			day++;
	}
}

void DateTime::nextMonth()
{
	if(month == 12)
	{
		month = 1;
		nextYear();
	}
	else
		month++;
}

void DateTime::nextYear()
{
	if(year == 9999)
		year++;
	else
		year = 1;
}

void DateTime::setTime(int hour, int minute, int second)
{
	setHour(hour);
	setMinute(minute);
	setSecond(second);
}

void DateTime::setHour(int h)
{
	hour = (h >= 0 && h < 24) ? h : 0; //Validate the hours
}

void DateTime::setMinute(int m)
{
	minute = (m >= 0 && m < 60) ? m : 0;//Validate the minutes
}

void DateTime::setSecond(int s)
{
	second = (s >= 0 && s < 60) ? s : 0;//Validate the seconds
}

int DateTime::getHour()
{
	return hour;
}

int DateTime::getMinute()
{
	return minute;
}

int DateTime::getSecond()
{
	return second;
}

void DateTime::printUniversal()
{
	cout << setfill('0') << setw(2) << getHour() << ":"
	<< setw(2) << getMinute() << ":" << setw(2) << getSecond() << " " 
	<< getMonth() << "/" << setw(2) << getDay() << "/" << setw(4) << getYear();
}

void DateTime::printStandard()
{
	cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
		<< ":" << setfill('0') << setw(2) << getMinute()
		<< ":" << setw(2) << getSecond() << (hour < 12 ? "AM" : "PM");
}

void DateTime::tick()
{
	if(second == 59)
	{
		second = 0;

		if(minute == 59)
		{
			minute = 0;

			if(hour == 23)
			{
				hour = 0;
				nextDay();
			}
			else
				hour++;
		}
		else
			minute++;
	}
	else
		second++;
}