//Definition of class RectanglePlus

#ifndef __RECTANGLEPLUS__
#define __RECTANGLEPLUS__

#include <vector>

using std::vector;

typedef struct
{
	int x;
	int y;	
} point;

class RectanglePlus
{
	private:
		point _points[4];

	public:
		RectanglePlus(point[4]);
		void setPointer(point, int index);
		void printPoints();
};

#endif