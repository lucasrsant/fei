//Implementation of class Complex
#include "Complex.h"
#include <iostream>

using namespace std;

Complex::Complex(double real = 0, double imaginarium = 0)
{
	_realPart = real;
	_imaginariumPart = imaginarium;
}

void Complex::sum(Complex value)
{
	_realPart += value.getReal();
	_imaginariumPart += value.getImaginarium();
}

void Complex::subtract(Complex value)
{
	_realPart -= value.getReal();
	_imaginariumPart -= value.getImaginarium();
}

void Complex::print()
{
	if(_imaginariumPart < 0)
		cout << _realPart << " - " << _imaginariumPart * -1 << "i" << endl;
	else
		cout << _realPart << " + " << _imaginariumPart << "i" << endl;
}

double Complex::getImaginarium()
{
	return _imaginariumPart;
}

double Complex::getReal()
{
	return _realPart;
}