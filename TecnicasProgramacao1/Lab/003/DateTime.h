//Definition of class DateTime

#ifndef __DATETIME__
#define __DATETIME__

#include <string>

using std::string;

class DateTime
{
	public:
		DateTime(int = 0, int = 0, int = 0, int = 1, int = 1, int = 2000); //Default constructor
		void print();
		void nextDay(); //Increment one day to the current date

		void setTime(int, int, int); //Configure hour, minute and second
		void setHour(int);
		void setMinute(int);
		void setSecond(int);

		int getHour();
		int getMinute();
		int getSecond();

		string getMonth();
		int getDay();
		int getYear();

		void printUniversal(); //Print hour in universal-format date/hour
		void printStandard(); //Print hour in standard-format date/hour
		void tick(); //Increments the current time in one second

	private:

		int hour;  //0-23 (24 hours clock format)
		int minute; //0-59
		int second; //0-59
		int month;
		int day;
		int year;

		void nextMonth();
		void nextYear();
		
};

#endif