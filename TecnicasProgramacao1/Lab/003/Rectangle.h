//Definition of class Rectangle

#ifndef __RECTANGLE__
#define __RECTANGLE__

class Rectangle
{
	private:
		float _width;
		float _height;
	public:
		Rectangle(float = 1.0, float = 1.0); //Default constructor
		float getPerimeter();
		float getArea();

		void setWidth(float);
		void setHeight(float);
		
		void print();
};

#endif