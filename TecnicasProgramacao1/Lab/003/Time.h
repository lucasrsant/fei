//Declaration of class Time
//Functions are defined in Time.cpp

//Include guard
#ifndef TIME_H
#define TIME_H

class Time
{
	private:
		int hour;  //0-23 (24 hours clock format)
		int minute; //0-59
		int second; //0-59
	public:
		Time(int = 0, int = 0, int = 0); //Default constructor

		void setTime(int, int, int); //Configure hour, minute and second
		void setHour(int);
		void setMinute(int);
		void setSecond(int);

		int getHour();
		int getMinute();
		int getSecond();

		void printUniversal(); //Print hour in universal-format date/hour
		void printStandard(); //Print hour in standard-format date/hour
		void tick(); //Increments the current time in one second
};

#endif