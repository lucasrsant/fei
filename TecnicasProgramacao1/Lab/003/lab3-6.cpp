#include "RectanglePlus.h"

int main(void)
{
	point points[4];
	points[0].x = 0;
	points[0].y = 0;

	points[1].x = 0;
	points[1].y = 2;

	points[2].x = 2;
	points[2].y = 0;

	points[3].x = 2;
	points[3].y = 2;

	RectanglePlus r(points);
	r.printPoints();

	return 0;
}