//Declaration of class Complex

#ifndef __COMPLEX__
#define __COMPLEX__

#include <string>

using namespace std;

class Complex
{
	private:
		double _realPart;
		double _imaginariumPart;
	public:
		Complex(double, double);

		void sum(Complex);
		void subtract(Complex);
		void print();

		double getImaginarium();
		double getReal();
};

#endif