//Main class to test Complex class
#include "Complex.h"
#include <iostream>

using namespace std;

int main(void)
{
	double realPart;
	double imagPart;

	cout << "Input the real part of the first Complex number: " << endl;
	cin >> realPart;

	cout << "Input the imaginarium part of the first Complex number: " << endl;
	cin >> imagPart;

	Complex a(realPart, imagPart);

	cout << "Input the real part of the second Complex number: " << endl;
	cin >> realPart;

	cout << "Input the imaginarium part of the second Complex number: " << endl;
	cin >> imagPart;

	Complex b(realPart, imagPart);

	cout << "First complex: ";
	a.print();

	cout << "Second complex: ";
	b.print();

	a.sum(b);
	cout << "B added to A: ";
	a.print();

	a.subtract(b);
	a.subtract(b);

	cout << "B subtracted from A: ";
	a.print();


	return 0;
}