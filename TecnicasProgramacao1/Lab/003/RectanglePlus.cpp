//Implementation of class RectanglePlus
#include "RectanglePlus.h"
#include <cstring>
#include <iostream>

using std::cout;
using std::endl;

RectanglePlus::RectanglePlus(point points[4])
{
	_points[0] = points[0];
	_points[1] = points[1];
	_points[2] = points[2];
	_points[3] = points[3];
}

void RectanglePlus::printPoints()
{
	for(int i = 0; i < 4; i++)
	{
		cout << "Point #" << i << " (" << _points[i].x << ", " << _points[i].y << ")" << endl;
	}
}