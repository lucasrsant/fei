#include "Time.h"
#include <iostream>

using std::cout;
using std::endl;

int main(void)
{
	Time t;
	for(int i = 0; i < 60 * 60 * 24; i++)
	{
		t.tick();
		t.printUniversal();
		cout << endl;
	}

	return 0;
}