//Implementation of class Date
#include <iostream>
using std::cout;
using std::endl;

#include "Date.h" //Includes the definition of class Date from Date.h

Date::Date(int m, int d, int y)
{
	month = m >= 1 && m <= 12 ? m : 1;
	year = y >= 0 && y <= 9999 ? y : 2000;

	if(m == 2) //february
	{
		//check if is leap year
		if(year % 4 == 0)
			d = d >= 1 && d <= 29 ? d : 1;
		else
			d = d >= 1 && d <= 28 ? d : 1;
	}
	else if(m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) //Those months whose have 31 days
		day = d >= 1 && d <= 31 ? d : 1;
	else //otherwise, consider 30 days
		day = d >= 1 && d <= 30 ? d : 1;
}

void Date::print()
{
	cout << month << "/" << day << "/" << year;
}

void Date::nextDay()
{
	if(month == 2)
	{
		if(year % 4 == 0)
		{
			if(day == 29)
			{
				day = 1;
				nextMonth();
			}
			else
				day++;
		}
		else
		{
			if(day == 28)
			{
				day = 1;
				nextMonth();
			}
			else
				day++;
		}
	}
	else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
	{
		if(day == 31)
		{
			day = 1;
			nextMonth();
		}
		else
			day++;
	}
	else
	{
		if(day == 30)
		{
			day = 1;
			nextMonth();
		}
		else
			day++;
	}
}

void Date::nextMonth()
{
	if(month == 12)
	{
		month = 1;
		nextYear();
	}
	else
		month++;
}

void Date::nextYear()
{
	if(year < 9999)
		year++;
	else
		year = 1;
}