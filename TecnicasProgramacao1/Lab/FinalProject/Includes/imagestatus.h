//ImageStatus Enum definition

#ifndef __IMAGESTATUS__
#define __IMAGESTATUS__
enum ImageStatus
{
	Success,
	ImageFormatNotSupported,
	NoSuchFile,
	UnableToReadFileHeader,
	UnableToReadImageData
};
#endif