//Image data header file
#ifndef __IMAGEDATA__
#define __IMAGEDATA__

#include <cstdlib>
#include "imagepixel.h"

class ImageData
{
	private:
		ImagePixel** pixels;
		int quantity;
		int imageSize;

	public:
		ImageData(int);
		~ImageData();
		void addPixel(ImagePixel*);
		void replacePixel(ImagePixel*, int);
		ImagePixel* getPixel(int);
};
#endif