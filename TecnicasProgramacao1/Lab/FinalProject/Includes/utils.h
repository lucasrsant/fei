//Definition of class utils
#ifndef __UTILS__
#define __UTILS__

#include "stdint.h"

class Utils
{
	public:
		static uint16_t bigToInt16(const char*);
		static uint32_t bigToInt32(const char*);
		static uint16_t littleToInt16(const char*);
		static uint32_t littleToInt32(const char*);
};

#endif