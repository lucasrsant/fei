//Image Enum definition	

#ifndef __IMAGETYPE__
#define __IMAGETYPE__
enum ImageType
{
	NONE, //Image not opened yet
	BMP,
	JPG,
	PNG
};
#endif