#ifndef __BITMAP_IMAGE__
#define __BITMAP_IMAGE__

#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <string>

#include "Bitmap/dibheader.h"
#include "image.h"
#include "utils.h"

using std::string;
using std::endl;
using std::cout;
using std::setprecision;

#define BITMAP_HEADER_SIZE 14

class BitmapImage : public Image
{
	private:
		dibHeader _dibHeader;
		char _magicNumber[2];

		ImageStatus parseDib(FILE*);
		ImageStatus parseImage(FILE*);
		ImageStatus parseHeader(FILE*);
		ImageStatus parseData(FILE*);

	public:
		BitmapImage();
		~BitmapImage();
		int getWidth();
		int getHeight();
		int getBitsPerPixel();
		int getImageSize();
		void dumpInfo();
		ImageStatus writeFile(string);
};

#endif