//Definition of class DIB Header (Bitmap images)
#ifndef __DIB_HEADER__
#define __DIB_HEADER__

#include "stdint.h"

typedef struct
{
	uint32_t headerSize;
	uint32_t width;
	uint32_t height;
	uint16_t colorPlanes;
	uint16_t bitsPerPixel;
	uint32_t compressionMethod;
	uint32_t imageSize;
	uint32_t horizontalResolution;
	uint32_t verticalResolution;
	uint32_t numberOfColors; //0 or 2 exp n
	uint32_t numberOfImportantColors; //0 when every color is important, generally ignored
}dibHeader;

#endif