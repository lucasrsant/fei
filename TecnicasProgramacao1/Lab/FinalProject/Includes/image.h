//Image class - Header file
#ifndef __IMAGE__
#define __IMAGE__

#include <string>
#include <cstring>
#include "imagetype.h"
#include "imagestatus.h"
#include "imagedata.h"
#include "Bitmap/dibheader.h"

using std::string;

class Image
{
	protected:
		ImageData* _imageData;
		ImageType _imageType;
		int _fileSize;
		int _dataOffset;
		
		string _filePath;

		unsigned char* _headerCopy;

		virtual ImageStatus parseImage(FILE*) = 0;
		virtual ImageStatus parseHeader(FILE*) = 0;
		virtual ImageStatus parseData(FILE*) = 0;

	public:
		Image();
		~Image();
		int getFileSize();
		ImageType getImageType();
		ImageStatus openImage(string);
		ImageData* getImageData();

		virtual int getWidth() = 0;
		virtual int getHeight() = 0;
		virtual int getBitsPerPixel() = 0;
		virtual int getImageSize() = 0;
		virtual void dumpInfo() = 0;

		virtual ImageStatus writeFile(string) = 0;
};
#endif