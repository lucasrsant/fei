#ifndef __IMAGE_FILTER__
#define __IMAGE_FILTER__

#include "imagepixel.h"
#include "image.h"

class ImageFilter
{
	public:
		void applySepia(Image*);
		void applyCustomColorFilter(Image* image, float colorMatrix[]);
		void applyMonochromatic(Image* image);
};

#endif