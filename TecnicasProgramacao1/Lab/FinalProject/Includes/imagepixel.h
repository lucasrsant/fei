//ImagePixel header file

#ifndef __IMAGEPIXEL__
#define __IMAGEPIXEL__

#include <iostream>

using std::cout;
using std::endl;

class ImagePixel
{
	private:
		int _r;
		int _g;
		int _b;
		int _a;
	public:
		ImagePixel(int, int, int, int);
		ImagePixel();
		~ImagePixel();
		
		//getters
		int getRed();
		int getGreen();
		int getBlue();
		int getAlpha();

		//setters
		void setRed(int);
		void setGreen(int);
		void setBlue(int);
};
#endif