#include "imagefilter.h"

void ImageFilter::applySepia(Image* image)
{
	float colorMatrix[9] = { 0.393, 0.769, 0.189, 0.349, 0.686, 0.168, 0.272, 0.534, 0.131 };
	ImageFilter::applyCustomColorFilter(image, colorMatrix);
}

void ImageFilter::applyMonochromatic(Image* image)
{
	int bytesPerPixel = image->getBitsPerPixel() / 8;
	int pixelCount = image->getImageSize() / bytesPerPixel;
	ImageData* imageData = image->getImageData();

	for(int i = 0; i < pixelCount; i++)
	{
		ImagePixel* currentPixel = imageData->getPixel(i);

		int inputRed = currentPixel->getRed();
		int inputGreen = currentPixel->getGreen();
		int inputBlue = currentPixel->getBlue();

		uint8_t maxValue = inputRed > inputGreen ? inputRed : inputGreen > inputBlue ? inputGreen : inputBlue; 

		ImagePixel* newPixel = new ImagePixel(maxValue, maxValue, maxValue, 1);
		imageData->replacePixel(newPixel, i);
	}
}

void ImageFilter::applyCustomColorFilter(Image* image, float colorMatrix[])
{
	int bytesPerPixel = image->getBitsPerPixel() / 8;
	int pixelCount = image->getImageSize() / bytesPerPixel;
	ImageData* imageData = image->getImageData();

	for(int i = 0; i < pixelCount; i++)
	{
		ImagePixel* currentPixel = imageData->getPixel(i);

		int inputRed = currentPixel->getRed();
		int inputGreen = currentPixel->getGreen();
		int inputBlue = currentPixel->getBlue();

		int outputRed = (inputRed * colorMatrix[0]) + (inputGreen * colorMatrix[1]) + (inputBlue * colorMatrix[2]);
		int outputGreen = (inputRed * colorMatrix[3]) + (inputGreen * colorMatrix[4]) + (inputBlue * colorMatrix[5]);
		int outputBlue = (inputRed * colorMatrix[6]) + (inputGreen * colorMatrix[7]) + (inputBlue * colorMatrix[8]);

		outputRed = outputRed > 255 ? 255 : outputRed;
		outputGreen = outputGreen > 255 ? 255 : outputGreen;
		outputBlue = outputBlue > 255 ? 255 : outputBlue;

		ImagePixel* newPixel = new ImagePixel(outputRed, outputGreen, outputBlue, 1);
		imageData->replacePixel(newPixel, i);
	}
}