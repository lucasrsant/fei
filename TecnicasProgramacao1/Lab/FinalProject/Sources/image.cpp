//Image Class - implementation file
#include "image.h"
#include "imagepixel.h"
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

using std::string;
using std::cout;
using std::endl;
using std::setprecision;

Image::Image()
{
	_imageType = NONE;
}

Image::~Image()
{
	delete _imageData;
}

ImageStatus Image::openImage(string filepath)
{
	FILE* imageFile = fopen(filepath.c_str(), "rb");
	if(imageFile)
	{
		_filePath = filepath;
		return parseImage(imageFile);
	}
	else
		return NoSuchFile;
}

ImageData* Image::getImageData()
{
	return _imageData;
}