//Main file implementation
#include "Bitmap/imagebitmap.h"
#include "imagefilter.h"
#include <iostream>
#include <cstring>

using std::cout;
using std::cin;
using std::endl;

void printUsage()
{
	cout << "How to use ImageFilter:" << endl << endl;
	cout << "\tImageFiter.exe -i [INPUT_IMAGE] -f [FILTER] -o [OUTPUT_IMAGE]" << endl << endl;
	cout << "\tFILTER:" << endl << endl;
	cout << "\t\t1 - Sepia" << endl;
	cout << "\t\t2 - Monochromatic" << endl;
	cout << "\t\t3 - Custom color matrix" << endl << endl;
}

void readColorMatrix(float (&colorMatrix)[9])
{
	cout << "You must input 9 float values between 0 and 1:" << endl << endl;

	for(int i = 0; i < 9; i++)
	{
		cout << "Value #" << i + 1<< ":";
		float v;
		cin >> v;

		colorMatrix[i] = v;
	}

	//return colorMatrix;
}

int main(int argc, char** argv)
{
	if(argc != 7)
	{
		printUsage();
		return -1;
	}

	if(strcmp(argv[1], "-i") != 0)
	{
		printUsage();
		return -1;
	}

	if(strcmp(argv[3], "-f") != 0)
	{
		printUsage();
		return -1;
	}

	if(strcmp(argv[5], "-o") != 0)
	{
		printUsage();
		return -1;
	}

	string filePath = argv[2];
	string outputFilePath = argv[6];
	int filter = atoi(argv[4]);
	BitmapImage* image = new BitmapImage();
	ImageStatus status = image->openImage(filePath);

	switch(status)
	{
		case Success:
			image->dumpInfo();
			break;
		case NoSuchFile:
			cout << "No such file or directory" << endl;
			break;
		case UnableToReadFileHeader:
			cout << "Unable to read bitmap header" << endl;
			break;
	}

	ImageFilter* imageFilter = new ImageFilter();

	switch(filter)
	{
		case 1:
			imageFilter->applySepia(image);	
			break;
		case 2:
			imageFilter->applyMonochromatic(image);
			break;
		case 3:
			float colorMatrix[9] = { 0 };
			readColorMatrix(colorMatrix);
			imageFilter->applyCustomColorFilter(image, colorMatrix);
			break;
	}

	if(image->writeFile(outputFilePath) == Success)
		cout << "New image has been generated successfully" << endl;
	else
		cout <<  "Error during image processing" << endl;

	delete image;

	return 0;
}