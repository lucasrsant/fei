#include "Bitmap/imagebitmap.h"

BitmapImage::BitmapImage() : Image()
{
	_headerCopy = (unsigned char*)malloc(BITMAP_HEADER_SIZE);
}

BitmapImage::~BitmapImage()
{
	free(_headerCopy);
	_headerCopy = 0;
}

int BitmapImage::getBitsPerPixel()
{
	return _dibHeader.bitsPerPixel;
}

int BitmapImage::getImageSize()
{
	return _dibHeader.imageSize;
}

int BitmapImage::getWidth()
{
	return _dibHeader.width;
}

int BitmapImage::getHeight()
{
	return _dibHeader.height;
}

void BitmapImage::dumpInfo()
{
	cout << "File path: " << _filePath << endl;
	cout << "File size: " << setprecision(3) << (float)_fileSize / 1024 / 1024 << " MB" << endl;
	cout << "Magic Number: " << (char)_magicNumber[0] << (char)_magicNumber[1] << endl;
	cout << "Data offset: " << _dataOffset << endl;
	cout << "DIB header size: " << _dibHeader.headerSize << endl;
	cout << "Width: " << _dibHeader.width << endl;
	cout << "Height: " << _dibHeader.height << endl;
	cout << "Color planes: " << _dibHeader.colorPlanes << endl;
	cout << "Bits per pixel: " << _dibHeader.bitsPerPixel << endl;
	cout << "Compression method: " << _dibHeader.compressionMethod << endl;
	cout << "Image size: " << _dibHeader.imageSize << endl;
	cout << "Horizontal resolution: " << _dibHeader.horizontalResolution << endl;
	cout << "Vertical resolution: " << _dibHeader.verticalResolution << endl;
	cout << "Number of colors: " << _dibHeader.numberOfColors << endl;
	cout << "Number of important colors: " << _dibHeader.numberOfImportantColors << endl;
}

ImageStatus BitmapImage::parseDib(FILE* imageFile)
{
	char initialHeader[4] = { 0 };
	dibHeader dib;

	if(ftell(imageFile) != BITMAP_HEADER_SIZE)
		fseek(imageFile, SEEK_SET, BITMAP_HEADER_SIZE);

	int bytesRead = fread(initialHeader, 1, 4, imageFile);
	dib.headerSize = Utils::littleToInt32(initialHeader);

	char* dibHeader = (char*)malloc(dib.headerSize);
	fread(dibHeader, 1, dib.headerSize, imageFile);

	dib.width = Utils::littleToInt32(&dibHeader[0]);
	dib.height = Utils::littleToInt32(&dibHeader[4]);
	dib.colorPlanes = Utils::littleToInt16(&dibHeader[8]);
	dib.bitsPerPixel = Utils::littleToInt32(&dibHeader[10]);
	dib.compressionMethod = Utils::littleToInt32(&dibHeader[12]);
	dib.imageSize = Utils::littleToInt32(&dibHeader[16]);
	dib.horizontalResolution = Utils::littleToInt32(&dibHeader[20]);
	dib.verticalResolution = Utils::littleToInt32(&dibHeader[24]);
	dib.numberOfColors = Utils::littleToInt32(&dibHeader[28]);
	dib.numberOfImportantColors = Utils::littleToInt32(&dibHeader[32]);

	_dibHeader = dib;

	free(dibHeader);
	dibHeader = 0;
	return Success;
}

ImageStatus BitmapImage::parseImage(FILE * imageFile)
{
	ImageStatus status;

	status = parseHeader(imageFile);
	if(status != Success)
		return status;

	status = parseDib(imageFile);
	if(status != Success)
		return status;

	if(_dibHeader.headerSize == 40 && (_dibHeader.compressionMethod == 3 || _dibHeader.compressionMethod == 6))
		return ImageFormatNotSupported;

	status = parseData(imageFile);

	return status;
}

ImageStatus BitmapImage::parseHeader(FILE* imageFile)
{
	char header[BITMAP_HEADER_SIZE] = { 0 };
	int bytesRead = fread(header, 1, BITMAP_HEADER_SIZE, imageFile);

	if(bytesRead == BITMAP_HEADER_SIZE)
	{
		memcpy(_headerCopy, header, BITMAP_HEADER_SIZE);

		_magicNumber[0] = header[0];
		_magicNumber[1] = header[1];

		_fileSize = Utils::littleToInt32(&header[2]);
		_dataOffset = Utils::littleToInt32(&header[10]);

		return Success;
	}
	else
		return UnableToReadFileHeader;
}

ImageStatus BitmapImage::writeFile(string outputFilePath)
{
	FILE* newFile = fopen(outputFilePath.c_str(), "wb");
	int bytesWritten = 0;
	if(newFile)
	{
		bytesWritten = fwrite(_headerCopy, 1, BITMAP_HEADER_SIZE, newFile);

		if(bytesWritten == BITMAP_HEADER_SIZE)
		{
			bytesWritten = fwrite(&_dibHeader, 1, _dibHeader.headerSize, newFile);

			if(bytesWritten = _dibHeader.headerSize)
			{
				int bytesPerPixel = _dibHeader.bitsPerPixel / 8;
				int pixelCount = _dibHeader.imageSize / bytesPerPixel;
				
				for(int i = 0; i < pixelCount; i++)
				{
					ImagePixel* p = _imageData->getPixel(i);
					unsigned char pixelData[3] = { 0 };
					pixelData[2] = (unsigned char)p->getRed();
					pixelData[1] = (unsigned char)p->getGreen();
					pixelData[0] = (unsigned char)p->getBlue();		
					fwrite(pixelData, 1, bytesPerPixel, newFile);	
				}

				fclose(newFile);
				return Success;
			}
		}
	}
}

ImageStatus BitmapImage::parseData(FILE* imageFile)
{
	char* imageData = (char*)malloc(_dibHeader.imageSize);
	fseek(imageFile, _dataOffset, SEEK_SET);
	int bytesRead = fread(imageData, 1, _dibHeader.imageSize, imageFile);

	_imageData = new ImageData(_dibHeader.imageSize);

	if(bytesRead == _dibHeader.imageSize)
	{
		uint8_t bytesPerPixel = _dibHeader.bitsPerPixel / 8;

		for(int i = 0; i < _dibHeader.imageSize / bytesPerPixel; i++)
		{
			uint8_t red = imageData[i * bytesPerPixel + 0];
			uint8_t green = imageData[i * bytesPerPixel + 1];
			uint8_t blue = imageData[i * bytesPerPixel + 2];

			ImagePixel* p = new ImagePixel(red, green, blue, 1);
			_imageData->addPixel(p);
		}

		free(imageData);
		imageData = 0;
		return Success;
	}
	
	free(imageData);
	imageData = 0;
	return UnableToReadImageData;
}