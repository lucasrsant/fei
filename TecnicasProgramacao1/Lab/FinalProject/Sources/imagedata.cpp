#include "ImageData.h"

ImageData::ImageData(int imageSize)
{
	pixels = (ImagePixel**)calloc(sizeof(ImagePixel*) * imageSize, 1);
	this->quantity = 0;
	this->imageSize = imageSize;
}

ImageData::~ImageData()
{
	for(int i = 0; i < quantity; i++)
	{
		delete pixels[i];
		pixels[i] = 0;
	}

	quantity = 0;
	free(pixels);
	pixels = 0;
}

void ImageData::addPixel(ImagePixel* imagePixel)
{
	if(quantity < imageSize - 1)
	{
		pixels[quantity] = imagePixel;
		quantity++;
	}
}

ImagePixel* ImageData::getPixel(int index)
{
	return (ImagePixel*)pixels[index];
}

void ImageData::replacePixel(ImagePixel* pixel, int index)
{
	delete pixels[index];
	pixels[index] = pixel;
}