//Implementation of class Utils
#include "utils.h"

uint32_t Utils::bigToInt32(const char *bytes)
{
	return ((uint8_t)bytes[0] << 24) + ((uint8_t)bytes[1] << 16) + ((uint8_t)bytes[2] << 8) + ((uint8_t)bytes[3]);
}

uint16_t Utils::bigToInt16(const char *bytes)
{
	return ((uint8_t)bytes[0] << 8) + ((uint8_t)bytes[1]);
}

uint32_t Utils::littleToInt32(const char *bytes)
{
	return ((uint8_t)bytes[3] << 24) + ((uint8_t)bytes[2] << 16) + ((uint8_t)bytes[1] << 8) + ((uint8_t)bytes[0] << 0);
}

uint16_t Utils::littleToInt16(const char *bytes)
{
	return ((uint8_t)bytes[1] << 8) + ((uint8_t)bytes[0]);
}