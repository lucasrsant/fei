//ImagePixel implementation file
#include "imagepixel.h"

ImagePixel::ImagePixel()
{
	_r = _g = _b = _a = 0;
}

ImagePixel::ImagePixel(int r, int g, int b, int a)
{
	_r = r;
	_g = g;
	_b = b;
	_a = a;
}

ImagePixel::~ImagePixel()
{
	//cout << "destroying pixel" << endl;
}

int ImagePixel::getRed()
{
	return _r;
}

int ImagePixel::getGreen()
{
	return _g;
}

int ImagePixel::getBlue()
{
	return _b;
}