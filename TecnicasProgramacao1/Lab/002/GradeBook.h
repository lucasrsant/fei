#include <string> 
// a classe GradeBook utiliza a classe de string padrão C++
using std::string;

// Definição da classe GradeBook
class GradeBook
{
	public:
   		GradeBook( string, string ); // construtor que inicializa courseName         
   		void setCourseName( string ); // função que configura o nome do curso
   		string getCourseName(); // função que obtém o nome do curso          
   		void setProfessorName(string); //Set the professor name
   		string getProfessorName(); //Get the professor name 
   		void displayMessage(); // função que exibe uma mensagem de boas-vindas

	private:
   		string courseName; // nome do curso para esse GradeBook
   		string professorName; //Professor's name
}; 
// fim da classe GradeBook
