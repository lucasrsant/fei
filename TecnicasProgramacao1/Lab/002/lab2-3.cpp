//Main class to test class Employee
#include "Employee.h"
#include <iostream>

using namespace std;

int main(void)
{
	Employee employee1("Adam", "Jones", 1200);
	Employee employee2("Richard", "Johnson", 3000);

	cout << employee1.getName() << "'s annual salary: " << employee1.getSalary() * 12 << endl;
	cout << employee2.getName() << "'s annual salary: " << employee2.getSalary() * 12 << endl;

	employee1.setSalary(employee1.getSalary() * 1.1);
	employee2.setSalary(employee2.getSalary() * 1.1);

	cout << "----After salary raise----" << endl;

	cout << employee1.getName() << "'s annual salary: " << employee1.getSalary() * 12 << endl;
	cout << employee2.getName() << "'s annual salary: " << employee2.getSalary() * 12 << endl;
}