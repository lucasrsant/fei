//Definition of class Employee

#ifndef __EMPLOYEE__
#define __EMPLOYEE__

#include <string>

using namespace std;

class Employee
{
	private:
		string _name;
		string _lastName;
		int _salary;
	public:
		Employee();
		Employee(string, string, int);

		void setName(string);
		void setLastName(string);
		void setSalary(int);

		string getName();
		string getLastName();
		int getSalary();
};
#endif