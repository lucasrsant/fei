//Nome: 		Lucas Renan dos Santos
//RM: 			22115017-0
//Disciplina: 	Linguagem e Técnicas de Programação I
//Data: 		27/02/2016

#include "Account.h"

using namespace std;

int readOption()
{
	int opt;
	cout << "Choose an operation: " << endl;
	cout << "\t1 - Credit an amount" << endl;
	cout << "\t2 - Debit an amount" << endl;
	cout << "\t3 - Show balance" << endl;
	cout << "\t4 - Return" << endl;

	cin >> opt;
	return opt;	
}

void checkAccount(Account account)
{
	int option;
	do
	{
		//Check which bank operation the user wants to perform
		option = readOption();

		switch(option)
		{
			case 1:
			{
				cout << "Input an amount to credit: " << endl;
				int amount;
				cin >> amount;
				//Credit the account with the read amount
				account.credit(amount);
				cout << amount << " credits was added to the current account" << endl;
				break;
			}
			case 2:
			{
				cout << "Input an amount to debit: " << endl;
				int amount;
				cin >> amount;
				//Try to debit the account with the read amount
				if(account.debit(amount))
					cout << amount << " credits was subtracted from the current account" << endl;
				break;
			}
			case 3:
				//Shows the current account balance to the user
				cout << "The balance of the current account is " << account.getBalance() << endl;
				break;
			case 4:
				break;
			default:
				cout << "Invalid option" << endl;
		}
	}
	while(option != 4);
}

int readCostumer()
{
	int opt;
	cout << "Choose a costumer: " << endl;
	cout << "\t1 - James" << endl;
	cout << "\t2 - John" << endl;
	cout << "\t3 - Exit program" << endl;

	cin >> opt;
	return opt;
}

int main(void)
{
	int option;

	do
	{
		//Firstly asks the user to chosse a customer to peform bank operations
		option = readCostumer();

		switch(option)
		{
			case 1:
			{			
				Account jamesAccount(10000);
				//Call the method to show the bank operations to be performed on the selected account
				checkAccount(jamesAccount);
			}
				break;
			case 2:
			{
				Account johnAccount(-500);
				//Call the method to show the bank operations to be performed on the selected account
				checkAccount(johnAccount);
			}
				break;
			case 3:
				break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(option != 3);

	return 0;
}