//Implementation of class Employee
#include "Employee.h"

using namespace std;

Employee::Employee()
{
	
}

Employee::Employee(string name, string lastName, int salary)
{
	_name = name;
	_lastName = lastName;
	_salary = salary < 0 ? 0 : salary;
}

int Employee::getSalary()
{
	return _salary;
}

string Employee::getName()
{
	return _name;
}

string Employee::getLastName()
{
	return _lastName;
}

void Employee::setSalary(int salary)
{
	_salary = salary;
}

void Employee::setName(string name)
{
	_name = name;
}

void Employee::setLastName(string lastName)
{
	_lastName = lastName;
}