//Nome: 		Lucas Renan dos Santos
//RM: 			22115017-0
//Disciplina: 	Linguagem e Técnicas de Programação I
//Data: 		27/02/2016

//Account class - Implementation file
#include "Account.h"

using namespace std;

Account::Account()
{
	
}

Account::Account(int initialBalance)
{
	//Check if the initial balance is less than zero, if yes, show the error message
	if(initialBalance < 0)
	{
		_balance = 0;		
		displayMessage("The initial balance was invalid");
	}
	else
		_balance = initialBalance;
}

void Account::credit(int amount)
{
	//Add the amount to the current balance
	_balance += amount;
}

bool Account::debit(int amount)
{
	//Check if the current amount that is trying to be debited is greater than the current balance
	if(amount > getBalance())
	{
		displayMessage("Debit amount exceeded account balance");
		return false;
	}
	//If not, performs the debit
	else
		_balance -= amount;

	return true;
}

void Account::displayMessage(string message)
{
	cout << message << endl;
}

int Account::getBalance()
{
	return _balance;
}