//Nome: 		Lucas Renan dos Santos
//RM: 			22115017-0
//Disciplina: 	Linguagem e Técnicas de Programação I
//Data: 		27/02/2016

//Header file - Account Class
#include <iostream>
#include <string>

using std::string;

class Account
{
	private:
		int _balance;
		void displayMessage(string);
	public:
		Account(void);
		Account(int);
		void credit(int);
		bool debit(int);
		int getBalance(void);
};