#include <iostream>
using std::cout; 
using std::endl;
#include "GradeBook.h" // inclui a definição de classe GradeBook

// o construtor inicializa courseName com a string fornecida como argumento
GradeBook::GradeBook( string name, string professorName )
{
   setCourseName( name ); // chama a função set para inicializar courseName
   setProfessorName( professorName );
} 
// fim do construtor GradeBook

// função para configurar o nome do curso
void GradeBook::setCourseName( string name )
{
   courseName = name; // armazena o nome do curso no objeto
} 
// fim da função setCourseName

void GradeBook::setProfessorName(string name)
{
	professorName = name;
}

// função para obter o nome do curso
string GradeBook::getCourseName()
{
   return courseName; // retorna courseName do objeto
} 
// fim da função getCourseName

string GradeBook::getProfessorName()
{
	return professorName;
}

// exibe uma mensagem de boas-vindas para o usuário GradeBook
void GradeBook::displayMessage()
{
   // chama getCourseName para obter o courseName
   cout << "Welcome to the grade book for " << getCourseName() << "!" << endl;
   cout << "This course is presented by: " << getProfessorName() << endl;
} 
// fim da função displayMessage
