//Nome: 		Lucas Renan dos Santos
//RM: 			22115017-0
//Disciplina: 	Linguagem e técnicas de programação I
//Data: 		27/02/2016

#include <iostream>
using std::cout; 
using std::endl;
#include "GradeBook.h" // inclui a definição de classe GradeBook

// a função main inicia a execução do programa
int main(void)
{
   // cria dois objetos GradeBook
   GradeBook gradeBook1("CS101 Introduction to C++ Programming", "Clint Eastwood");
   GradeBook gradeBook2("CS102 Data Structures in C++", "Charles Bronson");

   // exibe valor inicial de courseName para cada GradeBook
   cout << "gradeBook1 created for course: " << gradeBook1.getCourseName() << "\ngradeBook2 created for course: " << gradeBook2.getCourseName()  << endl;

   gradeBook1.displayMessage();
   gradeBook2.displayMessage();

   return 0; // indica terminação bem-sucedida
} 
// fim de main