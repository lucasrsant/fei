//Program to test class StringUtils
#include "StringUtils.h"
#include <string>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main(void)
{
	string a, b;

	cout << "Input the first string:" <<endl;
	cin >> a;

	cout << "Input the second string:" <<endl;
	cin >> b;

	cout << "The string concatenated is: " << StringUtils::concatString(a, b) << endl;

	return 0;
}