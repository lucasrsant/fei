//Implementation of class RationalNumber
#include "RationalNumber.h"
#include <iostream>

using std::cout;
using std::endl;

RationalNumber::RationalNumber(int a, int b)
{
	int gdc = GreatestCommonDivisor(a, b);
	_a = a / gdc;
	_b = b / gdc;
}

int RationalNumber::GreatestCommonDivisor(int a, int b)
{
	return b == 0 ? a : GreatestCommonDivisor(b, a % b);
}

void RationalNumber::printNumber()
{
	cout << _a << "/" << _b << endl;
}