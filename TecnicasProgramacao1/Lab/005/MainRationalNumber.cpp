//Program to test RationalNumber class
#include "RationalNumber.h"
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main(void)
{
	int a, b;

	cout << "Input number A:" << endl;
	cin >> a;

	cout << "Input number B:" << endl;
	cin >> b;

	RationalNumber r(a, b);
	r.printNumber();

	return 0;
}