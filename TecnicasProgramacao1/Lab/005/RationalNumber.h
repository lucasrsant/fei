//Definition of class RationalNumber

#ifndef RATIONAL_NUMBER_H
#define RATIONAL_NUMBER_H

class RationalNumber
{
	public:
		RationalNumber(int, int); //Constructor a/b
		void printNumber();
	private:
		int GreatestCommonDivisor(int, int);
		int _a;
		int _b;
};

#endif