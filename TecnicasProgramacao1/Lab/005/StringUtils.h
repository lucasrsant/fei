//Definition of class StringUtils
#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <string>

using std::string;

class StringUtils
{
	public:
		static string concatString(string, string);
};

#endif