//Implementation of class Time
#include <iostream>
#include <iomanip>
#include "Time.h"

using std::cout;
using std::setfill;
using std::setw;

Time::Time(int h, int m, int s)
{
	int totalSeconds = ((h * 60 * 60) + (m * 60) + s) % (60 * 60 * 24); //Maximum the total of seconds in one day
	_time = totalSeconds;
}

Time &Time::setTime(int h, int m, int s)
{
	setHour(h);
	setMinute(m);
	setSecond(s);

	return *this;
}

Time &Time::setHour(int h)
{
	//hour = (h >= 0 && h <= 24) ? h : 0;
	return *this;
}

Time &Time::setMinute(int m)
{
	//minute = (m >= 0 && m <= 59) ? m : 0;
	return *this;
}

Time &Time::setSecond(int s)
{
	//second = (s >= 0 && s <= 59) ? s : 0;
	return *this;
}

int Time::getHour() const
{
	return _time / 60 / 60;
}

int Time::getMinute() const
{
	return _time % (60 * 60) / 60;
}

int Time::getSecond() const
{
	return _time % 60;
}

void Time::printUniversal() const
{
	int hour = getHour();
	int minute = getMinute();
	int second = getSecond();

	cout << setfill('0') << setw(2) << hour << ":"
	<< setw(2) << minute << ":" << setw(2) << second;
}

void Time::printStandard() const
{
	int hour = getHour();
	int minute = getMinute();
	int second = getSecond();

	cout << ((hour == 0 || hour == 12) ? 12 : hour % 12) <<
	":" << setfill('0') << setw(2) << minute << ":" << setw(2)
	<< second << (hour < 12 ? " AM" : " PM");
}