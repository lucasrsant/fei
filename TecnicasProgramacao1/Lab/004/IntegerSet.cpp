//Implementation of class IntegerSet
#include "IntegerSet.h"
#include <cstring>
#include <cstdlib>
#include <iostream>

using std::cout;
using std::endl;

IntegerSet::IntegerSet()
{
	_size = DEFAULT_SIZE;
	_elements = (char*)malloc(DEFAULT_SIZE);
	memset(_elements, 0, _size);
}

IntegerSet::IntegerSet(int length)
{
	_size = length;
	_elements = (char*)malloc(_size);
	memset(_elements, 0, _size);	
}

IntegerSet::IntegerSet(int length, int* buffer)
{
	_size = buffer[length - 1] + 1;
	_elements = (char*)malloc(_size);
	memset(_elements, 0, _size);

	for(int i = 0; i < length; i++)
	{
		if(buffer[i] >= 0 && buffer[i] < _size)
			_elements[buffer[i]] = 1;
	}
}

void IntegerSet::insertElement(int element)
{
	if(element > _size)
	{
		_size = element;
		_elements = (char*)realloc(_elements, element + 1);
	}

	_elements[element] = 1;
}

void IntegerSet::deleteElement(int element)
{
	if(_elements && element < _size)
		_elements[element] = 0;
}

bool IntegerSet::isEqualTo(IntegerSet a, IntegerSet b)
{
	int sizeA = a.getSize();
	int sizeB = b.getSize();

	if(sizeA != sizeB)
		return false;

	for(int i = 0; i < sizeA; i++)
	{
		if(a.getElement(i) != b.getElement(i))
			return false;
	}

	return true;
}

void IntegerSet::printSet()
{
	cout << "{";
	for(int i = 0; i < _size; i++)
	{
		if(_elements[i])
		{
			cout << i;
			if(i < _size - 1)
				cout << ", ";
		}
	}

	cout << "}" << endl;
}

int IntegerSet::getSize()
{
	return _size;
}

int IntegerSet::getRealSize()
{
	int size = 0;
	for(int i = 0; i < _size; i++)
	{
		if(_elements[i])
			size++;
	}
}

bool IntegerSet::getElement(int index)
{
	if(index < _size)
		return _elements[index];

	return 0;
}

IntegerSet IntegerSet::unionOfSets(IntegerSet a, IntegerSet b)
{
	int aSize = a.getSize();
	int bSize = b.getSize();
	int totalSize = aSize > bSize ? aSize : bSize;
	int* newVector = (int*)malloc(totalSize * sizeof(int));
	memset(newVector, 0, totalSize * sizeof(int));

	IntegerSet newSet(totalSize);

	for(int i = 0; i < totalSize; i++)
	{
		if(i > aSize && i > bSize)
			break;

		if((i < aSize && a.getElement(i)) || (i < bSize && b.getElement(i)))
			newSet.insertElement(i);
	}

	return newSet;
}

IntegerSet IntegerSet::intersectionOfSets(IntegerSet a, IntegerSet b)
{
	int aSize = a.getSize();
	int bSize = b.getSize();
	int totalSize = aSize > bSize ? aSize : bSize;
	int* newVector = (int*)malloc(totalSize * sizeof(int));
	memset(newVector, 0, totalSize * sizeof(int));

	IntegerSet newSet(totalSize);

	for(int i = 0; i < totalSize; i++)
	{
		if(i > aSize && i > bSize)
			break;

		if((i < aSize && a.getElement(i)) && (i < bSize && b.getElement(i)))
			newSet.insertElement(i);
	}

	return newSet;
}