//Definition of class IntegerSet
#ifndef __INTEGER_SET_HEADER__
#define __INTEGER_SET_HEADER__

#define DEFAULT_SIZE 100

class IntegerSet
{
	public:
		IntegerSet();
		IntegerSet(int);
		IntegerSet(int, int*);

		static IntegerSet unionOfSets(IntegerSet, IntegerSet);
		static IntegerSet intersectionOfSets(IntegerSet, IntegerSet);
		static bool isEqualTo(IntegerSet, IntegerSet);
		void insertElement(int);
		void deleteElement(int);
		void printSet();

		bool getElement(int);
		int getSize();
		int getRealSize();

	private:
		char* _elements;
		int _size;
};

#endif