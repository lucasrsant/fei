//Program to test the class IntegerSet
#include "IntegerSet.h"
#include <iostream>

using std::cout;
using std::endl;

int main(void)
{
	int A[] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29};
	int lengthA = sizeof(A) / sizeof(int);

	IntegerSet setA(lengthA, A);
	cout << "Conjunto A: "; 
	setA.printSet();

	int B[] = {1, 3, 5};
	int lengthB = sizeof(B) / sizeof(int);
	IntegerSet setB(lengthB, B);
	cout << "Conjunto B: ";
	setB.printSet();

	int C[] = {2, 4, 6};
	int lengthC = sizeof(C) / sizeof(int);
	IntegerSet setC(lengthC, C);
	cout << "Conjunto C: ";
	setC.printSet();

	IntegerSet setD = IntegerSet::unionOfSets(setB, setC);
	cout << "B uniao C: ";
	setD.printSet();

	int E[] = {1, 2, 3, 4, 5, 6};
	int lengthE = sizeof(E) / sizeof(int);
	IntegerSet setE(lengthE, E);
	cout << "Conjunto E: ";
	setE.printSet();

	int F[] = {1, 2, 3, 4, 5};
	int lengthF = sizeof(F) / sizeof(int);
	IntegerSet setF(lengthF, F);
	cout << "Conjunto F: ";
	setF.printSet();

	IntegerSet setG = IntegerSet::intersectionOfSets(setB, setF);
	cout << "B interseccao F: ";
	setG.printSet();

	cout << "B uniao C = E? " << IntegerSet::isEqualTo(setE, setD) << endl;
	cout << "Conjunto B = C? " << IntegerSet::isEqualTo(setB, setC) << endl;

	return 0;
}