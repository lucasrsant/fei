//Program to test class Time storing time into seconds
#include "Time.h"
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main(void)
{
	int hour, minute, second;
	cout << "Input the hours:" << endl;
	cin >> hour;

	cout << "Input the minutes:" << endl;
	cin >> minute;

	cout << "Input the secods:" << endl;
	cin >> second;

	Time t(hour, minute, second);
	t.printStandard();
	cout << endl;
	t.printUniversal();

	return 0;
}