//Nome:           Lucas Renan dos Santos 
//RM:             22115017-0
//Disciplina:     Linguagem e técnicas de programação I - CC3631
//Data:           26/02/2016

/*
 * Program 1 - Calculate the sum, the multiplicaton, the subtraction and the division between two numbers
 * Program 2 - Calculate the circunference, the diameter and the area of a circle
 * Program 3 - Check if the number A is multiple of number B
*/

#include <iostream>

#define PI 3.14159

using namespace std;

class Multiple
{
	private:
		int _a, _b;
	public:
		Multiple(int a, int b)
		{
			_a = a;
			_b = b;
		}

		bool isMultiple()
		{
			//Check if the rest of the division equals to zero, if yes, the number A is multiple of B
			return _a % _b == 0;
		}
};

class Circle
{
	private:
		int _r;
	public:
		Circle(int r)
		{
			_r = r;
		}

		float getDiameter()
		{
			//Calculate the diameter
			return 2 * _r;
		}

		float getCircunference()
		{
			//Calculate the circunference
			return 2 * PI * _r;
		}

		float getArea()
		{
			//Calculate the area
			return PI * _r * _r;
		}
};

class Numbers
{
	private:
		int _a, _b;
	public:
		Numbers(int a, int b)
		{
			_a = a;
			_b = b;
		}

		int sum()
		{
			//Performs the sum.
			return _a + _b;
		}

		int product()
		{
			//Performs the multiplication
			return _a * _b;
		}

		int diff()
		{
			//Performs the subtraction
			return _a - _b;
		}

		float div()
		{
			//Performs the division
			return (float)_a / (float)_b;
		}
};

void program1(void)
{
	//Read the input parameters
	int a, b;
	cout << "Input a number:" << endl;
	cin >> a;
	cout << "Input another number:" << endl;
	cin >> b;

	//Instantiate the class
	Numbers n(a, b);

	//Print the results using the input numbers
	cout << "Sum: " << n.sum() << endl;
	cout << "Product: " << n.product() << endl;
	cout << "Diff: " << n.diff() << endl;
	cout << "Division: " << n.div() << endl;
}

int program2(void)
{
	//Read the radius
	int r;
	cout << "Input radius: " << endl;
	cin >> r;

	//Instantiate a circle using the input above.
	Circle c(r);

	//Print the operations
	cout << "Circunference: " << c.getCircunference() << endl;
	cout << "Diameter: " << c.getDiameter() << endl;
	cout << "Area: " << c.getArea() << endl;
}

int program3(void)
{
	//Read the input numbers
	int a, b;
	cout << "Input a number:" << endl;
	cin >> a;
	cout << "Input another number:" << endl;
	cin >> b;

	//Instantiate the class
	Multiple m(a, b);

	//Check if number A is multiple of B
	if(m.isMultiple())
		cout << a << " is multiple of " << b << endl;
	else
		cout << a << " is not multiple of " << b << endl;
}

int main(void)
{
	int opt;
	
	//Ask the user for which program it wants to run until it press number 4 to exit.
	do
	{
		cout << "Choose a program to run:" << endl;
		cout << "\t1 - Operations with two numbers" << endl;
		cout << "\t2 - Circunference" << endl;
		cout << "\t3 - Multiple" << endl;
		cout << "\t4 - Exit" <<endl;

		cin >> opt;

		switch(opt)
		{
			case 1:
				program1();
				break;
			case 2:
				program2();
				break;
			case 3:
				program3();
				break;
			case 4:
				break;
			default:
				cout << "Invalid option" << endl;
				break;
		}
	}
	while(opt != 4);

	return 0;
}	