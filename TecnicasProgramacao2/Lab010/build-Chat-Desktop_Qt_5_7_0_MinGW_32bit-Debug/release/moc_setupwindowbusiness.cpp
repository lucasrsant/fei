/****************************************************************************
** Meta object code from reading C++ file 'SetupWindowBusiness.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Chat/SetupWindowBusiness.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SetupWindowBusiness.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SetupWindowBusiness_t {
    QByteArrayData data[10];
    char stringdata0[125];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SetupWindowBusiness_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SetupWindowBusiness_t qt_meta_stringdata_SetupWindowBusiness = {
    {
QT_MOC_LITERAL(0, 0, 19), // "SetupWindowBusiness"
QT_MOC_LITERAL(1, 20, 7), // "onError"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(4, 58, 11), // "onConnected"
QT_MOC_LITERAL(5, 70, 14), // "_connectedSlot"
QT_MOC_LITERAL(6, 85, 15), // "_receivedPacket"
QT_MOC_LITERAL(7, 101, 6), // "packet"
QT_MOC_LITERAL(8, 108, 10), // "_errorSlot"
QT_MOC_LITERAL(9, 119, 5) // "error"

    },
    "SetupWindowBusiness\0onError\0\0"
    "QAbstractSocket::SocketError\0onConnected\0"
    "_connectedSlot\0_receivedPacket\0packet\0"
    "_errorSlot\0error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SetupWindowBusiness[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       4,    0,   42,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   43,    2, 0x08 /* Private */,
       6,    1,   44,    2, 0x08 /* Private */,
       8,    1,   47,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, 0x80000000 | 3,    9,

       0        // eod
};

void SetupWindowBusiness::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SetupWindowBusiness *_t = static_cast<SetupWindowBusiness *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 1: _t->onConnected(); break;
        case 2: _t->_connectedSlot(); break;
        case 3: _t->_receivedPacket((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 4: _t->_errorSlot((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SetupWindowBusiness::*_t)(QAbstractSocket::SocketError );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SetupWindowBusiness::onError)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SetupWindowBusiness::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SetupWindowBusiness::onConnected)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject SetupWindowBusiness::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SetupWindowBusiness.data,
      qt_meta_data_SetupWindowBusiness,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SetupWindowBusiness::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SetupWindowBusiness::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SetupWindowBusiness.stringdata0))
        return static_cast<void*>(const_cast< SetupWindowBusiness*>(this));
    return QObject::qt_metacast(_clname);
}

int SetupWindowBusiness::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void SetupWindowBusiness::onError(QAbstractSocket::SocketError _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SetupWindowBusiness::onConnected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
