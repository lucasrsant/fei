#include "Network.h"

Network::Network(QObject* parent)
{
    _qTcpSocket = new QTcpSocket(parent);
    connect(_qTcpSocket, SIGNAL(connected()), this, SLOT(_connectedSlot()));
    connect(_qTcpSocket, SIGNAL(disconnected()), this, SLOT(_disconnectedSlot()));
    connect(_qTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(_errorSlot(QAbstractSocket::SocketError)));
    connect(_qTcpSocket, SIGNAL(readyRead()), this, SLOT(_readyReadSlot()));
}

void Network::openConnection(const QString& hostAddress, quint16 portNo) {
    _hostAddress = new QString(hostAddress);
    _portNo = portNo;
    qDebug() << "connecting to " << hostAddress << " on port " << portNo;
    _qTcpSocket->connectToHost(hostAddress, portNo);
}

void Network::closeConnection() {
    _qTcpSocket->close();
}

qint64 Network::sendPacket(const QByteArray packet) {
    qint64 bytesWritten = _qTcpSocket->write(packet);
    _qTcpSocket->flush();

    qDebug() << bytesWritten << " bytes written";

    return bytesWritten;
}

bool Network::isConnected() {
    return _qTcpSocket->isReadable() && _qTcpSocket->isWritable();
}

void Network::_connectedSlot() {
    qDebug() << "connected to " << *_hostAddress << " on port " << _portNo;

    emit onConnected();
}

void Network::_disconnectedSlot() {
    qDebug() << "disconnected from " << *_hostAddress << " on port " << _portNo;
    emit onDisconnected();
}

void Network::_errorSlot(QAbstractSocket::SocketError error) {
    qDebug() << "an error has occurred " << error;
    emit onError(error);
}

void Network::_readyReadSlot() {
    QByteArray receivedPacket = _qTcpSocket->readAll();

    emit onReceivedPacket(receivedPacket);
}

