#include "SetupWindow.h"

QLineEdit* SetupWindow::setupNicknameLineEdit() {
    QLineEdit* qNicknameLineEdit = new QLineEdit;
    qNicknameLineEdit->setPlaceholderText("Type your nickname...");
    QRegExp regex("^[a-zA-Z0-9_.\-]*$");
    QRegExpValidator* validator = new QRegExpValidator(regex, this);
    qNicknameLineEdit->setValidator(validator);

    return qNicknameLineEdit;
}

QLabel* SetupWindow::setupNicknameLabel() {
    QLabel* qNicknameLabel = new QLabel;
    qNicknameLabel->setText("Nickname");
    qNicknameLabel->setContentsMargins(0, 0, 5, 0);

    return qNicknameLabel;
}

void SetupWindow::setupOkButton() {
    _okButton = new QPushButton;

    _okButton->setText("Connect");
    _okButton->setContentsMargins(10, 10, 0, 10);
    _okButton->setFixedWidth(100);
    _okButton->move(200, 0);

    connect(_okButton, SIGNAL(released()), this, SLOT(_clickSlot()));
}

QStatusBar* SetupWindow::setupStatusBar(const QString& initialMessage) {
    _statusBarLabel = new QLabel;
    QStatusBar* statusBar = new QStatusBar;
    _statusBarLabel->setText(initialMessage);
    statusBar->addPermanentWidget(_statusBarLabel);
    statusBar->setSizeGripEnabled(false);

    return statusBar;
}

void SetupWindow::setupUserInterface() {
    this->resize(400, 120);
    this->setWindowTitle("Chat FEI - Settings");

    QGroupBox* qUserConfigGroupBox = new QGroupBox;
    qUserConfigGroupBox->setTitle("User settings");

    this->setContentsMargins(10, 5, 10, 5);
    this->setCentralWidget(qUserConfigGroupBox);

    QVBoxLayout* groupBoxLayout = new QVBoxLayout;
    QHBoxLayout* nicknameInfoLayout = new QHBoxLayout;

    nicknameInfoLayout->addWidget(this->setupNicknameLabel());

    _nicknameTextEdit = this->setupNicknameLineEdit();
    nicknameInfoLayout->addWidget(_nicknameTextEdit);

    groupBoxLayout->addLayout(nicknameInfoLayout);

    this->setupOkButton();

    groupBoxLayout->addWidget(_okButton);
    groupBoxLayout->setAlignment(_okButton, Qt::AlignRight);

    qUserConfigGroupBox->setLayout(groupBoxLayout);

    QStatusBar* statusBar = this->setupStatusBar("Disconnected...");
    groupBoxLayout->addWidget(statusBar);
    groupBoxLayout->setAlignment(statusBar, Qt::AlignLeft);
}

void SetupWindow::_clickSlot() {
    _okButton->setText("Connecting...");
    _okButton->setDisabled(true);
    _statusBarLabel->setText("Connecting...");
    _business->connectToServer();
}

SetupWindow::SetupWindow(QWidget *parent) : QMainWindow(parent)
{
    setupUserInterface();
    connect(_business, SIGNAL(onError(QAbstractSocket::SocketError)), this, SLOT(_errorSlot(QAbstractSocket::SocketError)));
    connect(_business, SIGNAL(onConnected()), this, SLOT(_connectedSlot()));
}

void SetupWindow::_connectedSlot() {
    _business->setupNickname(_nicknameTextEdit->text());
    _statusBarLabel->setText("Connection successfully!");

    _business->sendMessage("Sending test message automatically");
}

void SetupWindow::_errorSlot(QAbstractSocket::SocketError error) {
    _statusBarLabel->setText("An error has occurred...");
    _okButton->setText("Connect");
    _okButton->setEnabled(true);
}
