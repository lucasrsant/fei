#include "SetupWindowBusiness.h"

SetupWindowBusiness::SetupWindowBusiness()
{
    _network = new Network;
    connect(_network, SIGNAL(onConnected()), this, SLOT(_connectedSlot()));
    connect(_network, SIGNAL(onError(QAbstractSocket::SocketError)), this, SLOT(_errorSlot(QAbstractSocket::SocketError)));
    connect(_network, SIGNAL(onReceivedPacket(QByteArray)), this, SLOT(_receivedPacket(QByteArray)));
}

void SetupWindowBusiness::connectToServer() {
    _network->openConnection("10.103.1.112", 8080);
}

void SetupWindowBusiness::setupNickname(QString nickname) {
    nickname += "\r\n\r\n";
    _network->sendPacket(nickname.toUtf8());
}

void SetupWindowBusiness::sendMessage(QString message) {
    message += "\r\n\r\n";
    _network->sendPacket(message.toUtf8());
}

void SetupWindowBusiness::_connectedSlot() {
    qDebug() << "connected on host";
    emit onConnected();
}

void SetupWindowBusiness::_receivedPacket(QByteArray packet) {
    qDebug() << "received data";
    qDebug() << packet;
}

void SetupWindowBusiness::_errorSlot(QAbstractSocket::SocketError error) {
    qDebug() << "an error has occured";
    emit onError(error);
}
