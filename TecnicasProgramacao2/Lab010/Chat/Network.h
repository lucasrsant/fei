#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QTcpSocket>

class Network : public QObject
{
    Q_OBJECT

public:
    Network(QObject* parent = Q_NULLPTR);
    void openConnection(const QString& hostAddress, quint16 portNo = 80);
    void closeConnection();
    qint64 sendPacket(const QByteArray packet);
    bool isConnected();

    //Callback setters
    void setOnConnectedCallback(void(*callback)(void));
    void setOnDisconnectedCallback(void(*callback)(void));

signals:
    void onConnected();
    void onDisconnected();
    void onError(QAbstractSocket::SocketError error);
    void onReceivedPacket(QByteArray packet);

private:
    QTcpSocket* _qTcpSocket;
    QString* _hostAddress;
    quint16 _portNo;

private slots:
    void _connectedSlot();
    void _disconnectedSlot();
    void _errorSlot(QAbstractSocket::SocketError error);
    void _readyReadSlot();
};

#endif // NETWORK_H
