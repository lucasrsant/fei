#include <iostream>
#include <Network.h>
#include <SetupWindow.h>

#include <QApplication>


using namespace std;

Network _network;

void onConnected() {
    qInfo() << "Insert a message:";
    QTextStream qtin(stdin);
    QString message = qtin.readLine();

    qint64 bytesWritten = _network.sendPacket(message.toUtf8());
    if(bytesWritten > -1)
        qInfo() << bytesWritten << " bytes written";
    else
        qInfo() << "An error has occured";

    _network.closeConnection();
}

void onDisconnected() {
    qInfo() << "Message from my callback";
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    SetupWindow setupWindow;
    setupWindow.show();
    //_network.setOnConnectedCallback(onConnected);
    //_network.setOnDisconnectedCallback(onDisconnected);
    //_network.openConnection("www.google.com.br");

    return app.exec();
}
