#ifndef SETUPWINDOW_H
#define SETUPWINDOW_H

#include <QMainWindow>
#include <QGroupBox>
#include <QLayout>
#include <QLineEdit>
#include <QRegExp>
#include <QRegExpValidator>
#include <QLabel>
#include <QPushButton>
#include <QStatusBar>

#include "SetupWindowBusiness.h"

class SetupWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit SetupWindow(QWidget *parent = 0);

private:
    void setupUserInterface();
    QLineEdit* setupNicknameLineEdit();
    QLabel* setupNicknameLabel();
    void setupOkButton();
    QStatusBar* setupStatusBar(const QString& initialMessage);

    //PROPERTIES
    SetupWindowBusiness* _business = new SetupWindowBusiness;
    QLineEdit* _nicknameTextEdit;
    QLabel* _statusBarLabel;
    QPushButton* _okButton;

private slots:
    void _clickSlot();
    void _errorSlot(QAbstractSocket::SocketError error);
    void _connectedSlot();
};

#endif // SETUPWINDOW_H
