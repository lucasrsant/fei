TEMPLATE = app
#CONFIG += c++11
#CONFIG += app_bundle
CONFIG += qt

#QMAKE_LFLAGS += -static-libgcc -static-libstdc++ -static

QT += core network widgets

SOURCES += main.cpp \
    SetupWindowBusiness.cpp \
    SetupWindow.cpp \
    Network.cpp

HEADERS += \
    SetupWindowBusiness.h \
    SetupWindow.h \
    Network.h
