#ifndef SETUPWINDOWBUSINESS_H
#define SETUPWINDOWBUSINESS_H

#include <QObject>
#include "Network.h"

class SetupWindowBusiness : public QObject
{
    Q_OBJECT

public:
    SetupWindowBusiness();
    void connectToServer();
    void setupNickname(QString nickname);
    void sendMessage(QString message);

private:
    Network* _network;

signals:
    void onError(QAbstractSocket::SocketError);
    void onConnected();

private slots:
    void _connectedSlot();
    void _receivedPacket(QByteArray packet);
    void _errorSlot(QAbstractSocket::SocketError error);
};

#endif // SETUPWINDOWBUSINESS_H
