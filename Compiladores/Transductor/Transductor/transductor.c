//Lucas Renan dos Santos - RA 22115017-0
//Leonardo Papst Figueiredo - RA 22115008-9
//Guilherme Crucillo - RA 22115040-2


#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

//prototypes
void e0();
void e1();
void e2();
void decline();
void accept();

void insertWord();
void printResult();
void printVariablesTable();

int p = 0;
char word[200];

int currentWordSize = 0;
char currentWord[200] = { 0 };

char** wordsList;
int wordsListSize = 10;
int wordsListIndex = 0;

int readingNumericWord;

int findInList(char* word) {
	int i;
	for (i = 0; i < wordsListIndex; i++) {
		if (myStrcmp(word, wordsList[i]) == 1)
			return i;
	}

	return -1;
}

void printResult() {
	int i;
	int outputIndex = 0;
	for (i = 0; i < wordsListIndex; i++) {
		char* word = wordsList[i];

		if (isNumericWord(word) == 0) {
			int index = findInList(word);

			if (index == i) {
				printf("V(%d)", outputIndex, word);
				outputIndex++;
			}
		}
		else
			printf("N(%s)", word);
	}
		
	printVariablesTable();
}

void printVariablesTable() {

	printf("\n\nVARIABLES TABLE:\n");

	int i;
	int outputIndex = 0;
	for (i = 0; i < wordsListIndex; i++) {
		char* word = wordsList[i];

		if (isNumericWord(word) == 0) {
			int index = findInList(word);

			if (index == i) {
				printf("%d = %s\n", outputIndex, word);
				outputIndex++;
			}
		}		
	}
}

void insertWord() {

	wordsList[wordsListIndex] = malloc(sizeof(char) * currentWordSize + 1);
	myMemcpy(currentWord, wordsList[wordsListIndex], currentWordSize + 1);
	
	wordsListIndex++;

	if (wordsListIndex == wordsListSize - 1)
		wordsListSize *= 2;
}

void decline() {
	printf("ERRO!\n");
	system("pause");
	exit(-1);
}

void accept() {

	if (currentWordSize > 0)
		insertWord();

	printResult();

	system("pause");
	exit(0);
}

void e0() {

	if (currentWordSize > 0)
		insertWord();

	currentWordSize = 0;
	myMemset(0, currentWord, sizeof(currentWord) / sizeof(char));

	readingNumericWord = 1;

	if (isLetter(word[p])) {

		currentWord[currentWordSize] = word[p];
		currentWordSize++;

		p++;
		e1();
	}
	else if (isSpace(word[p])) {
		p++;
		e0();
	}
	else if (isNumber(word[p])) {

		currentWord[currentWordSize] = word[p];
		currentWordSize++;

		p++;
		e2();
	}
	else if (word[p] == '\0')
		accept();
	else
		decline();
}

void e1() {

	readingNumericWord = 0;

	if (isSpace(word[p])) {
		p++;
		e0();
	}
	else if (word[p] == '\0')
		accept();

	currentWord[currentWordSize] = word[p];
	currentWordSize++;

	if (isLetter(word[p]) || isNumber(word[p])) {
		p++;
		e1();
	}
	else
		decline();
}

void e2() {

	if (isSpace(word[p])) {
		p++;
		e0();
	}
	else if (word[p] == '\0')
		accept();

	currentWord[currentWordSize] = word[p];
	currentWordSize++;

	if (isNumber(word[p])) {
		p++;
		e2();
	}
	else if (isLetter(word[p])) {
		p++;
		e0();
	}
	else
		decline();
}

int main() {

	wordsList = (char*)malloc(sizeof(char*) * wordsListSize);
	
	puts("Digite sua palavra: ");
	gets(word);
	e0();
}