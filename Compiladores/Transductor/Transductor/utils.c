#include "utils.h"

uint8_t isNumber(uint8_t c) {
	return (c >= '0' && c <= '9');
}

uint8_t isLetter(uint8_t c) {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

uint8_t isSpace(uint8_t c) {
	return c == ' ';
}

uint8_t isNumericWord(uint8_t* buffer) {
	uint32_t i;
	uint32_t wordSize = myStrlen(buffer);
	for (i = 0; i < wordSize; i++) {
		if (isLetter(buffer[i]))
			break;
	}

	return i == wordSize;
}

void myMemset(uint8_t value, void* buffer, uint32_t bufferSize) {
	uint32_t i = bufferSize;
	uint8_t* p = buffer;
	while (i > 0) {
		*p = value;
		p++;
		i--;
	}
}

void myMemcpy(const uint8_t* src, const uint8_t* dst, uint32_t bufferSize) {
	uint32_t i;

	uint8_t* srcPtr = src;
	uint8_t* dstPtr = dst;

	for (i = 0; i < bufferSize; i++) {
		*dstPtr = *srcPtr;
		dstPtr++;
		srcPtr++;
	}
}

uint32_t myStrlen(uint8_t* buffer) {
	uint32_t i = 0;
	while (buffer[i] != 0)
		i++;
	return i;
}

uint8_t myStrcmp(uint8_t* src, uint8_t* dst) {

	uint32_t dstLen = strlen(dst);
	uint32_t srcLen = strlen(src);

	if (dstLen != srcLen)
		return 0;

	uint32_t i, result = 1;
	for (i = 0; i < dstLen; i++)
		result *= dst[i] == src[i];

	return result;
}