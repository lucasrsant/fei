#pragma once
#ifndef __UTILS_HEADER__
#define __UTILS_HEADER__

#include <stdint.h>

uint8_t isNumeric(uint8_t c);
uint8_t isLetter(uint8_t c);
uint8_t isSpace(uint8_t c);

uint8_t isNumericWord(uint8_t* buffer);

void myMemset(uint8_t value, void* buffer, uint32_t bufferSize);
void myMemcpy(uint8_t* src, uint8_t* dst, uint32_t bufferSize);
uint8_t myStrcmp(uint8_t* src, uint8_t* dst);
uint32_t myStrlen(uint8_t* buffer);

#endif