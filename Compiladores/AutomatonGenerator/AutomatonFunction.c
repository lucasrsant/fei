#include <stdio.h>
#include <stdlib.h>
void e0();
void e1();
void decline();
void accept();

int p = 0;
char word[200];

void decline() {
	printf("Wrong!");
	exit(-1);
}

void accept() {
	printf("Success!");
	exit(0);
}

void e0() {
	if(word[p] == 'a'){
		p++;
		e1();
	}
	else {
		if(word[p] == 'b') {
			p++;
			e0();
		}
		else {
			if(word[p] == 'c') {
				p++;
				e0();
			}
			else
				decline();
		}
	}
}

void e1() {
	if(word[p] == '\0'){
		accept();
	}
	else
		decline();
}

int main(){
	puts("Digit your word: ");
	gets(word);
	e0();
}
