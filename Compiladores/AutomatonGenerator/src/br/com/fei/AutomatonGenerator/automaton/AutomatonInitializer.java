package br.com.fei.AutomatonGenerator.automaton;

import br.com.fei.AutomatonGenerator.alphabet.AlphabetReader;
import br.com.fei.AutomatonGenerator.states.StateReader;

import java.io.PrintStream;
import java.util.Scanner;

public class AutomatonInitializer {

    private final Scanner scanner;
    private final PrintStream printStream;
    private final AlphabetReader alphabetReader;
    private final StateReader stateReader;

    private final Automaton automaton;

    public AutomatonInitializer(PrintStream printStream,
                                Scanner scanner,
                                AlphabetReader alphabetReader,
                                StateReader stateReader) {
        this.printStream = printStream;
        this.scanner = scanner;
        this.alphabetReader = alphabetReader;
        this.stateReader = stateReader;

        this.automaton = new Automaton();
    }

    public void readAlphabetSize() {
        printAlphabetSizeMessage();

        int alphabetSize = scanner.nextInt();

        automaton.setAlphabetSize(alphabetSize);
        automaton.setAlphabet(alphabetReader.readAlphabet(alphabetSize));
    }

    private void printAlphabetSizeMessage() {
        printStream.println("How many symbols the alphabet has?");
    }

    public void readStates() {
        printStateSizeMessage();
        int statesSize = scanner.nextInt();

        automaton.setStatesSize(statesSize);

        printFinalStatesSizeMessage();
        int finalStatesSize = scanner.nextInt();

        automaton.setInitialState(stateReader.readInitialState());
        automaton.setListStatesMatrix(stateReader.readStateSymbolsMatrix(statesSize, automaton.getAlphabet()));
        automaton.setFinalStates(stateReader.readFinalStates(finalStatesSize));
    }

    private void printStateSizeMessage() {
        printStream.println("How many states?");
    }

    private void printFinalStatesSizeMessage() {
        printStream.println("How many final states?");
    }

    public Automaton getAutomaton() {
        return automaton;
    }
}
