package br.com.fei.AutomatonGenerator.automaton;

import br.com.fei.AutomatonGenerator.states.StateSymbol;

import java.io.PrintStream;
import java.util.List;

public class Automaton {
    private int alphabetSize;
    private int statesSize;
    private int finalStatesSize;
    private int initialState;
    private List<String> alphabet;
    private List<Integer> finalStates;
    private List<StateSymbol> statesSymbol;
    private List<List<StateSymbol>> listStatesMatrix;

    public void setAlphabetSize(int alphabetSize) {
        this.alphabetSize = alphabetSize;
    }

    public void setAlphabet(List<String> alphabet) {
        this.alphabet = alphabet;
    }

    public List<String> getAlphabet() {
        return alphabet;
    }

    public void setStatesSize(int statesSize) {
        this.statesSize = statesSize;
    }

    public void setListStatesMatrix(List<List<StateSymbol>> listStatesMatrix) {
        this.listStatesMatrix = listStatesMatrix;
    }

    public List<List<StateSymbol>> getListStatesMatrix() {
        return listStatesMatrix;
    }

    public void setFinalStates(List<Integer> finalStates) {
        this.finalStates = finalStates;
    }

    public void setInitialState(int initialState) {
        this.initialState = initialState;
    }

    public int getInitialState() {
        return initialState;
    }

    public List<Integer> getFinalStates() {
        return finalStates;
    }

    public int getStatesSize() {
        return statesSize;
    }

    public void printTableStatesSymbol(PrintStream printStream)
    {
        String write = "          ";

        for (String alphabetItem : alphabet)
            write += alphabetItem + "   ";

        printStream.println(write);

        write = "";
        for (int i = 0; i < statesSize; i++)
        {
            if (finalStates.contains(i) && initialState == i)
                write = write + "<-> E" + i + "   ";
            else {
                if (finalStates.contains(i))
                    write = write + "<-- E" + i + "   ";
                else
                {
                    if (initialState == i)
                    {
                        write = write + "--> E" + i + "   ";
                    }
                    else
                    {
                        write = write + "    E" + i + "   ";
                    }
                }
            }

            for (int j = 0; j < alphabetSize; j++)
            {
                StateSymbol symbol = listStatesMatrix.get(i).get(j);// listStatesMatrix[i].Where(o => o.state == i && o.symbol == alphabet[j]).FirstOrDefault();
                if (symbol.getNextState() == -1)
                    write = write + " -" + "   ";
                else
                    write = write + "E" + symbol.getNextState() + "   ";
            }

            printStream.println(write);
            write = "";
        }
    }
}