package br.com.fei.AutomatonGenerator;

import br.com.fei.AutomatonGenerator.automaton.Automaton;
import br.com.fei.AutomatonGenerator.automaton.AutomatonInitializer;

public class Program
{
    public static void main(String[] args)
    {
        AutomatonInitializer automatonInitializer = new AutomatonInitializer(DependencyInjector.getPrintStream(),
                DependencyInjector.getScanner(), DependencyInjector.getAlphabetReader(), DependencyInjector.getStateReader());

        automatonInitializer.readAlphabetSize();
        automatonInitializer.readStates();

        Automaton automaton = automatonInitializer.getAutomaton();

        automaton.printTableStatesSymbol(DependencyInjector.getPrintStream());
        showExportMenu(automaton);
    }

    private static void showExportMenu(Automaton automaton) {

        int option = -1;
        do
        {
            DependencyInjector.getPrintStream().println("==============HOW DO YOU WANT TO EXPORT=================");
            DependencyInjector.getPrintStream().println("1 - Go To");
            DependencyInjector.getPrintStream().println("2 - Function");
            DependencyInjector.getPrintStream().println("0 - Exit");
            DependencyInjector.getPrintStream().println("========================================================");
            option = DependencyInjector.getScanner().nextInt();

            switch (option)
            {
                case 1:
                    DependencyInjector.getAutomatonGoToWriter().writeSourceFile(automaton, "AutomatonGoTo.c");
                    DependencyInjector.getPrintStream().println("Automaton created!");
                    break;

                case 2:
                    DependencyInjector.getAutomatonFunctionWriter().writeSourceFile(automaton, "AutomatonFunction.c");
                    DependencyInjector.getPrintStream().println("Automaton created!");
                    break;

                default:
                    break;
            }

        } while (option != 0);
    }
}