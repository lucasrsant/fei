package br.com.fei.AutomatonGenerator.alphabet;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AlphabetReader {

    private final Scanner scanner;
    private final AlphabetMessagePrinter messagePrinter;

    public AlphabetReader(Scanner scanner, AlphabetMessagePrinter messagePrinter) {
        this.scanner = scanner;
        this.messagePrinter = messagePrinter;
    }

    public List<String> readAlphabet(int alphabetSize) {
        ArrayList<String> alphabet = new ArrayList<>();

        for(int i = 0; i < alphabetSize; i++) {
            messagePrinter.printInputAlphabetMessage(i + 1);
            alphabet.add(scanner.next());
        }

        return alphabet;
    }
}
