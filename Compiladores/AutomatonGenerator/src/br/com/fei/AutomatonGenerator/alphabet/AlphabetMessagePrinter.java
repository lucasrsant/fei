package br.com.fei.AutomatonGenerator.alphabet;

import java.io.PrintStream;
import java.util.List;

public class AlphabetMessagePrinter {
    private final PrintStream printStream;

    public AlphabetMessagePrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void printInputAlphabetMessage(int index) {
        printStream.println("Input the symbol " + index);
    }

    public void printAlphabet(List<String> alphabet) {
        printStream.println("========================================================");
        printStream.println("Alphabet size: " + alphabet.size());
            for(int i = 0; i < alphabet.size(); i++)
                printStream.println("Symbol #" + i + 1 + ": " + alphabet.get(i));
        printStream.println("========================================================");
    }
}
