package br.com.fei.AutomatonGenerator;

import br.com.fei.AutomatonGenerator.alphabet.AlphabetMessagePrinter;
import br.com.fei.AutomatonGenerator.alphabet.AlphabetReader;
import br.com.fei.AutomatonGenerator.states.StateMessagePrinter;
import br.com.fei.AutomatonGenerator.states.StateReader;
import br.com.fei.AutomatonGenerator.writer.AutomatonSourceWriter;
import br.com.fei.AutomatonGenerator.writer.AutomatonSourceWriterFunctionImpl;
import br.com.fei.AutomatonGenerator.writer.AutomatonSourceWriterGoToImpl;

import java.io.PrintStream;
import java.util.Scanner;

public class DependencyInjector {

    private static Scanner scanner;
    private static PrintStream printStream;
    private static AlphabetReader alphabetReader;
    private static AlphabetMessagePrinter alphabetMessagePrinter;
    private static StateReader stateReader;
    private static StateMessagePrinter stateMessagePrinter;
    private static AutomatonSourceWriter gotoWriter;
    private static AutomatonSourceWriter functionWriter;

    public static AlphabetReader getAlphabetReader() {
        if(alphabetReader == null)
            alphabetReader = new AlphabetReader(getScanner(), getAlphabetMessagePrinter());

        return alphabetReader;
    }

    public static Scanner getScanner() {
        if(scanner == null)
            scanner = new Scanner(System.in);

        return scanner;
    }

    public static AlphabetMessagePrinter getAlphabetMessagePrinter() {
        if(alphabetMessagePrinter == null)
            alphabetMessagePrinter = new AlphabetMessagePrinter(getPrintStream());

        return alphabetMessagePrinter;
    }

    public static PrintStream getPrintStream() {
        if(printStream == null)
            printStream = System.out;

        return printStream;
    }

    public static StateReader getStateReader() {
        if(stateReader == null)
            stateReader = new StateReader(getScanner(), getStateMessagePrinter());

        return stateReader;
    }

    public static StateMessagePrinter getStateMessagePrinter() {
        if(stateMessagePrinter == null)
            stateMessagePrinter = new StateMessagePrinter(getPrintStream());

        return stateMessagePrinter;
    }

    public static AutomatonSourceWriter getAutomatonFunctionWriter() {
        if(functionWriter == null)
            functionWriter = new AutomatonSourceWriterFunctionImpl();

        return functionWriter;
    }

    public static AutomatonSourceWriter getAutomatonGoToWriter() {
        if(gotoWriter == null)
            gotoWriter = new AutomatonSourceWriterGoToImpl();

        return gotoWriter;
    }
}