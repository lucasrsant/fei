package br.com.fei.AutomatonGenerator.states;

import java.io.PrintStream;

public class StateMessagePrinter {
    private final PrintStream printStream;

    public StateMessagePrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void printInputStateMessage(int stateIndex, String alphabet) {
        printStream.println("For the state [e" + stateIndex + "] and symbol [" + alphabet + "], which is the next state?");
    }

    public void printInputFinalStateMessage(int stateIndex) {
        printStream.println("Which is the final state [" + stateIndex + "]?");
    }

    public void printInputInitialStateMessage() {
        printStream.println("Which is the initial state?");
    }
}