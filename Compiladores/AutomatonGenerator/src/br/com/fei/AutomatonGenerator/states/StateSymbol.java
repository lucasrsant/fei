package br.com.fei.AutomatonGenerator.states;

public class StateSymbol {

    private int state;
    private String symbol;
    private int nextState;

    private StateSymbol() {

    }

    public int getState() {
        return state;
    }

    public int getNextState() {
        return nextState;
    }

    public String getSymbol() {
        return symbol;
    }

    public static class Builder {
        private StateSymbol stateSymbol;
        private Builder() {
            stateSymbol = new StateSymbol();
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder withSymbol(String symbol) {
            stateSymbol.symbol = symbol;
            return this;
        }

        public Builder withState(int state) {
            stateSymbol.state = state;
            return this;
        }

        public Builder withNextState(int nextState) {
            stateSymbol.nextState = nextState;
            return this;
        }

        public StateSymbol build() {
            return stateSymbol;
        }
    }
}
