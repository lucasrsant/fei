package br.com.fei.AutomatonGenerator.states;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StateReader {
    private final Scanner scanner;
    private final StateMessagePrinter stateMessagePrinter;

    public StateReader(Scanner scanner, StateMessagePrinter stateMessagePrinter) {
        this.scanner = scanner;
        this.stateMessagePrinter = stateMessagePrinter;
    }

    public List<List<StateSymbol>> readStateSymbolsMatrix(int statesSize, List<String> alphabet) {
        List<List<StateSymbol>> listStatesMatrix = new ArrayList<>();

        for (int j = 0; j < statesSize; j++)
        {
            listStatesMatrix.add(new ArrayList<>());
            for (String alphabetItem : alphabet)
            {
                stateMessagePrinter.printInputStateMessage(j, alphabetItem);
                int nextState = scanner.nextInt();
                while (isNextStateValid(nextState, statesSize) == false)
                    nextState = scanner.nextInt();

                listStatesMatrix.get(j).add(StateSymbol.Builder.newBuilder()
                        .withSymbol(alphabetItem)
                        .withState(j)
                        .withNextState(nextState)
                        .build());
            }
        }

        return listStatesMatrix;
    }

    public List<Integer> readFinalStates(int finalStatesSize) {

        List<Integer> finalStates = new ArrayList<>();

        for (int i = 0; i < finalStatesSize; i++)
        {
            stateMessagePrinter.printInputFinalStateMessage(i);

            int finalState = scanner.nextInt();
            while (isFinalStateValid(finalState, finalStatesSize))
                finalState = scanner.nextInt();

            finalStates.add(finalState);
        }

        return finalStates;
    }

    public int readInitialState() {
        stateMessagePrinter.printInputInitialStateMessage();
        return scanner.nextInt();
    }

    private boolean isNextStateValid(int nextState, int statesQuantity) {
        return nextState < statesQuantity && nextState >= -1;
    }

    private boolean isFinalStateValid(int state, int statesQuantity) {
        return state < statesQuantity && state >= 0;
    }
}
