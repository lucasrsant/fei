package br.com.fei.AutomatonGenerator.writer;

public class SourceWriterUtils {
    public static String tabs(int count) {
        StringBuilder tabStringBuilder = new StringBuilder();
        while (count > 0)
        {
            tabStringBuilder.append("\t");
            count--;
        }
        return tabStringBuilder.toString();
    }
}