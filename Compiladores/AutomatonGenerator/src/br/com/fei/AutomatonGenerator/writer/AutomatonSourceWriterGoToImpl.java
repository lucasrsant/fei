package br.com.fei.AutomatonGenerator.writer;

import br.com.fei.AutomatonGenerator.automaton.Automaton;
import br.com.fei.AutomatonGenerator.states.StateSymbol;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class AutomatonSourceWriterGoToImpl implements AutomatonSourceWriter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @Override
    public void writeSourceFile(Automaton automaton, String fileName) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName))) {
            String fileContent = generateFileContent(automaton);
            bufferedWriter.write(fileContent);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateFileContent(Automaton automaton) {
        StringBuilder source = new StringBuilder();

        source.append("#include <stdio.h>");
        source.append(LINE_SEPARATOR);

        source.append("#include <stdlib.h>");
        source.append(LINE_SEPARATOR);

        source.append("int p = 0;");
        source.append(LINE_SEPARATOR);
        source.append("char word[200];");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("int main() {");
        source.append(LINE_SEPARATOR);
        source.append("\tputs(\"Enter a word: \");");
        source.append(LINE_SEPARATOR);
        source.append("\tgets(word);");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("\tgoto e");
        source.append(automaton.getInitialState());
        source.append(";");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        for (int i = 0; i < automaton.getListStatesMatrix().size(); i++)
        {
            boolean hasElse = false;
            boolean hasNextState = false;
            boolean hasFinalState = false;

            int countElse = 0;
            source.append("e");
            source.append(i);
            source.append(":");
            source.append(LINE_SEPARATOR);

            for(int j = 0; j < automaton.getListStatesMatrix().get(i).size(); j++)
            {
                StateSymbol symbol = automaton.getListStatesMatrix().get(i).get(j);

                if (hasElse == true)
                {
                    String tabs = SourceWriterUtils.tabs(countElse + 1);
                    if (symbol.getNextState() != -1)
                    {
                        source.append(tabs);
                        source.append("else{");
                        source.append(LINE_SEPARATOR);

                        source.append(tabs);
                        source.append("\tif(word[p] == '");
                        source.append(symbol.getSymbol());
                        source.append("'){");
                        source.append(LINE_SEPARATOR);

                        source.append(tabs);
                        source.append("\t\tp++;");
                        source.append(LINE_SEPARATOR);
                        source.append(tabs);
                        source.append("\t\tgoto e");
                        source.append(symbol.getNextState());
                        source.append(";");
                        source.append(LINE_SEPARATOR);

                        source.append(tabs);
                        source.append("\t}");
                        source.append(LINE_SEPARATOR);
                        countElse++;
                    }
                    else
                    {
                        if (automaton.getFinalStates().contains(symbol.getState()) && hasFinalState == false)
                            hasFinalState = true;
                    }
                }
                else
                {
                    if (automaton.getFinalStates().contains(symbol.getState()) && symbol.getNextState() == -1 && hasFinalState == false)
                        hasFinalState = true;
                    else
                    {
                        if (symbol.getNextState() != -1)
                        {
                            hasElse = true;
                            hasNextState = true;
                            source.append("\tif(word[p] == '");
                            source.append(symbol.getSymbol());
                            source.append("'){");
                            source.append(LINE_SEPARATOR);

                            source.append("\t\tp++;");
                            source.append(LINE_SEPARATOR);
                            source.append("\t\tgoto e");
                            source.append(symbol.getNextState());
                            source.append(";");
                            source.append(LINE_SEPARATOR);
                            source.append("\t}");
                            source.append(LINE_SEPARATOR);
                        }
                    }
                }
            }

            if (hasFinalState == true)
            {
                hasFinalState = true;
                String tabs = SourceWriterUtils.tabs(countElse + 1);
                if (hasElse == false)
                {
                    hasElse = true;
                    hasNextState = true;
                    source.append("\tif(word[p] == '\\0\'){");
                    source.append(LINE_SEPARATOR);
                    source.append("\t\tgoto accept;");
                    source.append(LINE_SEPARATOR);
                    source.append("\t}");
                    source.append(LINE_SEPARATOR);
                }
                else
                {
                    source.append(tabs);
                    source.append("else{");
                    source.append(LINE_SEPARATOR);
                    source.append(tabs);
                    source.append("\tif(word[p] == '\\0\'){");
                    source.append(LINE_SEPARATOR);
                    source.append(tabs);
                    source.append("\t\tgoto accept;");
                    source.append(LINE_SEPARATOR);
                    source.append(tabs);
                    source.append("\t}");
                    source.append(LINE_SEPARATOR);
                    countElse++;
                }
            }

            if (hasNextState)
            {
                source.append(SourceWriterUtils.tabs(countElse));
                source.append("\telse");
                source.append(LINE_SEPARATOR);
                source.append(SourceWriterUtils.tabs(countElse));
                source.append("\t\tgoto decline;");
                source.append(LINE_SEPARATOR);
            }
            else {
                source.append("\tgoto decline;");
                source.append(LINE_SEPARATOR);
            }

            while (countElse > 0)
            {
                source.append(SourceWriterUtils.tabs(countElse));
                source.append("}");
                source.append(LINE_SEPARATOR);
                countElse--;
            }

            source.append(LINE_SEPARATOR);
        }

        source.append("decline:");
        source.append(LINE_SEPARATOR);
        source.append("\tputs(\"Wrong! \");");
        source.append(LINE_SEPARATOR);
        source.append("\treturn(-1);");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("accept:");
        source.append(LINE_SEPARATOR);
        source.append("\tprintf(\"Success!\");");
        source.append(LINE_SEPARATOR);
        source.append("\treturn(0);");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("}");
        source.append(LINE_SEPARATOR);

        return source.toString();
    }
}