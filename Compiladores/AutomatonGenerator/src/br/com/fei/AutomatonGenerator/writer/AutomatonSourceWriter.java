package br.com.fei.AutomatonGenerator.writer;

import br.com.fei.AutomatonGenerator.automaton.Automaton;

public interface AutomatonSourceWriter {
    void writeSourceFile(Automaton automaton, String fileName);
}
