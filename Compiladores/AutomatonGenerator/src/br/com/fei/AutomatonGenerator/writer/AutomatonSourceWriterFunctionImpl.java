package br.com.fei.AutomatonGenerator.writer;

import br.com.fei.AutomatonGenerator.automaton.Automaton;
import br.com.fei.AutomatonGenerator.states.StateSymbol;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class AutomatonSourceWriterFunctionImpl implements AutomatonSourceWriter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @Override
    public void writeSourceFile(Automaton automaton, String fileName) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName))) {
            String fileContent = generateFileContent(automaton);
            bufferedWriter.write(fileContent);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateFileContent(Automaton automaton) {

        StringBuilder source = new StringBuilder();

        source.append("#include <stdio.h>");
        source.append(LINE_SEPARATOR);
        source.append("#include <stdlib.h>");
        source.append(LINE_SEPARATOR);

        for (int i = 0; i < automaton.getStatesSize(); i++)
        {
            //generating prototypes
            source.append("void e");
            source.append(i);
            source.append("();");
            source.append(LINE_SEPARATOR);
        }

        source.append("void decline();");
        source.append(LINE_SEPARATOR);

        source.append("void accept();");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("int p = 0;");
        source.append(LINE_SEPARATOR);
        source.append("char word[200];");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("void decline() {");
        source.append(LINE_SEPARATOR);

        source.append("\tprintf(\"Wrong!\");");
        source.append(LINE_SEPARATOR);
        source.append("\texit(-1);");
        source.append(LINE_SEPARATOR);
        source.append("}");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        source.append("void accept() {");
        source.append(LINE_SEPARATOR);
        source.append("\tprintf(\"Success!\");");
        source.append(LINE_SEPARATOR);
        source.append("\texit(0);");
        source.append(LINE_SEPARATOR);
        source.append("}");
        source.append(LINE_SEPARATOR);
        source.append(LINE_SEPARATOR);

        for (int i = 0; i < automaton.getListStatesMatrix().size(); i++) {
            boolean hasElse = false;
            boolean hasNextState = false;
            boolean hasFinalState = false;

            int countElse = 0;

            source.append("void e");
            source.append(i);
            source.append("() {");
            source.append(LINE_SEPARATOR);

            for(int j = 0; j < automaton.getListStatesMatrix().get(i).size(); j++) {

                StateSymbol symbol = automaton.getListStatesMatrix().get(i).get(j);

                if (hasElse == true)
                {
                    String tabs = SourceWriterUtils.tabs(countElse + 1);
                    if (symbol.getNextState() != -1)
                    {
                        source.append(tabs);
                        source.append("else {");
                        source.append(LINE_SEPARATOR);
                        source.append(tabs);
                        source.append("\tif(word[p] == '");
                        source.append(symbol.getSymbol());
                        source.append("') {");
                        source.append(LINE_SEPARATOR);
                        source.append(tabs);
                        source.append("\t\tp++;");
                        source.append(LINE_SEPARATOR);
                        source.append(tabs);
                        source.append("\t\te");
                        source.append(symbol.getNextState());
                        source.append("();");
                        source.append(LINE_SEPARATOR);
                        source.append(tabs);
                        source.append("\t}");
                        source.append(LINE_SEPARATOR);
                        countElse++;
                    }
                    else
                    {
                        if (automaton.getFinalStates().contains(symbol.getState()) && hasFinalState == false)
                            hasFinalState = true;
                    }
                }
                else
                {
                    if (automaton.getFinalStates().contains(symbol.getState()) && symbol.getNextState() == -1 && hasFinalState == false)
                        hasFinalState = true;
                    else
                    {
                        if(symbol.getNextState() != -1)
                        {
                            hasElse = true;
                            hasNextState = true;
                            source.append("\tif(word[p] == '");
                            source.append(symbol.getSymbol());
                            source.append("'){");
                            source.append(LINE_SEPARATOR);
                            source.append("\t\tp++;");
                            source.append(LINE_SEPARATOR);
                            source.append("\t\te");
                            source.append(symbol.getNextState());
                            source.append("();");
                            source.append(LINE_SEPARATOR);
                            source.append("\t}");
                            source.append(LINE_SEPARATOR);
                        }
                    }
                }
            }

            if(hasFinalState == true)
            {
                hasFinalState = true;
                String tabs = SourceWriterUtils.tabs(countElse + 1);

                if (hasElse == false)
                {
                    hasElse = true;
                    hasNextState = true;
                    source.append("\tif(word[p] == '\\0\'){");
                    source.append(LINE_SEPARATOR);
                    source.append("\t\taccept();");
                    source.append(LINE_SEPARATOR);
                    source.append("\t}");
                    source.append(LINE_SEPARATOR);
                }
                else
                {
                    source.append(tabs);
                    source.append("else{");
                    source.append(LINE_SEPARATOR);

                    source.append(tabs);
                    source.append("\tif(word[p] == '\\0\'){");
                    source.append(LINE_SEPARATOR);
                    source.append(tabs);
                    source.append("\t\taccept();");
                    source.append(LINE_SEPARATOR);
                    source.append(tabs);
                    source.append("\t}");
                    source.append(LINE_SEPARATOR);
                    countElse++;
                }
            }

            if (hasNextState)
            {
                source.append(SourceWriterUtils.tabs(countElse));
                source.append("\telse");
                source.append(LINE_SEPARATOR);
                source.append(SourceWriterUtils.tabs(countElse));
                source.append("\t\tdecline();");
                source.append(LINE_SEPARATOR);
            }
            else {
                source.append("\tdecline();");
                source.append(LINE_SEPARATOR);
            }

            while (countElse > 0)
            {
                source.append(SourceWriterUtils.tabs(countElse));
                source.append("}");
                source.append(LINE_SEPARATOR);
                countElse--;
            }

            source.append("}");
            source.append(LINE_SEPARATOR);
            source.append(LINE_SEPARATOR);
        }

        source.append("int main(){");
        source.append(LINE_SEPARATOR);
        source.append("\tputs(\"Digit your word: \");");
        source.append(LINE_SEPARATOR);
        source.append("\tgets(word);");
        source.append(LINE_SEPARATOR);
        source.append("\te");
        source.append(automaton.getInitialState());
        source.append("();");
        source.append(LINE_SEPARATOR);
        source.append("}");
        source.append(LINE_SEPARATOR);

        return source.toString();
    }
}
