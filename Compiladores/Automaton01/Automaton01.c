#include <stdio.h>

//prototypes
void e0();
void e1();
void e2();
void e3();
void e4();
void success();
void failure();

char word[200] = { 0 };
int position = 0;

void success() {
	printf("Success\n");
	exit(0);
}

void failure() {
	printf("Failure\n");
	exit(0);
}

void e0() {
	if(word[position] == 'a') {
		position++;
		e0();
	}
	else if(word[position] == 'b') {
		position++;
		e0();
	}
	else if(word[position] == 'i') {
		position++;
		e1();
	}
	else if(word[position] == 'e') {
		position++;
		e2();		
	}
	else if(word[position] == 0)
		success();
	else
		failure();
}

void e1() {
	if(word[position] == 'f') {
		position++;
		e0();
	}
	else
		failure();
}

void e2() {
	if(word[position] == 'l') {
		position++;
		e3();
	}
	else {
		failure();
	}
}

void e3() {
	if(word[position] == 's') {
		position++;
		e4();
	}
	else {
		failure();
	}
}

void e4() {
	if(word[position] == 'e') {
		position++;
		e0();
	}
	else {
		failure();
	}
}

int main(int argc, char** argv) {

	printf("Enter a word:\n");
	gets(word);
	e0();

	exit(0);
}