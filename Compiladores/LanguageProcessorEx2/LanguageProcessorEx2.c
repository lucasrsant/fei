#include <stdio.h>
#include <stdlib.h>

//LUCAS RENAN DOS SANTOS - RA 22115017-0
//LEONARDO PAPST FIGUEIREDO - RA 22115008-9

char words[] = {'a', 'b', 'i', 'e'};

int myStrLen(char* word) {
	int i = 0;
	
	while(word[i] != 0)
	    i++;
	
	return i;
}

char* processWord(char* word) {
	int wordLength = myStrLen(word);
    char* errorIndicator = calloc(sizeof(char) * wordLength + 1, 1);
	int match = 0;
	int i, j;
	
	for(i = 0; i < wordLength; i++)
	{
		errorIndicator[i] = '-';
		for(j = 0; j < sizeof(words)/sizeof(char); j++) {
			if(word[i] == words[j]) {
				if(j == 2) { //should be if
					if(word[i + 1] == 'f') {
						//match = 1;
						i++;
						errorIndicator[i + 1] = '-';
					}	
					else
						errorIndicator[i + 1] = '^';
				} 
				else if (j == 3) { //should be else
					if(word[i + 1] == 'l') {
						i++;
						errorIndicator[i] = '-';
						
						if(word[i + 1] == 's') {
							i++;
							errorIndicator[i] = '-';
							
							if(word[i + 1] == 'e') {
								i++;
								errorIndicator[i] = '-';
							}
							else {
								errorIndicator[i + 1] = '^';
							}
						}
						else {
							errorIndicator[i + 1] = '^';
						}
					}
					else {
						errorIndicator[i + 1] = '^';
					}
				}

				match = 1;
                break;
			}
		}
		
		if(match != 1) {
            errorIndicator[i] = '^';
            errorIndicator[i + 1] = 0;
		    return errorIndicator;
		}
		
		match = 0;
	}
	
	return 0;
}

int main(int argc, char** argv) {

	int i;
	for(i = 1; i < argc; i++) {
		char* word = argv[i];
		printf("processando palavra %s...\n", word);
		char* errorIndicator = processWord(word);
		
		if(errorIndicator > 0) {
			printf("Error\n");
			printf("%s\n", word);
		    printf("%s\n", errorIndicator);
		}
		else {
			printf("Success!\n");
		}
	}

	system("pause");
	return 0;
}
