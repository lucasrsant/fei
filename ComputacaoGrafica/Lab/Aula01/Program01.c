#include <stdio.h>
#include <stdlib.h>

#include "GL/glut.h"

GLfloat angle, fAspect;

//defines
#define TEAPOT 1
#define TORUS 2
#define CUBE 3

#define BLACK 1
#define WHITE 2

#define RED 1
#define GREEN 2
#define BLUE 3

//prototypes
void mouseHandler(int button, int state, int x, int y);
void createMainMenu(void);
void drawTeapot(void);
void drawTorus(void);
void drawCube(void);
void drawBackgroundColor(void);
void drawShapeColor(void);

int shape = 0;
int shapeColor = 0;
int backgroundColor = 0;

void draw(void) 
{
	drawBackgroundColor();
	glClear(GL_COLOR_BUFFER_BIT);

	drawShapeColor();
	
	switch(shape) {
		case TEAPOT:
			drawTeapot();
			break;
		case TORUS:
			drawTorus();
			break;
		case CUBE:
			drawCube();
			break;
	}

	glutSwapBuffers();
}

void drawBackgroundColor() {
	switch(backgroundColor) {
		case BLACK:
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			break;
		case WHITE:
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			break;
	}
}

void drawShapeColor() {
	switch(shapeColor) {
		case RED:
			glColor3f(1.0f, 0.0f, 0.0f);
			break;
		case GREEN:
			glColor3f(0.0f, 1.0f, 0.0f);
			break;
		case BLUE:
			glColor3f(0.0f, 0.0f, 1.0f);
			break;
	}
}

void drawTeapot(void) {
	glutWireTeapot(50.0f);
}

void drawTorus(void) {
	glutWireTorus(20.0f, 40.0f, 20, 20);
}

void drawCube(void) {
	glutWireCube(50.0f);
}

void init(void) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	shape = TEAPOT;
	shapeColor = RED;
	angle = 45;
}

void mouseHandler(int button, int state, int x, int y) {
	if(button == GLUT_RIGHT_BUTTON)
		if(state == GLUT_DOWN)
			createMainMenu();

	glutPostRedisplay();
}

void setVisualizationParams(void) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(angle, fAspect, 0.1f, 500);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0, 80, 200, 0, 0, 0, 0, 1, 0);
}

void windowHandler(GLsizei width, GLsizei height) {
	if(height == 0)
		height = 1;

	glViewport(0, 0, width, height);

	fAspect = (GLfloat)width/(GLfloat)height;

	setVisualizationParams();
}

void shapeMenuCallback(int option) {
	shape = option;
	glutPostRedisplay();
}

void shapeColorMenuCallback(int option) {
	shapeColor = option;
	glutPostRedisplay();
}

void backgroundColorMenuCallback(int option) {
	backgroundColor = option;
	glutPostRedisplay();
}

int createBackgroundColorMenu() {
	int menu = glutCreateMenu(backgroundColorMenuCallback);
	glutAddMenuEntry("White", WHITE);
	glutAddMenuEntry("Black", BLACK);

	return menu;
}

int createShapeColorMenu() {
	int menu = glutCreateMenu(shapeColorMenuCallback);
	glutAddMenuEntry("Red", RED);
	glutAddMenuEntry("Green", GREEN);
	glutAddMenuEntry("Blue", BLUE);

	return menu;
}

int createShapeMenu() {
	int menu = glutCreateMenu(shapeMenuCallback);
	glutAddMenuEntry("Teapot", TEAPOT);
	glutAddMenuEntry("Torus", TORUS);
	glutAddMenuEntry("Cube", CUBE);

	return menu;
}

void mainMenuCallback(int option) {
	//Do nothing
}

void createMainMenu(void) {
	
	int backgroundColorMenu = createBackgroundColorMenu();
	int shapeColorMenu = createShapeColorMenu();
	int shapeMenu = createShapeMenu();

	int mainMenu = glutCreateMenu(mainMenuCallback);
	glutAddSubMenu("Background color", backgroundColorMenu);
	glutAddSubMenu("Shape color", shapeColorMenu);
	glutAddSubMenu("Shapes", shapeMenu);

	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv) {

	//init GLUT and create a window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(350, 300);
	
	glutCreateWindow("Visualizacao 3D");
	
	glutDisplayFunc(draw);
	glutReshapeFunc(windowHandler);
	glutMouseFunc(mouseHandler);

	init();
	glutMainLoop();


}