#include <stdio.h>
#include <stdlib.h>

#include "GL/glut.h"

GLfloat angle, fAspect;

//defines
#define TEAPOT 0
#define TORUS 1
#define CUBE 2

#define BLACK 0
#define WHITE 1
#define RED 2
#define GREEN 3
#define BLUE 4

#define SOLID 0
#define WIRE 1

//prototypes
void mouseHandler(int button, int state, int x, int y);
void createMainMenu(void);
void drawTeapot(void);
void drawTorus(void);
void drawCube(void);
void drawBackgroundColor(void);
void drawShapeColor(void);

GLfloat windowWidth = 350.0f;
GLfloat windowHeight = 300.0f;

GLfloat xStep = 1.0f;
GLfloat yStep = 1.0f;

GLfloat shapeSize = 50.0f;

int max = 40;
int count = 0;
int increasing = 1;
GLfloat direction[3] = { 0.0f, 0.0f, 1.0f };

int shape = 0;
int shapeColor = 0;
int shapeMode = 0;
int backgroundColor = 0;

void draw(void) 
{
	drawBackgroundColor();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawShapeColor();

	switch(shape) {
		case TEAPOT:
			drawTeapot();
			break;
		case TORUS:
			drawTorus();
			break;
		case CUBE:
			drawCube();
			break;
	}

	glutSwapBuffers();
}

void timer(int value) {
	if(increasing) {
		if(count >= max) {
			increasing = 0;
			direction[0] = 0.0f;
			direction[1] = 0.0f;
			direction[2] = 1.0f;
		}

		glTranslatef(1.0f * direction[0], 1.0f * direction[1], 1.0f * direction[2]);
		count++;
	}
	else {
		if(count <= 0) {
			increasing = 1;
			direction[0] = 0.0f;
			direction[1] = 0.0f;
			direction[2] = -1.0f;
		}

		glTranslatef(1.0f * direction[0], 1.0f * direction[1], 1.0f * direction[2]);
		count--;
	}

	glutPostRedisplay(); 
	glutTimerFunc(33, timer, value + 1);
}

void drawBackgroundColor() {
	switch(backgroundColor) {
		case BLACK:
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			break;
		case WHITE:
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			break;
	}
}

void drawShapeColor() {
	switch(shapeColor) {
		case RED:
			glColor3f(1.0f, 0.0f, 0.0f);
			break;
		case GREEN:
			glColor3f(0.0f, 1.0f, 0.0f);
			break;
		case BLUE:
			glColor3f(0.0f, 0.0f, 1.0f);
			break;
	}
}

void drawTeapot(void) {
	switch(shapeMode) {
		case WIRE:
			glutWireTeapot(shapeSize);
			break;
		case SOLID:
			glutSolidTeapot(shapeSize);
			break;
	}
	
}

void drawTorus(void) {
	switch(shapeMode) {
		case WIRE:
			glutWireTorus(20.0f, shapeSize, 20, 20);
			break;
		case SOLID:
			glutSolidTorus(20.0f, shapeSize, 20, 20);
			break;
	}
}

void drawCube(void) {
	switch(shapeMode) {
		case WIRE:
			glutWireCube(shapeSize);
			break;
		case SOLID:
			glutSolidCube(shapeSize);
			break;
	}
	
}

void initLighting() {
	GLfloat enviromentLight[4] = {0.2f, 0.2f, 0.2f, 1.0f};
	GLfloat diffusedLight[4] = {0.7f, 0.7f, 0.7f, 1.0f}; //color
	GLfloat specularLight[4] = {0.5f, 0.5f, 0.5f, 0.5f}; //brightness
	GLfloat lightningPosition[4] = {0.0f, 50.0f, 50.0f, 1.0f};

	GLfloat specularity[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLint specMaterial = 60;

	glShadeModel(GL_SMOOTH);

	glMaterialfv(GL_FRONT, GL_SPECULAR, specularity);
	glMateriali(GL_FRONT, GL_SHININESS, specMaterial);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, enviromentLight);

	glLightfv(GL_LIGHT0, GL_AMBIENT, enviromentLight); 
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffusedLight); 
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight); 
	glLightfv(GL_LIGHT0, GL_POSITION, lightningPosition);

	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
}

void init(void) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	initLighting();

	shape = TEAPOT;
	shapeColor = RED;
	angle = 45;
}

void mouseHandler(int button, int state, int x, int y) {
	if(button == GLUT_RIGHT_BUTTON)
		if(state == GLUT_DOWN)
			createMainMenu();

	glutPostRedisplay();
}

void setVisualizationParams(void) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(angle, fAspect, 0.1f, 500);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0, 80, 200, 0, 0, 0, 0, 1, 0);
}

void windowHandler(GLsizei width, GLsizei height) {
	if(height == 0)
		height = 1;

	glViewport(0, 0, width, height);

	fAspect = (GLfloat)width/(GLfloat)height;

	setVisualizationParams();
}

void shapeModeMenuCallback(int option) {
	shapeMode = option;
	glutPostRedisplay();
}

void shapeMenuCallback(int option) {
	shape = option;
	glutPostRedisplay();
}

void shapeColorMenuCallback(int option) {
	shapeColor = option;
	glutPostRedisplay();
}

void backgroundColorMenuCallback(int option) {
	backgroundColor = option;
	glutPostRedisplay();
}

int createBackgroundColorMenu() {
	int menu = glutCreateMenu(backgroundColorMenuCallback);
	glutAddMenuEntry("White", WHITE);
	glutAddMenuEntry("Black", BLACK);

	return menu;
}

int createShapeColorMenu() {
	int menu = glutCreateMenu(shapeColorMenuCallback);
	glutAddMenuEntry("Red", RED);
	glutAddMenuEntry("Green", GREEN);
	glutAddMenuEntry("Blue", BLUE);

	return menu;
}


int createShapeModeMenu() {
	int menu = glutCreateMenu(shapeModeMenuCallback);

	glutAddMenuEntry("Solid", SOLID);
	glutAddMenuEntry("Wire", WIRE);

	return menu;
}

int createShapeMenu() {
	int menu = glutCreateMenu(shapeMenuCallback);
	glutAddMenuEntry("Teapot", TEAPOT);
	glutAddMenuEntry("Torus", TORUS);
	glutAddMenuEntry("Cube", CUBE);

	return menu;
}

void mainMenuCallback(int option) {
	//Do nothing
}

void createMainMenu(void) {
	
	int backgroundColorMenu = createBackgroundColorMenu();
	int shapeColorMenu = createShapeColorMenu();
	int shapeModeMenu = createShapeModeMenu();
	int shapeMenu = createShapeMenu();

	int mainMenu = glutCreateMenu(mainMenuCallback);
	glutAddSubMenu("Background color", backgroundColorMenu);
	glutAddSubMenu("Shape color", shapeColorMenu);
	glutAddSubMenu("Shape mode", shapeModeMenu);
	glutAddSubMenu("Shapes", shapeMenu);

	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv) {

	//init GLUT and create a window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(350, 300);
	
	glutCreateWindow("Visualizacao 3D");
	
	glutDisplayFunc(draw);
	glutReshapeFunc(windowHandler);
	glutMouseFunc(mouseHandler);

	glutTimerFunc(33, timer, 1);

	init();
	glutMainLoop();


}