#include <stdio.h>
#include <stdlib.h>

//Crie uma estrutura de nó que guarde um número inteiro

typedef struct _node node;

typedef struct _node
{
    int value;
    node* next;
} node;

typedef struct
{
	node* previous;
	node* current;
} searchResult;

//Lista
typedef struct 
{
    int count;
    node* first;
} list;

void printList(list* l) 
{
    node* current = l->first;
    printf("l = {");
    while (current != NULL) 
    {
        printf("%-3d%s", current->value, current->next != NULL ? ", " : "}\n" );
        current = current->next;
    }
}

searchResult linearSearch(list* l, int key)
{
	searchResult result;

	if(l)
	{
		result.previous = NULL;
		result.current = l->first;

		while(result.current != NULL && result.current->value != key)
		{
			result.previous = result.current;
			result.current = result.current->next;
		}

		if(result.current && result.current->value == key)
			return result;
	}

	result.current = NULL;
	result.previous = NULL;

	return result;
}

void initialize(list* l) 
{
	l->count = 0;
	l->first = NULL;
}

//Returns 0 in case of success
//Return 1 in case of error
int insertLde(list* l, int value)
{
	if(l)
	{
		node* current = l->first;
		node* previous = NULL;

		while(current != NULL && current->value < value)
		{
			previous = current;
			current = current->next;
		}

		node* newNode = malloc(sizeof(node));

		if(!newNode)
			return 1;

		newNode->value = value;
		newNode->next = current;

		if(previous) //Check if I am inserting at the first position. In this case, there is no previous element
			previous->next = newNode;
		else
			l->first = newNode;

		l->count++;

		return 0;
	}

	return 1;
}


//Returns 0 in case of success
//Returns 1 in case of error
int removeValue(list* l, int value)
{
 	searchResult result = linearSearch(l, value);

 	if(result.current)
 	{
 		if(result.previous) //The value found is not the first element
 		{
 			result.previous->next = result.current->next;
 		}
 		else //The value found is the first element
 		{
 			l->first = result.current->next;
 		}

 		l->count--;
 		free(result.current);
 		return 0;
 	}  	

	return 1;
}

list split(list* l, int n) 
{
	list newList;
   	if(l)
   	{
   		node* current = l->first;
   		int i = 0;

   		node* lastItemListA;

   		while(current != NULL && current->value < n)
   		{
   			lastItemListA = current;
   			current = current->next;
   			i++;
   		}
   		
   		initialize(&newList);

   		newList.first = current;
   		newList.count = l->count - i;

   		l->count = i;
   		lastItemListA->next = NULL;
   	}

	return newList;
}

void removeAll(list* l) 
{
	if(l)
	{
		node* current = l->first;
		node* next = current->next;

		while(next != NULL)
		{
			l->count--;
			
			free(current);
			current = 0x0;

			current = next;
			next = current->next;
		}
	}
}

//Código de Teste
void unitTest() 
{
    list l;
    initialize(&l);
    insertLde(&l, 2);
    insertLde(&l, 5);
    insertLde(&l, 9);
    insertLde(&l, 12);
    insertLde(&l, 3);
    removeValue(&l, 4);
    removeValue(&l, 2);
    removeValue(&l, 3);
    printf("List 1 original (Total items: %d)\n", l.count);
    printList(&l);
    
    list l2 = split(&l, 9);
    printf("\n\nList 1 (Total items: %d)\n", l.count);
    printList(&l);
    
    printf("\n\nList 2 (Total items: %d)\n", l2.count);
    printList(&l2);
    
    printf("\n\n");
    removeAll(&l);
    removeAll(&l2);
}

int main(int argc, char* argv[]) 
{
    unitTest();
    return 0;
}