#include <stdio.h>
#include <stdlib.h>

typedef struct _node
{
    struct _node* parent;
    struct _node* left;
	struct _node* right;
	
    int key;
} node;

typedef struct
{
    node* root;
    int quantity;
} tree;

void initialize(tree* a)
{
    a->quantity = 0;
    a->root = NULL;
}

//0 means success
int addValue(tree* t, int value)
{
	node* current = t->root;
	node* parent = NULL;
	node* n = malloc(sizeof(node));
	
	n->key = value;
	n->parent = NULL;
	n->left = NULL;
	n->right = NULL;
	
	while(current != NULL)
	{
		parent = current;

        if(value < current->key) //go to left
			current = current->left;
		else //go to right
			current = current->right;
	}
	
	current = n;
	current->parent = parent;
	
	if(!t->root)
	    t->root = current;
	else if(parent)
	{
		if(value < parent->key)
			parent->left = current;
		else
	    	parent->right = current;
	}
	
	t->quantity++;

	return 0;
}

node* searchValue(node* n, int key)
{
	if(n == NULL || n->key == key)
 		return n;
	else if(key < n->key)
	    searchValue(n->left, key);
	else
	    searchValue(n->right, key);
}

// 0 means success
//-1 means error
int removeValue(tree* t, int key)
{
	node* n = searchValue(t->root, key);
	
	if(n == NULL)
	    return -1;

	if(n->left == NULL && n->right == NULL) //is a leaf
	{
		if(n->parent->left == n)
		    n->parent->left = NULL;
		else if(n->parent->right == n)
		    n->parent->right = NULL;
		    
		free(n);
		n = NULL;
	}
	else if(n->left == NULL || n->right == NULL) //only one child
	{
		if(n->parent)
		{
            node* child;
			if(n->left)
			{
				child = n->left;
				n->parent->left = child;
		    	
			}
			else if(n->right)
			{
				child = n->right;
				n->parent->right = child;
			}
			
			if(child)
				child->parent = n->parent;
		}
		
		free(n);
		n = NULL;
	}
	else //has two children
	{
		node* aux = n->right;
        while (aux->left) {
            aux = aux->left;
        }

        aux->left = n->left;
        if (aux->left)
            aux->left->parent = aux;

        if (aux->right)
            aux->right->parent = aux->parent;

        if (aux->parent != n) 
        {
           if(aux->parent)
              aux->parent->left = aux->right;
            aux->right = n->right;
            aux->right->parent = aux;
        }
        
        aux->parent = n->parent;
        if(n->parent)
        {
            if (key < n->parent->key)
            	n->parent->left = aux;
			else
				n->parent->right = aux;
        }

        free(n);
        n = 0;
	}

	t->quantity--;
	return 0;
}

void preOrder(node* n)
{
	if(!n)
		return;
		
    printf(" %d ", n->key);
	preOrder(n->left);
	preOrder(n->right);
}

void inOrder(node* n)
{
	if(!n)
		return;

	inOrder(n->left);
	printf(" %d ", n->key);
	inOrder(n->right);
}

void testSearch(tree* t, int value)
{
	node* n = searchValue(t->root, value);
	
	if(n)
	{
		printf("node found!\n");
		if(n->parent)
		    printf("parent value is %d\n", n->parent->key);
		else
		    printf("node is root\n");

		if(n->left)
		    printf("left value is %d\n", n->left->key);
		else
		    printf("there is no value on the left\n");

		if(n->right)
		    printf("right value is %d\n", n->right->key);
		else
			printf("there is no value on the right\n");
	}
	else
	    printf("node not found\n");
}

int main(int argc, char *argv[]) {

    tree t;
    initialize(&t);

    //Test your implementation here
    addValue(&t, 7);
    addValue(&t, 3);
    addValue(&t, 1);
	addValue(&t, 5);
    addValue(&t, 12);
    addValue(&t, 10);
    addValue(&t, 17);
    addValue(&t, 18);

	printf("count: %d\n", t.quantity);

    preOrder(t.root);
    puts("");
    inOrder(t.root);
    puts("");
    //testSearch(&t, 20);
    //puts("");
    //testSearch(&t, 18);
    
    removeValue(&t, 9); //removing leaf
    preOrder(t.root);
    puts("");
	removeValue(&t, 10); //removing node with left value
    preOrder(t.root);
    puts("");
    removeValue(&t, 3); //removing node with right value
    preOrder(t.root);
    puts("");
    removeValue(&t, 12); //removing node with left and right value
    preOrder(t.root);
    puts("");

    system("pause");
    return 0;
}