#include <stdio.h>
#include <stdlib.h>

#define HASH_TABLE_SIZE 13

typedef struct 
{
	int key;
	int value;
} tHashItem;

typedef struct _node
{
	tHashItem content;
	struct _node* next;
} node;

typedef struct 
{
	node* first;
	int quantity;
} linkedList;

typedef struct 
{
	int size;
	int quantity;
	linkedList* table;
} tHashTable;

void initializeLinkedList(linkedList* l)
{
	l->quantity = 0;
	l->first = NULL;
}

//0 for success
//-1 for error
int insertList(linkedList* l, tHashItem hashItem)
{
	node* newNode = malloc(sizeof(node));

	if(l == NULL)
		return -1;

	if(newNode == NULL)
		return -1;

	newNode->content = hashItem;
	newNode->next = NULL;

	node* current = l->first;
	node* previous = NULL;
	if(current == NULL)	//There are no nodes into the list
		l->first = newNode;

	while(current != NULL)
	{
		previous = current;
		current = current->next;
	}

	if(previous)
		previous->next = newNode;

	l->quantity++;
	return 0;
}

//0 for success
//-1 for error
int removeList(linkedList* l, int key)
{
	if(l == NULL)
		return -1;

	if(l->quantity == 0)
		return -1;

	node* current = l->first;
	node* previous = NULL;

	if(current == NULL)
		return -1;

	while(current != NULL && current->content.key != key)
	{
		previous = current;
		current = current->next;
	}

	if(previous)
		previous->next = current->next;

	if(l->first == current)
		l->first = current->next;

	free(current);
	current = 0;

	l->quantity--;

	return 0;
}

node* searchList(linkedList* l, int key)
{
	if(l == NULL)
		return NULL;

	if(l->quantity == 0)
		return NULL;

	node* current = l->first;

	while(current != NULL && current->content.key != key)
		current = current->next;

	return current;
}

void clearList(linkedList* l)
{
	if(l == NULL)
		return;

	while(l->first != NULL)
	{
		node* aux = l->first;
		l->first = l->first->next;
		l->quantity--;
		
		free(aux);
		aux = 0;
	}
}

int calculateHash(int key, int size)
{
	return key % size;
}

tHashTable* initializeHashTable(int size)
{
	tHashTable* t = calloc(sizeof(tHashTable), 1);
	t->size = size;
	t->quantity = 0;
	t->table = calloc(sizeof(linkedList) * size, 1);

	return t;
}

int insertHash(tHashItem item, tHashTable* t)
{
	int position = calculateHash(item.key, t->size);
	if(position > t->size - 1) //Shit!
		return -1;

	if(&t->table[position] == NULL) //linkedList not initialized yet
		initializeLinkedList(&t->table[position]);

	int result = insertList(&t->table[position], item);
	if(result == 0)
	{
		printf("Key %.2d added into list 0x%p, that already contains %d values\n", item.key, &t->table[position], t->table[position].quantity);
		t->quantity++;
	}

	return result;
}

int removeHash(int key, tHashTable* t)
{
	int position = calculateHash(key, t->size);
	if(position > t->size - 1) //Shit!
		return -1;

	int result = removeList(&t->table[position], key);

	if(result == 0)
	{
		printf("Key %.2d removed from list 0x%p, that now contains %d values\n", key, &t->table[position], t->table[position].quantity);
		t->quantity--;
	}

	return result;
}

tHashItem* searchHash(int key, tHashTable* t)
{
	int position = calculateHash(key, t->size);

	if(position > t->size - 1) //Shit!
		return NULL;

	node* n = searchList(&t->table[position], key);

	if(n)
		return &n->content;

	return NULL;
}

void destroyHash(tHashTable* t)
{
	int i;
	for(i = 0; i < t->size; i++)
	{
		clearList(&t->table[i]);
	}

	t->quantity = 0;
	t->size = 0;
	
	free(t->table);
	free(t);

	t = 0;
}

int main(int argc, char** argv)
{
	tHashTable* t = initializeHashTable(HASH_TABLE_SIZE);

	int i;
	for(i = 1; i < HASH_TABLE_SIZE * 3; i++)
	{
		tHashItem item;
		item.key = i;
		item.value = i;

		insertHash(item, t);
	}

	for(i = 3; i < HASH_TABLE_SIZE * 3; i += 3)
	{
		removeHash(i, t);
	}

	for(i = 6; i < HASH_TABLE_SIZE * 3; i += 4)
	{
		tHashItem* item = searchHash(i, t);
		if(item)
			printf("Key %.2d has the value %.2d\n", item->key, item->value);
		else
			printf("Key %.2d not found\n", i);
	}

	destroyHash(t);

	return 0;
}