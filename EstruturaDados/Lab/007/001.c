#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
	char* content;
	int count;
} stack;

void initialize(stack* s)
{
	s->count = 0;
}

void addToQueue(stack* s, char* wordA)
{
	int i, j;
	int length = strlen(wordA);
	s->content = malloc(length + 1);

	memcpy(s->content, wordA, length);
	s->count = length;
}

char removeFromStack(stack* s)
{
	s->count--;
	return s->content[s->count];
}

int isAnagram(char* wordA, char* wordB)
{
	stack s;
	initialize(&s);
	addToQueue(&s, wordA);
	int i;
	int c = s.count;
	int result = 1;

	for(i = 0; i < c; i++)
	{
		if(wordB[i] != removeFromStack(&s))
			result = 0;
	}
	
	if(s.count != 0)
		result = 0;
	
	free(s.content);
	
	return result;
}

int main(int argc, char** argv)
{
	isAnagram("amor", "roma") == 0 ? printf("is not a perfect anagram\n") : printf("is a perfect anagram\n");
	system("pause");
	return 0;
}
