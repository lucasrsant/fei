#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
	char* content;
	int count;
	int start;
} structure;

void initialize(structure *s)
{
	s->count = 0;
	s->start = 0;
}

void addToStructure(structure *s, char* buffer, int length)
{
	int i;
	s->content = (char*)malloc(length);
	for(i = 0; i < length; i++)
	    s->content[i] = buffer[i];
	    
	s->count = length;
}

char removeFromQueue(structure *s)
{
	s->count--;
	return s->content[s->count];
}

char removeFromStack(structure *s)
{
	if(s->start < s->count)
	{
		int i = s->start;
		s->start++;
		return s->content[i];
	}
}

int main(int argc, char** argv)
{
	structure stack;
	structure queue;

	char* word = "somavamos";
	int length = strlen(word);
	int partLength = length / 2;

	char* left = (char*)malloc(partLength);
	char* right = (char*)malloc(partLength);
	
	memcpy(left, word, partLength);
	memcpy(right, &word[length - partLength], partLength);

	initialize(&stack);
	initialize(&queue);

	addToStructure(&queue, left, partLength);
	addToStructure(&stack, right, partLength);

	char splitter = word[partLength];

	int i;
	for(i = 0; i < partLength; i++)
	{
		if(removeFromStack(&stack) != removeFromQueue(&queue))
			break;
	}

	if(i == partLength)
		printf("%c\n", splitter);
	else
		printf("NULL\n");

	system("pause");

	return 0;
}
