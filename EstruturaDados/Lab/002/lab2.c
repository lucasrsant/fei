#include <stdio.h>

#define MAX 30

//typedefs
typedef struct
{
	int cod;
	int salary;
	short payday;
} employee;

employee employees[MAX];

//prototypes
void addEmployee(employee employees);
void printEmployees();
void searchEmployeeByPayDay(int day);

void initialize()
{
	int i;
	for(i = 0; i < MAX; i++)
	{
		employees[i].cod = -1;
		employees[i].salary = 0;
		employees[i].payday = 0;
	}
}

void addEmployee(employee employee)
{
	employees[employee.payday] = employee;
}

void printEmployees()
{
	int i;
	for(i = 0; i < MAX; i++)
	{
		if(employees[i].cod > -1)
		{
			printf("-----Employee---------\n");
			printf(" Code: %d\n", employees[i].cod);
			printf(" Salary: %d\n", employees[i].salary);
			printf(" Payday: %d\n", employees[i].payday);
			printf("----------------------\n\n");
		}
	}
}

void searchEmployeeByPayDay(int day)
{
	if(day >= 1 && day < MAX)
	{
		if(employees[day].cod < 0)
		    printf("Employee not found\n");
  		else
  		{
        	printf("-----Employee---------\n");
			printf(" Code: %d\n", employees[day].cod);
			printf(" Salary: %d\n", employees[day].salary);
			printf(" Payday: %d\n", employees[day].payday);
			printf("----------------------\n\n");
		}
	}
}

void readEmployeeInfo()
{
	int cod;
	float salary;
	short payday;
	
	printf("Type the employee's code:\n");
	scanf("%d", &cod);
	
	printf("Type the employee's salary:\n");
	scanf("%f", &salary);
	
	printf("Type the employee's payday:\n");
	scanf("%hd", &payday);
	
	if(payday < 1 || payday > MAX)
	{
	    printf("Invalid payday\n");
	    return;
	}
	
	employee emp;
	emp.cod = cod;
	emp.salary = salary;
	emp.payday = payday;
	
	addEmployee(emp);
}

void readSearchParameter()
{
	short payday;
	printf("Type the payday that you want to find:\n");
	scanf("%hd", &payday);
	searchEmployeeByPayDay(payday);
}

int showOptions()
{
	printf("Choose an option:\n");
	printf("\t1 - Add an employee\n");
	printf("\t2 - Search for an employee by its payday\n");
	printf("\t3 - Print all employees\n");
	printf("\t4 - Exit\n");

	int opt;
	scanf("%d", &opt);
	return opt;
}

int main(int argc, char** argv)
{
	int opt = 0;
	initialize();
	
	do
	{
		opt = showOptions();

		switch(opt)
		{
			case 1:
				readEmployeeInfo();
				break;
			case 2:
				readSearchParameter();
				break;
			case 3:
				printEmployees();
				break;
			case 4:
				break;
			default:
                printf("Invalid option\n");
				break;
		}
	}
	while(opt != 4);
	
	return 0;
}
