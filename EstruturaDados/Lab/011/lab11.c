#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
    int *v;
    int max;
    int quantity;
} heap;

//prototypes
void printHeap(heap* h);
void sift(heap* h, int parentIndex);

/**Inicializa o Heap com tamanho n */
void initializeHeap(heap* h, int n)
{
    h->v = malloc(sizeof (int) * n);
    h->max = n;
    h->quantity = 0;
    if (!h->v) 
    {
        printf("Error allocating memory\n");
        system("PAUSE");
    }

}

int getRightChild(int n)
{
    return 2*n + 2;
}

int getLeftChild(int n)
{
    return 2*n + 1;
}

int getParent(int n)
{
    return (n - 1) / 2;
}

/**reordena o vetor h->v para que se torne um heap*/
void buildHeap(heap* h)
{
    sift(h, getParent(h->quantity - 1));
}

void sift(heap* h, int parentIndex)
{
    if(parentIndex < 0)
        return;

    int leftChildIndex = getLeftChild(parentIndex);
    int rightChildIndex = getRightChild(parentIndex);

    int leftChildValue = h->v[leftChildIndex];
    int rightChildValue = h->v[rightChildIndex];
    int parentValue = h->v[parentIndex];

    if(leftChildIndex >= h->quantity)
        return; //adding root

    if(leftChildValue > parentValue || rightChildValue > parentValue)
    {
        if(rightChildIndex >= h->quantity || leftChildValue > rightChildValue) //there is no right child
        {
            if(leftChildValue > parentValue)
            {
                h->v[parentIndex] = leftChildValue;
                h->v[leftChildIndex] = parentValue;
            }
        }
        else if(rightChildValue > leftChildValue)
        {
            h->v[parentIndex] = rightChildValue;
            h->v[rightChildIndex] = parentValue;
        }
    }
    
    if(parentIndex > 0)
        sift(h, getParent(parentIndex));
}

void destroyHeap(heap* h) 
{
    if (h->v)
        free(h->v);
}

/**Insere o elemento x no heap h*/
void addMaxHeap(heap* h, int x) 
{
    if(h->quantity < h->max)
    {
        h->v[h->quantity] = x;
        h->quantity++;

        //sift(h, getParent(h->quantity - 1));
    }
}

/**Retorna o maior elemento do heap h e remove-o da �rvore.*/
int removeMaxHeap(heap* h)
{
    int removedItem = h->v[0];
    h->v[0] = h->v[h->quantity - 1];
    h->quantity--;
    sift(h, getParent(h->quantity - 1));
    
    return removedItem;
}

void printHeap(heap* h)
{
    int i;
    for(i = 0; i < h->quantity; i++)    
        printf("%d", h->v[i]);

    printf("\n");
}

int main(int argc, char *argv[]) 
{
    heap h;
    initializeHeap(&h, 10);
    addMaxHeap(&h, 5);
    addMaxHeap(&h, 6);
    addMaxHeap(&h, 2);
    addMaxHeap(&h, 3);
    addMaxHeap(&h, 1);

    printHeap(&h);
    buildHeap(&h);
    printHeap(&h);

    printf("%d", removeMaxHeap(&h));
    printf("%d", removeMaxHeap(&h));
    printf("%d", removeMaxHeap(&h));
    printf("%d", removeMaxHeap(&h));
    printf("%d", removeMaxHeap(&h));

    system("pause");
    return 0;
}