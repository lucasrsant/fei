#include <stdlib.h> 
#include <stdio.h>
#include <strings.h>
  
#define FILE_NAME "list.bin"

typedef struct 
{
    int barCode;
    char productName[50]; 
    float price; 
} record; 

typedef struct no node;

struct no { 
    record content;
    node* next;
    node* previous;
} ;
   
typedef struct 
{
    node* first;
    node* last;
    int count;
} linkedList;

//prototypes
int removeItem(linkedList* list, int key);
  
void initialize(linkedList* l)
{ 
    l->first = NULL; 
    l->last = NULL; 
    l->count = 0; 
} 
  
void clearList(linkedList* l) 
{ 
    node* current = l->first;

    while (current != NULL) 
    {
        removeItem(l, current->content.barCode);
        current = l->first;
    }

    l->first = NULL;
    l->count = 0; 
} 
  
int insertItem(linkedList* list, record r) 
{
    node* current = list->first;
    node* previous = NULL;

    node* newNode = malloc(sizeof(node));

    newNode->next = NULL;
    newNode->previous = NULL;
    newNode->content = r;

    if(current)
    {
        while(current != NULL && current->content.barCode < r.barCode)
        {
            previous = current;
            current = current->next;
        }
        
        if(previous)
            previous->next = newNode;

        newNode->previous = previous;
        newNode->next = current;
    }
    else //adding at the first position
        list->first = newNode;  

    list->count++;

    return 0;
} 
  
node* search(linkedList* list, int key)
{ 
    node* current = list->first;

    while(current != NULL && current->content.barCode != key)
        current = current->next;

    return current;
} 
  
int removeItem(linkedList* list, int key)
{
    node* itemToDelete = search(list, key);
    if(itemToDelete)
    {
        if(itemToDelete->previous) //deleting in the middle or in the end of the list
            itemToDelete->previous->next = itemToDelete->next;
        else //deleting the first one
        {
            if(itemToDelete->next)
                itemToDelete->next->previous = NULL;

            list->first = itemToDelete->next;
        }

        free(itemToDelete);
        itemToDelete = 0x0;
        list->count--;
    }
    return 1; 
} 
  
void printList(linkedList* l) 
{
    node* current = l->first;

    while (current != NULL) 
    {
        printf("Bar Code: %d \n", current->content.barCode);
        printf("Name:   %s \n", current->content.productName);
        printf("Price:  %.2f \n", current->content.price);
        current = current->next;
        if (current != NULL) 
            printf("=============================================\n"); 
    }
} 
  
void mockList(linkedList *l) 
{ 
    record r;
    r.barCode = 1234565; 
    strcpy(r.productName, "The Legend Of Zelda"); 
    r.price = 77.80;
    insertItem(l, r); 

    r.barCode = 8765434; 
    strcpy(r.productName, "The Cuckcoos Calling"); 
    r.price = 49.90;
    insertItem(l, r);

    r.barCode = 555478;
    strcpy(r.productName, "And the Mountains Echoed");
    r.price = 53.90;
    insertItem(l, r);

    r.barCode = 1234567; 
    strcpy(r.productName, "Fifty Shades Trilogy"); 
    r.price = 82.80;
    insertItem(l, r); 

    r.barCode = 1234569; 
    strcpy(r.productName, "Marketing Trendlines"); 
    r.price = 25.20;
    insertItem(l, r); 
} 

void writeList(linkedList* list)
{
    FILE* file = fopen(FILE_NAME, "wb");

    if(!file)
    {
        printf("Unable to open the specified file\n");
        return;
    }

    node* current = list->first;

    while(current != NULL)
    {
        int bytesWritten = fwrite(&current->content, 1, sizeof(record), file);

        if(bytesWritten != sizeof(record))
        {
            printf("Error during file writing. Removing file ... \n");
            remove(FILE_NAME);

            return;
        }

        current = current->next;
    }

    fclose(file);
    file = 0;
}

void readList(linkedList* list)
{
    FILE* file = fopen(FILE_NAME, "rb");
    if(!file)
    {
        printf("Unable to open the specified file\n");
        return;
    }

    do
    {
        record rec;
        int bytesRead = fread(&rec, 1, sizeof(record), file);
        
        if(bytesRead != sizeof(record))
            continue;
        

        insertItem(list, rec);
    }
    while(!feof(file));
}

int main() 
{ 
    linkedList l1; 
    initialize(&l1); 
    printList(&l1); 
    mockList(&l1); 
    printList(&l1);

    printf("\n------------------------------------------------\n\n");

    writeList(&l1);
    clearList(&l1);

    readList(&l1);
    printList(&l1);

    clearList(&l1); 
  
    return 0; 
}