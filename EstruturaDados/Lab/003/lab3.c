#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX 100

//Register structure
typedef struct 
{
    char name[25];
    char phone[12];
} record;


//List structure
typedef struct 
{
    record v[MAX];
    int total;
} agenda;


void initialize(agenda *l) 
{
    l->total = 0;
}

void printRecord(record r) {
    printf(" Name.: %s\n", r.name);
    printf("Phone.: %s\n", r.phone);
}


void printAgenda(agenda *l) 
{
    int i = 0;
    for (i = 0; i < l->total; i++) 
        printRecord(l->v[i]);
}

//Inserts a record
//Returns 1 in case of success
//Returns 0 if the agenda is full
int insertAgenda(agenda *l, record *r) 
{
    if (l->total == MAX) 
    {
        //List is full
        return 0;
    }

    int i;
    for(i = 0; i < l->total && strcmp(r->name, l->v[i].name) > 0; i++);

    int j;
    for(j = l->total - 1; j >= i; j--)
        l->v[j + 1] = l->v[j];

    l->v[i] = *r;
    l->total++;

    return 1;
}

int binarySearch(agenda* l, int i, int f, char* keyToFind)
{
    int pivot = (i + f) / 2;
    int comparisonResult = strcmp(l->v[pivot].name, keyToFind);

    if(comparisonResult == 0)
        return pivot;

    if(i == f)
        return -1;

    if(comparisonResult < 0)
        return binarySearch(l, pivot + 1, f, keyToFind);
    else
        return binarySearch(l, i, pivot, keyToFind);
}

//Returns the index of the record which the key equals to the keyToFind
//If not found, returns -1
int search(agenda *l, char* keyToFind) 
{
    int i = binarySearch(l, 0, l->total - 1, keyToFind);
    return i;
}


//Removes a record which its index equals to idx.
//Returns 1 in case of success
//Returns 0 in case of error. (Invalid index)
int removeAgenda(agenda *l, int idx) 
{
    if(idx > l->total - 1 || idx < 0)
        return 0;

    int i;
    for(i = idx; i < l->total - 1; i++)
        l->v[i] = l->v[i + 1];

    l->total--;
    return 1;
}

void performTest(agenda *l)
{
    record r;
    strcpy(r.name, "Gustavo");
    strcpy(r.phone, "12312545");
    insertAgenda(l, &r);


    strcpy(r.name, "Alexandre");
    strcpy(r.phone, "1123545");
    insertAgenda(l, &r);

    strcpy(r.name, "Caio");
    strcpy(r.phone, "121145");
    insertAgenda(l, &r);

    strcpy(r.name, "Alberto");
    strcpy(r.phone, "12312335");
    insertAgenda(l, &r);
    
    printAgenda(l);
    printf("================================\n\n");

    int b = search(l, "Gustavo");
    if (b != -1) {
        printRecord( l->v[b]);
    } else {
        printf("Registro nao encontrado\n");
    }
    
    printf("================================\n\n");

    removeAgenda(l, search(l, "Gustavo"));
    
    b = search(l, "Gustavo");
    if (b != -1) {
        printRecord(l->v[b]);
    } else {
        printf("Registro nao encontrado\n");
    }
    
    printf("================================\n\n");
    printAgenda(l);
}

int main() 
{
    agenda l;
    initialize(&l);
    performTest(&l);
    return 0;
}