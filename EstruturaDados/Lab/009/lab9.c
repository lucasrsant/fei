#include <stdio.h>
#include <stdlib.h>
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

typedef struct _node 
{
    struct _node* parent;
    struct _node* left;
    struct _node* right;
    int key;
} node;

typedef struct 
{
    node* root;
    int quantity;
} tree;

int _print_t(node *n, int is_left, int offset, int depth, char s[40][255])
{
    char b[40];
    int width = 5;
    int i = 0;

    if (!n) return 0;

    sprintf(b, "(%03d)", n->key);

    int left  = _print_t(n->left,  1, offset,                depth + 1, s);
    int right = _print_t(n->right, 0, offset + left + width, depth + 1, s);

    for (i = 0; i < width; i++)
        s[2 * depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (i = 0; i < width + right; i++)
            s[2 * depth - 1][offset + left + width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset + left + width + right + width/2] = '+';

    } else if (depth && !is_left) {

        for (i = 0; i < left + width; i++)
            s[2 * depth - 1][offset - width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset - width/2 - 1] = '+';
    }

    return left + width + right;
}

int print_t(node *n)
{
    int i = 0;

    char s[40][255];
    for (i = 0; i < 40; i++)
        sprintf(s[i], "%80s", " ");

    _print_t(n, 0, 0, 0, s);

    for (i = 0; i < 40; i++)
        printf("%s\n", s[i]);
}

void initialize(tree* t) 
{
    t->quantity = 0;
    t->root = NULL;
}

int height(node* n) 
{   
    if (!n)
        return -1;

    return MAX(height(n->left), height(n->right)) + 1;
}

int balanceFactor(node* n) 
{
    if(!n)
        return 0;

    return height(n->right) - height(n->left);
}


void leftRotate(tree* t, node* x)
{
    if(!x)
        return;

    node* y = x->right;
    
    if(y)
    {
        if(x->parent)
        {
            y->parent = x->parent;

            if(y->key > x->parent->key)
                x->parent->right = y;
            else
                x->parent->left = y;
        }
        else
        {
            y->parent = NULL;
            t->root = y;
        }

        x->right = y->left;
        x->parent = y;
        y->left = x;
    }
}

void rightRotate(tree* t, node* y)
{
    if(!y)
        return;

    node* x = y->left;

    if(x)
    {
        if(y->parent)
        {
            x->parent = y->parent;

            if(x->key > y->parent->key)
                y->parent->right = x;
            else
                y->parent->left = x;
        }
        else
        {
            x->parent = NULL;
            t->root = x;
        }

        y->left = x->right;
        y->parent = x;
        x->right = y;
    }
}

void balance(tree* t, node* n)
{
    node* aux = n;
    int b = 0;
    do
    {
        b = balanceFactor(aux->parent);
        aux = aux->parent;
    }
    while(b >= -1 && b <= 1 && aux->parent != NULL);

    if(b == -2)
    {
        int bChild = balanceFactor(aux->left);

        if(bChild == 1)
            leftRotate(t, aux->left);

        rightRotate(t, aux);
    }

    if(b == 2)
    {
        int bChild = balanceFactor(aux->right);

        if(bChild == -1)
            rightRotate(t, aux->right);

        leftRotate(t, aux);
    }
}

//0 means success
//-1 means error
int add(tree* t, int value) 
{
    node* newNode = malloc(sizeof(node));

    if (newNode == NULL) 
    {
        printf("Pan!!\n");
        return -1;//Error
    }

    newNode->parent = NULL;
    newNode->left = NULL;
    newNode->right = NULL;
    newNode->key = value;

    if (t->root == NULL) 
    {
        t->root = newNode;
        t->quantity++;
        return 0; //OK
    }

    node* child;
    node* parent;

    child = t->root;
    parent = NULL;

    while (child != NULL) 
    {
        parent = child;

        if (value < child->key)
            child = child->left;
        else
            child = child->right;
    }

    newNode->parent = parent;
    if (value < parent->key)
        parent->left = newNode;
    else
        parent->right = newNode;

    t->quantity++;    
    balance(t, newNode);

    return 0;
}

node* search(tree* t, int key)
{
    node* current = t->root;

    while (current != NULL && current->key != key) 
    {
        if (key < current->key)
            current = current->left;
        else
            current = current->right;
    }

    return current;
}

int delete(tree* t, int key) 
{
    node* nodeToRemove = search(t, key);

    if (!nodeToRemove)
        return 0;

    node* rp = NULL;

    //Case 1: nodeToRemove do not have children
    if (nodeToRemove->left == NULL && nodeToRemove->right == NULL) 
    {
        if(nodeToRemove->parent)
        {
            if (key < nodeToRemove->parent->key) 
            {
                nodeToRemove->parent->left = NULL;
            } else {
                nodeToRemove->parent->right = NULL;
            }
       }
    }//Case 2: nodeToRemove has only one child
    else if ((nodeToRemove->left != NULL || nodeToRemove->right != NULL) && !(nodeToRemove->left != NULL && nodeToRemove->right != NULL)) 
    {
        if (nodeToRemove->left != NULL)
            rp = nodeToRemove->left;
        else
            rp = nodeToRemove->right;

        if(nodeToRemove->parent)
        {
            if (key < nodeToRemove->parent->key)
            {
                nodeToRemove->parent->left = rp;
            }
            else 
            {
                nodeToRemove->parent->right = rp;
            }
        }
        rp->parent = nodeToRemove->parent;
    }//Case 3: nodeToRemove has two children
    else 
    {
        rp = nodeToRemove->right;

        while (rp->left)
        {
            rp = rp->left;
        }

        rp->left = nodeToRemove->left;

        if (rp->left)
            rp->left->parent = rp;

        if (rp->right)
            rp->right->parent = rp->parent;

        if (rp->parent != nodeToRemove) 
        {
           if(rp->parent)
              rp->parent->left = rp->right;

            if(rp->right)
            {
                rp->right = nodeToRemove->right;
                rp->right->parent = rp;
            }
        }
        
        rp->parent = nodeToRemove->parent;
        if(nodeToRemove->parent)
        {
            if (key < nodeToRemove->parent->key)
                nodeToRemove->parent->left = rp;
            else
                nodeToRemove->parent->right = rp;
        }
    }

    if (!nodeToRemove->parent)
    {
        t->root = rp;
    }

    balance(t, nodeToRemove);
    free(nodeToRemove);
    return 1;

}

void erd(node* n) 
{
    if (n->left)
        erd(n->left);

    printf(" %d ", n->key);

    if (n->right)
        erd(n->right);
}

void spacing(const char* str, int n) 
{
    while (n > 0) 
    {
        printf("%s", str);
        n--;
    }
}

void printSubTree(node* n, int level)
{
    if (!n)
    {
        spacing("    ", level);
        puts("   ~");
    } 
    else 
    {
        printSubTree(n->right, level + 1);
        spacing("    ", level);
        printf("%4d\n", n->key);
        printSubTree(n->left, level + 1);
    }

    if (level == 0)
    {
        spacing("=", 40);
        printf("\n");
    }
}

void destroySubTree(node* a) 
{
    if (a->left)
        destroySubTree(a->left);
    if (a->right)
        destroySubTree(a->right);

    free(a);
}

int main(int argc, char *argv[]) 
{
    tree t;
    initialize(&t);

    add(&t, 5);
    print_t(t.root);
    add(&t, 8);
    print_t(t.root);
    add(&t, 6);
    print_t(t.root);
    add(&t, 20);
    print_t(t.root);
    add(&t, 19);
    print_t(t.root);
    add(&t, 17);
    print_t(t.root);
    add(&t, 18);
    print_t(t.root);
    add(&t, 15);
    print_t(t.root);

    printf("\n\n******REMOVING******\n\n");

    delete(&t, 5);
    print_t(t.root);
    delete(&t, 8);
    print_t(t.root);
    delete(&t, 15);
    print_t(t.root);

    destroySubTree(t.root);
    system("pause");
    return 0;
}