#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct node
{
	int value;
	struct node* next;
} node;

typedef struct
{
	node* first;
	unsigned int size;
} linkedList;

void initialize(linkedList * list)
{
	list->size = 0;
	list->first = NULL;
}

void insert(linkedList* list, int value)
{
	node* newNode = malloc(sizeof(node));
	newNode->value = value;
	newNode->next = NULL;

	node* current = list->first;
	node* previous = NULL;

	while(current != NULL && current->value < value)
	{
		previous = current;
		current = current->next;
	}

	if(previous != NULL)
		previous->next = newNode;	
	else
		list->first = newNode;
	
	newNode->next = current;
	list->size++;
}

void delete(linkedList* l, node* n)
{
	if(!n)
		return;

	node* current = l->first;

	if(current == n)
	{
		l->first = l->first->next;
		return;
	}

	while(current != NULL && current->next != n)
		current = current->next;

	if(current->next != n)
		return;
	
	current->next = n->next;
	
	l->size--;

	free(n);
	n = 0;
}

const node* search(linkedList* l, int value)
{
	node* current = l->first;

	while(current != NULL && current->value < value)
		current = current->next;

	if(current != NULL && current->value == value)
		return current;

	return NULL;
}

void print(linkedList* l)
{
	node* current = l->first;

	while(current != NULL)
	{
		printf("%-2d : %x -> %x\n", current->value, current, current->next);
		current = current->next;
	}

	puts("");
}

int main(int argc, char** argv)
{
	srand(time(NULL));

	linkedList l;
	initialize(&l);

	int i;
	for(i = 0; i < 20; i++)
	{
		insert(&l, i * 2);
	}

	print(&l);

	node* n = search(&l, 10);

	delete(&l, n);

	print(&l);

	system("pause");

	return 0;
}